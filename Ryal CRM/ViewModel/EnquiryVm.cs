﻿using Ryal_CRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ryal_CRM.ViewModel
{
    public class EnquiryVm
    {
        public List<tbl_enquiry_management> EnquiryList { get; set; }
    }
}