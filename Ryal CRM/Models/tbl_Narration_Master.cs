//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Narration_Master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_Narration_Master()
        {
            this.tbl_Purchase_Narration = new HashSet<tbl_Purchase_Narration>();
        }
    
        public int NmID { get; set; }
        public string Narration { get; set; }
        public string Department { get; set; }
        public Nullable<int> charge_code { get; set; }
        public bool isDefault { get; set; }
        public int created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public int updated_by { get; set; }
        public System.DateTime updated_on { get; set; }
        public Nullable<decimal> Amount { get; set; }
    
        public virtual tbl_admin_master_app tbl_admin_master_app { get; set; }
        public virtual tbl_admin_master_app tbl_admin_master_app1 { get; set; }
        public virtual tbl_charge_master tbl_charge_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Purchase_Narration> tbl_Purchase_Narration { get; set; }
    }
}
