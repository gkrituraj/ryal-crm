//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_airline_charge_master
    {
        public int airline_charge_id { get; set; }
        public Nullable<int> airline_Airline_No { get; set; }
        public Nullable<int> airline_Charge_No { get; set; }
        public string airline_charge_code { get; set; }
        public Nullable<int> airline_charge_created_by { get; set; }
        public Nullable<System.DateTime> airline_charge_created_on { get; set; }
    
        public virtual tbl_charge_master tbl_charge_master { get; set; }
        public virtual tbl_company_master tbl_company_master { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
