//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_user_branch_association
    {
        public int ub_no { get; set; }
        public int user_no { get; set; }
        public int ryal_branch_no { get; set; }
        public Nullable<System.DateTime> updated_on { get; set; }
        public Nullable<int> updated_by { get; set; }
        public string multiuser { get; set; }
    
        public virtual tbl_ryal_business_branch tbl_ryal_business_branch { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
