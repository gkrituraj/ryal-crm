//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_AirImpClearance_subjob_activity_list
    {
        public int AirImpClearance_Activity_id { get; set; }
        public Nullable<int> AirImpClearance_Activity_no { get; set; }
        public Nullable<int> AirImpClearance_Sub_job_id { get; set; }
        public Nullable<int> AirImpClearance_Bill_to_company_id { get; set; }
        public Nullable<System.DateTime> AirImpClearance_Created_on_datetime { get; set; }
        public Nullable<int> AirImpClearance_Created_by { get; set; }
        public bool AirImpClearance_Activity_status { get; set; }
        public Nullable<int> AirImpClearance_activity_bill_to { get; set; }
        public Nullable<int> AirImpClearance_activity_employee { get; set; }
    
        public virtual tbl_activity_bill_to tbl_activity_bill_to { get; set; }
        public virtual tbl_activity_master tbl_activity_master { get; set; }
        public virtual Tbl_AirImpClearance_subjob Tbl_AirImpClearance_subjob { get; set; }
        public virtual tbl_company_business_unit tbl_company_business_unit { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
