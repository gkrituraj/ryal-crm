//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_mawb_details
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_mawb_details()
        {
            this.tbl_mawb_airline = new HashSet<tbl_mawb_airline>();
            this.tbl_mawb_cargo_details = new HashSet<tbl_mawb_cargo_details>();
            this.tbl_mawb_cargo_details1 = new HashSet<tbl_mawb_cargo_details1>();
            this.tbl_mawb_cargo_dimension = new HashSet<tbl_mawb_cargo_dimension>();
            this.tbl_mawb_charges = new HashSet<tbl_mawb_charges>();
            this.tbl_mawb_hawb_link = new HashSet<tbl_mawb_hawb_link>();
        }
    
        public int MAWB_id { get; set; }
        public int MAWB_master_job_id { get; set; }
        public Nullable<System.DateTime> MAWB_Created_datetime { get; set; }
        public string MAWB_stock_no { get; set; }
        public string MAWB_currency { get; set; }
        public string MAWB_freight { get; set; }
        public string MAWB_others { get; set; }
        public string MAWB_status { get; set; }
        public string MAWB_cargo_type { get; set; }
        public string MAWB_cargo_service { get; set; }
        public string MAWB_insurance { get; set; }
        public string MAWB_value_for_customs { get; set; }
        public Nullable<System.DateTime> MAWB_dsr_date { get; set; }
        public Nullable<System.DateTime> MAWB_csr_date { get; set; }
        public Nullable<decimal> MAWB_total_freight_charges { get; set; }
        public Nullable<decimal> MAWB_total_due_carrier_charges { get; set; }
        public Nullable<decimal> MAWB_total_due_agent_charges { get; set; }
        public Nullable<decimal> MAWB_gross_freight_total { get; set; }
        public bool MAWB_activity_status { get; set; }
        public string MAWB_nature_quantity_goods { get; set; }
        public string MAWB_accounting_information { get; set; }
        public string MAWB_handling_information { get; set; }
        public string MAWB_bottom_description { get; set; }
        public string MAWB_exp_invoice_hash { get; set; }
        public string MAWB_shipping_bill_no { get; set; }
        public Nullable<System.DateTime> MAWB_shipping_bill_date { get; set; }
        public string MAWB_Shipper_Address { get; set; }
        public string MAWB_Consignee_Address { get; set; }
        public string MAWB_Value_For_Carriage { get; set; }
        public Nullable<System.DateTime> MAWB_Exp_Invoice_Date { get; set; }
        public string Airport_Destination_name { get; set; }
        public string MAWB_AirLine { get; set; }
        public string MAWB_AirLine_code { get; set; }
        public string MAWB_Tracking { get; set; }
        public Nullable<int> MAWB_XML_Doc_Version { get; set; }
        public string MAWB_Consignee_Name { get; set; }
        public Nullable<int> MAWB_CreatedBy { get; set; }
        public string consignee_zipcode { get; set; }
        public Nullable<int> consignee_country { get; set; }
        public Nullable<int> MAWB_EAWB_XML_Doc_Version { get; set; }
        public string shipper_zipcode { get; set; }
        public Nullable<int> shipper_country { get; set; }
        public string agent_charge_type { get; set; }
        public string carrier_charge_type { get; set; }
        public bool mawb_draft { get; set; }
        public string mawb_notify { get; set; }
        public string mawb_print_destination { get; set; }
        public string mawb_billing_description { get; set; }
        public string mawb_print_origin { get; set; }
        public Nullable<decimal> final_ch_wt { get; set; }
        public string final_rate_class { get; set; }
        public Nullable<decimal> final_tarrif_rate { get; set; }
        public Nullable<decimal> final_amount { get; set; }
        public string mawb_curr_track_status { get; set; }
        public Nullable<bool> is_tracking_enabled { get; set; }
    
        public virtual tbl_country_master tbl_country_master { get; set; }
        public virtual Tbl_master_job Tbl_master_job { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mawb_airline> tbl_mawb_airline { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mawb_cargo_details> tbl_mawb_cargo_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mawb_cargo_details1> tbl_mawb_cargo_details1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mawb_cargo_dimension> tbl_mawb_cargo_dimension { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mawb_charges> tbl_mawb_charges { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mawb_hawb_link> tbl_mawb_hawb_link { get; set; }
    }
}
