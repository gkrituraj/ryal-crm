//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_SeaImport_subjob_contacts
    {
        public int SeaImport_subjob_contact_id { get; set; }
        public Nullable<int> SeaImportContact_addressbook_no { get; set; }
        public Nullable<int> SeaImport_subjob_id { get; set; }
        public string SeaImport_Contact_is_employee { get; set; }
        public Nullable<int> SeaImport_Contact_department_id { get; set; }
        public Nullable<System.DateTime> SeaImport_Created_on_datetime { get; set; }
        public Nullable<int> SeaImport_Created_by { get; set; }
    
        public virtual tbl_address_book tbl_address_book { get; set; }
        public virtual tbl_department_master tbl_department_master { get; set; }
        public virtual Tbl_SeaImport_sub_job Tbl_SeaImport_sub_job { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
