//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_charge_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_charge_master()
        {
            this.Tbl_AirImport_Charges = new HashSet<Tbl_AirImport_Charges>();
            this.tbl_airline_charge_master = new HashSet<tbl_airline_charge_master>();
            this.tbl_AirLine_Handling_Can_Details = new HashSet<tbl_AirLine_Handling_Can_Details>();
            this.tbl_bill_credit_Note_details = new HashSet<tbl_bill_credit_Note_details>();
            this.tbl_bill_debit_Note_details = new HashSet<tbl_bill_debit_Note_details>();
            this.tbl_debit_Note_details = new HashSet<tbl_debit_Note_details>();
            this.tbl_Enquiry_aec_misc_charges = new HashSet<tbl_Enquiry_aec_misc_charges>();
            this.tbl_Enquiry_aef_charges = new HashSet<tbl_Enquiry_aef_charges>();
            this.tbl_charge_template_details = new HashSet<tbl_charge_template_details>();
            this.tbl_Enquiry_Charge_Detail = new HashSet<tbl_Enquiry_Charge_Detail>();
            this.tbl_Enquiry_Quotation_Air_Dstn_orgn_chrg = new HashSet<tbl_Enquiry_Quotation_Air_Dstn_orgn_chrg>();
            this.tbl_Enquiry_Quotation_Air_Frt_chrg = new HashSet<tbl_Enquiry_Quotation_Air_Frt_chrg>();
            this.tbl_Enquiry_Quotation_Frt_charge = new HashSet<tbl_Enquiry_Quotation_Frt_charge>();
            this.tbl_Enquiry_Quotation_Oth_charge = new HashSet<tbl_Enquiry_Quotation_Oth_charge>();
            this.tbl_hawb_charges = new HashSet<tbl_hawb_charges>();
            this.tbl_mawb_charges = new HashSet<tbl_mawb_charges>();
            this.tbl_Narration_Master = new HashSet<tbl_Narration_Master>();
            this.tbl_purchase_details = new HashSet<tbl_purchase_details>();
            this.Tbl_SeaImport_Charges = new HashSet<Tbl_SeaImport_Charges>();
            this.tbl_user_master_App = new HashSet<tbl_user_master_App>();
        }
    
        public int charge_no { get; set; }
        public string charge_code { get; set; }
        public string charge_name { get; set; }
        public int charge_type { get; set; }
        public Nullable<int> charge_sac { get; set; }
        public decimal charge_amount { get; set; }
        public bool charge_discount_applicable { get; set; }
        public bool app_airexports { get; set; }
        public bool app_airimports { get; set; }
        public bool app_seaexports { get; set; }
        public bool app_seaimports { get; set; }
        public bool app_airclearance { get; set; }
        public bool app_seaclearance { get; set; }
        public bool app_warehousing { get; set; }
        public bool app_transportation { get; set; }
        public Nullable<System.DateTime> edited_on { get; set; }
        public Nullable<int> edited_by { get; set; }
        public bool app_airimportclearance { get; set; }
        public bool app_seaimportclearance { get; set; }
        public string charge_doc_type { get; set; }
        public Nullable<int> charge_Income_Ledger { get; set; }
        public Nullable<int> charge_Expence_Ledger { get; set; }
        public Nullable<decimal> charge_igst { get; set; }
        public Nullable<decimal> charge_cgst { get; set; }
        public Nullable<decimal> charge_sgst { get; set; }
        public string is_taxable { get; set; }
        public string is_charge_deactived { get; set; }
        public string charge_is_Bank_Receipt { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_AirImport_Charges> Tbl_AirImport_Charges { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_airline_charge_master> tbl_airline_charge_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_AirLine_Handling_Can_Details> tbl_AirLine_Handling_Can_Details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_bill_credit_Note_details> tbl_bill_credit_Note_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_bill_debit_Note_details> tbl_bill_debit_Note_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_debit_Note_details> tbl_debit_Note_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Enquiry_aec_misc_charges> tbl_Enquiry_aec_misc_charges { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Enquiry_aef_charges> tbl_Enquiry_aef_charges { get; set; }
        public virtual tbl_Ledger tbl_Ledger { get; set; }
        public virtual tbl_Ledger tbl_Ledger1 { get; set; }
        public virtual tbl_sac_code_master tbl_sac_code_master { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_charge_template_details> tbl_charge_template_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Enquiry_Charge_Detail> tbl_Enquiry_Charge_Detail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Enquiry_Quotation_Air_Dstn_orgn_chrg> tbl_Enquiry_Quotation_Air_Dstn_orgn_chrg { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Enquiry_Quotation_Air_Frt_chrg> tbl_Enquiry_Quotation_Air_Frt_chrg { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Enquiry_Quotation_Frt_charge> tbl_Enquiry_Quotation_Frt_charge { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Enquiry_Quotation_Oth_charge> tbl_Enquiry_Quotation_Oth_charge { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_hawb_charges> tbl_hawb_charges { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mawb_charges> tbl_mawb_charges { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Narration_Master> tbl_Narration_Master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_purchase_details> tbl_purchase_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_SeaImport_Charges> Tbl_SeaImport_Charges { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_user_master_App> tbl_user_master_App { get; set; }
    }
}
