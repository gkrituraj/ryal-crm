//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_user_master_warehouse
    {
        public int user_wh_id { get; set; }
        public int user_wh_user_no { get; set; }
        public int user_wh_unit_no { get; set; }
    
        public virtual tbl_company_business_unit tbl_company_business_unit { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
