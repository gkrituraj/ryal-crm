//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_shipping_master_job
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tbl_shipping_master_job()
        {
            this.tbl_mbl_details = new HashSet<tbl_mbl_details>();
            this.Tbl_SeaClearance_subjob = new HashSet<Tbl_SeaClearance_subjob>();
            this.Tbl_Shiping_sub_job = new HashSet<Tbl_Shiping_sub_job>();
        }
    
        public int shipping_Master_job_id { get; set; }
        public string shipping_Master_job_no { get; set; }
        public Nullable<System.DateTime> shipping_Created_on_datetime { get; set; }
        public Nullable<int> shipping_Created_by { get; set; }
        public int shipping_Ryal_compnay_id { get; set; }
        public string shipping_Master_job_type { get; set; }
        public Nullable<int> shipping_Master_shipper_id { get; set; }
        public Nullable<int> shipping_Master_Consignee_id { get; set; }
        public string shipping_Master_shipper_name { get; set; }
        public string shipping_Master_consignee_name { get; set; }
        public string SXF_Deleted_MasterJob { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mbl_details> tbl_mbl_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_SeaClearance_subjob> Tbl_SeaClearance_subjob { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_Shiping_sub_job> Tbl_Shiping_sub_job { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
