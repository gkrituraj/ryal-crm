//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_query_remarks
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_query_remarks()
        {
            this.tbl_AEC_Milestone = new HashSet<tbl_AEC_Milestone>();
            this.tbl_AIC_Milestone = new HashSet<tbl_AIC_Milestone>();
            this.tbl_SEC_Milestone = new HashSet<tbl_SEC_Milestone>();
            this.tbl_SIC_Milestone = new HashSet<tbl_SIC_Milestone>();
        }
    
        public int qr_id { get; set; }
        public string Activity_ID { get; set; }
        public string remark_text { get; set; }
        public Nullable<int> updated_by { get; set; }
        public Nullable<System.DateTime> updated_on { get; set; }
    
        public virtual tbl_admin_master_app tbl_admin_master_app { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_AEC_Milestone> tbl_AEC_Milestone { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_AIC_Milestone> tbl_AIC_Milestone { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_SEC_Milestone> tbl_SEC_Milestone { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_SIC_Milestone> tbl_SIC_Milestone { get; set; }
    }
}
