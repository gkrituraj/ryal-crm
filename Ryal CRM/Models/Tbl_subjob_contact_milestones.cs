//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_subjob_contact_milestones
    {
        public int Subjob_milestone_id { get; set; }
        public Nullable<int> Subjob_contact_id { get; set; }
        public Nullable<int> Sub_job_id { get; set; }
        public Nullable<int> Milestone_id { get; set; }
        public Nullable<System.DateTime> Created_on_datetime { get; set; }
        public Nullable<int> Created_by { get; set; }
    }
}
