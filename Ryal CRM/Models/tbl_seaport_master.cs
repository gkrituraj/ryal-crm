//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_seaport_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_seaport_master()
        {
            this.tbl_company_business_unit = new HashSet<tbl_company_business_unit>();
            this.tbl_company_business_unit1 = new HashSet<tbl_company_business_unit>();
            this.tbl_hbl_details = new HashSet<tbl_hbl_details>();
            this.tbl_hbl_details1 = new HashSet<tbl_hbl_details>();
            this.tbl_mbl_details = new HashSet<tbl_mbl_details>();
            this.tbl_mbl_details1 = new HashSet<tbl_mbl_details>();
            this.tbl_ryal_business_branch = new HashSet<tbl_ryal_business_branch>();
            this.Tbl_SeaClearance_subjob = new HashSet<Tbl_SeaClearance_subjob>();
            this.Tbl_SeaImpClearance_subjob = new HashSet<Tbl_SeaImpClearance_subjob>();
            this.Tbl_SeaImport_sub_job = new HashSet<Tbl_SeaImport_sub_job>();
            this.Tbl_SeaImport_sub_job1 = new HashSet<Tbl_SeaImport_sub_job>();
            this.Tbl_Shiping_sub_job = new HashSet<Tbl_Shiping_sub_job>();
            this.Tbl_Shiping_sub_job1 = new HashSet<Tbl_Shiping_sub_job>();
        }
    
        public int seaport_no { get; set; }
        public string seaport_code { get; set; }
        public string seaport_name { get; set; }
        public int country_no { get; set; }
        public Nullable<System.DateTime> updated_on { get; set; }
        public int updated_by { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_company_business_unit> tbl_company_business_unit { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_company_business_unit> tbl_company_business_unit1 { get; set; }
        public virtual tbl_country_master tbl_country_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_hbl_details> tbl_hbl_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_hbl_details> tbl_hbl_details1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mbl_details> tbl_mbl_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_mbl_details> tbl_mbl_details1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_ryal_business_branch> tbl_ryal_business_branch { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_SeaClearance_subjob> Tbl_SeaClearance_subjob { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_SeaImpClearance_subjob> Tbl_SeaImpClearance_subjob { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_SeaImport_sub_job> Tbl_SeaImport_sub_job { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_SeaImport_sub_job> Tbl_SeaImport_sub_job1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_Shiping_sub_job> Tbl_Shiping_sub_job { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_Shiping_sub_job> Tbl_Shiping_sub_job1 { get; set; }
    }
}
