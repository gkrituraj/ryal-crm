//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Enquiry_status
    {
        public int enquiry_stat_id { get; set; }
        public int Enquiry_id { get; set; }
        public int enquiry_stat_user { get; set; }
        public string enquiry_stat_Status { get; set; }
        public System.DateTime enquiry_stat_date { get; set; }
    
        public virtual tbl_enquiry_management tbl_enquiry_management { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
