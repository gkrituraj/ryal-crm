//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_hawb_charges
    {
        public int HAWB_charge_id { get; set; }
        public int HAWB_id { get; set; }
        public int HAWB_Charge_no { get; set; }
        public string HAWB_Charge_type { get; set; }
        public Nullable<decimal> HAWB_Charge_amount { get; set; }
        public Nullable<decimal> HAWB_Charge_tax_amount { get; set; }
        public string HAWB_Charge_remarks { get; set; }
        public string istaxable { get; set; }
        public Nullable<decimal> HAWB_Charge_rate { get; set; }
        public string HAWB_Charge_unit { get; set; }
    
        public virtual tbl_charge_master tbl_charge_master { get; set; }
        public virtual tbl_hawb_details tbl_hawb_details { get; set; }
    }
}
