//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_AirClearance_subjob_activity_list
    {
        public int AirClearance_Activity_id { get; set; }
        public Nullable<int> AirClearance_Activity_no { get; set; }
        public Nullable<int> AirClearance_Sub_job_id { get; set; }
        public Nullable<int> AirClearance_Bill_to_company_id { get; set; }
        public Nullable<System.DateTime> AirClearance_Created_on_datetime { get; set; }
        public Nullable<int> AirClearance_Created_by { get; set; }
        public bool AirClearance_Activity_status { get; set; }
        public Nullable<int> AirClearance_activity_bill_to { get; set; }
        public Nullable<int> AirClearance_activity_employee { get; set; }
    
        public virtual tbl_activity_bill_to tbl_activity_bill_to { get; set; }
        public virtual tbl_activity_master tbl_activity_master { get; set; }
        public virtual Tbl_AirClearance_subjob Tbl_AirClearance_subjob { get; set; }
        public virtual tbl_company_business_unit tbl_company_business_unit { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
        public virtual tbl_user_master tbl_user_master1 { get; set; }
    }
}
