//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_airlines_master
    {
        public int al_no { get; set; }
        public int company_no { get; set; }
        public string al_ar_code { get; set; }
        public Nullable<int> al_prefix { get; set; }
        public string al_numbering { get; set; }
        public string al_csr_on { get; set; }
        public string al_commission_on { get; set; }
        public string al_tds_on_contract_applicable_on { get; set; }
        public string al_tds_comission { get; set; }
        public string al_tds_on_contract { get; set; }
        public string al_bank_account_name { get; set; }
        public string al_bank_account_details { get; set; }
        public bool al_is_airway_bill_on_net_net { get; set; }
        public string al_cost_per_csr { get; set; }
        public bool al_is_include_tds_on_cargo_sales { get; set; }
        public bool al_is_tds_on_comission_in_cargo_sales { get; set; }
        public bool al_roundoff_comssion_in_airway_bill { get; set; }
        public bool al_roundoff_comission_cargo_sales { get; set; }
        public bool al_extract_destination_code_in_airport { get; set; }
        public bool al_airline_of_indian_origin { get; set; }
        public bool al_round_odd_freight_airway_bill { get; set; }
        public string al_account_international { get; set; }
        public string al_account_express { get; set; }
        public bool al_is_blacklisted { get; set; }
        public string country_name { get; set; }
    
        public virtual tbl_awb_prefix_airline tbl_awb_prefix_airline { get; set; }
        public virtual tbl_company_master tbl_company_master { get; set; }
    }
}
