//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_shipping_dg_declaration
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_shipping_dg_declaration()
        {
            this.tbl_shipping_dg_goods_identification = new HashSet<tbl_shipping_dg_goods_identification>();
        }
    
        public int Shipping_dg_decl_id { get; set; }
        public string Shipping_doc_no { get; set; }
        public string Shipping_sub_job_no { get; set; }
        public string Shipping_shipper_name_address { get; set; }
        public string Shipping_consignee_name_address { get; set; }
        public Nullable<int> Shipping_discharge_seaport { get; set; }
        public Nullable<int> Shipping_loading_seaport { get; set; }
        public string Shipping_shipper_name { get; set; }
        public string Shipping_consignee_name { get; set; }
        public string Shipping_discharge_seaport_name { get; set; }
        public string Shipping_loading_seaport_name { get; set; }
        public string Shipping_handling_information { get; set; }
        public string Shipping_Vessel_Name { get; set; }
        public string Shipping_VoyageNo { get; set; }
        public string Shipping_Shipper_Reference { get; set; }
        public string Shipping_Freight_Forwarder { get; set; }
        public string Shipping_Destination { get; set; }
        public string Shipping_GrossWt { get; set; }
        public string Shipping_NetWt { get; set; }
        public string Shipping_Cube { get; set; }
        public string Shipping_Container_Identification { get; set; }
        public string Shipping_Seal_No { get; set; }
        public string Shipping_Container_Vehicle { get; set; }
        public string Shipping_TareWt { get; set; }
        public string Shipping_Total_GrossWt { get; set; }
        public string Shipping_Client_Company { get; set; }
        public string Sjipping_client_Name_Status { get; set; }
        public string Shipping_Client_Place_Date { get; set; }
        public string Shipping_Client_Declarant { get; set; }
        public string Shipping_Cmpny_Company { get; set; }
        public string Sjipping_Cmpny_Name_Status { get; set; }
        public string Shipping_Cmpny_Place_Date { get; set; }
        public string Shipping_Cmpny_Declarant { get; set; }
        public Nullable<System.DateTime> Shipping_UpdatedON { get; set; }
        public Nullable<int> Shipping_UpdatedBy { get; set; }
        public Nullable<int> Shipping_CreatedBy { get; set; }
        public Nullable<System.DateTime> Shipping_Cmpny_Date { get; set; }
        public Nullable<System.DateTime> Shipping_Client_Date { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_shipping_dg_goods_identification> tbl_shipping_dg_goods_identification { get; set; }
    }
}
