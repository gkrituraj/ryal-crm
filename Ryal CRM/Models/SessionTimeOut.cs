﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRMAdmin.Models
{
    public class SessionTimeOut : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session["user_login_id"] == null)
            {
                filterContext.Result = new RedirectResult("~/CustomerPortal/Login");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}