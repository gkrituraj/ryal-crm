//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_SeaImpClearance_masterjob
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tbl_SeaImpClearance_masterjob()
        {
            this.Tbl_SeaImpClearance_subjob = new HashSet<Tbl_SeaImpClearance_subjob>();
        }
    
        public int SeaImpClearance_Masterjob_id { get; set; }
        public string SeaImpClearance_Masterjob_no { get; set; }
        public Nullable<int> SeaImpClearance_CreatedBy { get; set; }
        public Nullable<System.DateTime> SeaImpClearace_Created_On { get; set; }
        public string SIC_Deleted_MasterJob { get; set; }
    
        public virtual tbl_user_master tbl_user_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_SeaImpClearance_subjob> Tbl_SeaImpClearance_subjob { get; set; }
    }
}
