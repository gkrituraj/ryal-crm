//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_module_lockbyuser
    {
        public int lock_id { get; set; }
        public Nullable<int> lock_user_id { get; set; }
        public string lock_module { get; set; }
        public Nullable<int> lock_key_value { get; set; }
    
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
