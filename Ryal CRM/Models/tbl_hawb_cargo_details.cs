//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_hawb_cargo_details
    {
        public int HAWB_cargo_details_id { get; set; }
        public int HAWB_id { get; set; }
        public string HAWB_product_name { get; set; }
        public int HAWB_no_of_packages { get; set; }
        public decimal HAWB_total_package_weight { get; set; }
        public string HAWB_package_type { get; set; }
        public string HAWB_units { get; set; }
        public int HAWB_length { get; set; }
        public int HAWB_breadth { get; set; }
        public int HAWB_height { get; set; }
        public decimal HAWB_volume_weight { get; set; }
        public decimal HAWB_unit_rate { get; set; }
        public Nullable<decimal> HAWB_total_amount { get; set; }
        public bool HAWB_is_dg { get; set; }
        public string HAWB_unno { get; set; }
        public string HAWB_rate_class { get; set; }
        public string HAWB_pg { get; set; }
        public string uin_class { get; set; }
        public string temperature { get; set; }
        public string pax_cao { get; set; }
        public Nullable<bool> is_temp { get; set; }
        public Nullable<decimal> charge_wt { get; set; }
    
        public virtual tbl_hawb_details tbl_hawb_details { get; set; }
    }
}
