//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Purchase_Narration
    {
        public int PnID { get; set; }
        public int NmID { get; set; }
        public int PurchaseID { get; set; }
        public string Subjobno { get; set; }
        public decimal Amount { get; set; }
    
        public virtual tbl_Narration_Master tbl_Narration_Master { get; set; }
        public virtual tbl_purchase tbl_purchase { get; set; }
    }
}
