//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_document_master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_document_master()
        {
            this.tbl_document_locker = new HashSet<tbl_document_locker>();
        }
    
        public int document_no { get; set; }
        public Nullable<int> activity_no { get; set; }
        public string document_name { get; set; }
        public Nullable<int> document_by { get; set; }
        public Nullable<System.DateTime> document_on { get; set; }
        public string document_type { get; set; }
    
        public virtual tbl_activity_master tbl_activity_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_document_locker> tbl_document_locker { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
