//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_config_master
    {
        public int config_no { get; set; }
        public string config_name { get; set; }
        public string config_url { get; set; }
        public string config_user_id { get; set; }
        public string config_pasword { get; set; }
        public Nullable<int> config_created_by { get; set; }
        public Nullable<System.DateTime> config_created_on { get; set; }
        public Nullable<int> config_updated_by { get; set; }
        public Nullable<System.DateTime> config_updated_on { get; set; }
    
        public virtual tbl_user_master tbl_user_master { get; set; }
        public virtual tbl_user_master tbl_user_master1 { get; set; }
    }
}
