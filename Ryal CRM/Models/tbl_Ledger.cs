//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Ledger
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_Ledger()
        {
            this.tbl_charge_master = new HashSet<tbl_charge_master>();
            this.tbl_charge_master1 = new HashSet<tbl_charge_master>();
        }
    
        public int Ledger_Id { get; set; }
        public string Ledger_Code { get; set; }
        public string Ledger_Name { get; set; }
        public Nullable<int> Ledger_UpdatedBy { get; set; }
        public Nullable<System.DateTime> Ledger_UpdatedOn { get; set; }
        public string Ledger_is_Bank_Receipt { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_charge_master> tbl_charge_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_charge_master> tbl_charge_master1 { get; set; }
        public virtual tbl_user_master tbl_user_master { get; set; }
    }
}
