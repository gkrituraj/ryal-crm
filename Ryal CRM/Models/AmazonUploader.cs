﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace RyalErp.Models
{
    public class AmazonUploader
    {
        public string SendMyFileToS3(Stream localFilePath, string bucketName, string subDirectoryInBucket, string fileNameInS3, string doc_pk)
        {
            try
            {
                IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);

                TransferUtility utility = new TransferUtility(client);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    request.BucketName = bucketName;
                }
                else
                { 
                    request.BucketName = bucketName + @"/" + subDirectoryInBucket;
                }

                string unique_file_name = doc_pk == null ? "" : doc_pk + "-" + "" + DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second + DateTime.Now.Millisecond + "." + fileNameInS3.Split('.')[1];
                
                request.Key = unique_file_name;
                request.InputStream = localFilePath;
                utility.Upload(request);                

                return "true|"+ unique_file_name;
            }
            catch(Exception ex)
            {
                return "false|"+ex.Message;
            }                        
        }

        public bool DeletMyFileToS3(Stream localFilePath, string bucketName, string subDirectoryInBucket, string fileNameInS3, string doc_pk, string already_attach_ref)
        {
            try
            {
                IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);

                TransferUtility utility = new TransferUtility(client);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    request.BucketName = bucketName;
                }
                else
                {                 
                    request.BucketName = bucketName + @"/" + subDirectoryInBucket;
                }

                string unique_file_name = already_attach_ref;
                request.Key = unique_file_name;

                client.DeleteObject(request.BucketName, request.Key);
                client.Dispose();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeletMyFileToS3(string bucketName, string subDirectoryInBucket, string fileNameInS3, string doc_pk, string already_attach_ref)
        {
            try
            {
                IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);

                TransferUtility utility = new TransferUtility(client);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    request.BucketName = bucketName;
                }
                else
                {
                    request.BucketName = bucketName + @"/" + subDirectoryInBucket;
                }

                string unique_file_name = already_attach_ref;
                request.Key = unique_file_name;

                client.DeleteObject(request.BucketName, request.Key);
                client.Dispose();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}