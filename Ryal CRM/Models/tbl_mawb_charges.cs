//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_mawb_charges
    {
        public int MAWB_charge_id { get; set; }
        public int MAWB_id { get; set; }
        public int MAWB_Charge_no { get; set; }
        public string MAWB_Charge_type { get; set; }
        public decimal MAWB_Charge_amount { get; set; }
        public decimal MAWB_Charge_tax_amount { get; set; }
        public string MAWB_Charge_remarks { get; set; }
        public string istaxable { get; set; }
        public Nullable<decimal> MAWB_Charge_rate { get; set; }
        public string MAWB_Charge_unit { get; set; }
        public Nullable<decimal> Final_Buying_Rate { get; set; }
        public Nullable<decimal> Final_Amount { get; set; }
        public Nullable<decimal> Hod_Buying_Rate { get; set; }
        public Nullable<decimal> Hod_Amount { get; set; }
    
        public virtual tbl_charge_master tbl_charge_master { get; set; }
        public virtual tbl_mawb_details tbl_mawb_details { get; set; }
    }
}
