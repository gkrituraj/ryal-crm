﻿
using Ryal_CRM.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;


namespace RyalErp.Models
{
    public class Save_Edit_Search_Delete_AuditLog
    {
        public int Module_No;
        public int SubModule_No;
        public DateTime ActionDate = DateTime.Now;
        public DateTime ActionTime = DateTime.Now;
        public int[] ActionType = { 1, 2, 3, 7, 8 };//1=Add,2=Edit,3=Delete,7=Search,8=Print

        private RyalErpDbEntities _context = null;

        public Save_Edit_Search_Delete_AuditLog()
        {
            _context = new RyalErpDbEntities();
        }
       


        public string GetCurrentFinancialYear()
        {
            int CurrentYear = DateTime.Today.Year;
            int PreviousYear = DateTime.Today.Year - 1;
            int NextYear = DateTime.Today.Year + 1;
            string PreYear = PreviousYear.ToString().Substring(2, 2);
            string NexYear = NextYear.ToString().Substring(2, 2);
            string CurYear = CurrentYear.ToString().Substring(2, 2);
            string FinYear = null;

            if (DateTime.Today.Month > 3)
                FinYear = CurYear + "-" + NexYear;
            else
                FinYear = PreYear + "-" + CurYear;
            return FinYear.Trim();
        }

        public string GetFinancialYear()
        {
            int CurrentYear = DateTime.Today.Year;
            int PreviousYear = DateTime.Today.Year - 1;
            int NextYear = DateTime.Today.Year + 1;
            string PreYear = PreviousYear.ToString().Substring(2, 2);
            string NexYear = NextYear.ToString().Substring(2, 2);
            string CurYear = CurrentYear.ToString().Substring(2, 2);
            string FinYear = null;

            if (DateTime.Today.Month > 3)
                FinYear = CurYear + "" + NexYear;
            else
                FinYear = PreYear + "" + CurYear;
            return FinYear.Trim();
        }


        public  string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 10000000) > 0)
            {
                words += NumberToWords(number / 10000000) + " Crore ";
                number %= 10000000;
            }

            if ((number / 100000) > 0)
            {
                words += NumberToWords(number / 100000) + " lacs ";
                number %= 100000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += " ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += " " + unitsMap[number % 10];
                }
            }

            return words;
        }



    }


}