//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ryal_CRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_add_phone
    {
        public int add_phone_no { get; set; }
        public Nullable<int> add_contact_no { get; set; }
        public string add_phone_label { get; set; }
        public string add_phone_type { get; set; }
        public string add_mobile_country_code { get; set; }
        public string add_phone_isd_code { get; set; }
        public string add_phone_mobile_no { get; set; }
        public Nullable<int> add_ext { get; set; }
        public Nullable<System.DateTime> updated_on { get; set; }
        public Nullable<int> updated_by { get; set; }
        public bool is_default { get; set; }
    
        public virtual tbl_address_book tbl_address_book { get; set; }
    }
}
