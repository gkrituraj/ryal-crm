﻿using AESEncryptDecrypt;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using CRMAdmin.Models;
using OfficeOpenXml;
using Ryal_CRM.Models;
using RyalErp.Models;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace Ryal_CRM.Controllers
{
    public class CustomerPortalController : Controller
    {

        private RyalErpDbEntities _context = null;
        public CustomerPortalController()
        {
            _context = new RyalErpDbEntities();
        }
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Login Customer
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            if (TempData["alertMessage"] != null)
            {
                ViewBag.alertMessage = TempData["alertMessage"].ToString();
            }
            Session.Clear();
            return View();
        }
        [HttpPost]
        public ActionResult Login(string user_login_id, string user_login_password)
        {
            try
            {
                var userlist = _context.tbl_crm_user_master.ToList();
                AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();

                var enc_password = encryptDecrypt.EncryptString(user_login_password);

                var admins = _context.tbl_crm_user_master.Where(x => x.crm_user_id == user_login_id && x.crm_user_password == enc_password).FirstOrDefault();

                if (admins != null)
                {
                    Session["crm_user_no"] = admins.crm_user_no;
                    var UserActive = _context.tbl_crm_user_master.Where(x => x.crm_user_id == user_login_id).FirstOrDefault();
                    Session["add_contact_no"] = UserActive.crm_user_add_contact_no;
                    if (UserActive != null)
                    {
                        Session["Customer_User"] = UserActive.tbl_address_book.add_contact_fname + " " + UserActive.tbl_address_book.add_contact_lname.Substring(0, 1);
                        Session["Img"] = UserActive.tbl_address_book.add_refrence_id;
                        Session["user_login_id"] = "user_login_id";

                        var company_business_unit = UserActive.tbl_crm_business_unit.Select(x => x.tbl_company_business_unit).ToList();

                        List<SelectListItem> business_name = new List<SelectListItem>();
                        for (int i = 0; i < company_business_unit.Count(); i++)
                        {

                            var Text = company_business_unit[i].unit_name;
                            var value = company_business_unit[i].unit_no;
                            business_name.Add(new SelectListItem { Text = Text, Value = value.ToString() });
                        }
                        Session["unit_name"] = new SelectList(business_name);

                        Save_Edit_Search_Delete_AuditLog fifance = new Save_Edit_Search_Delete_AuditLog();
                        var finyear = fifance.GetFinancialYear();
                        Session["financialyear"] = fifance.GetCurrentFinancialYear();
                        Session["financial_year"] = fifance.GetFinancialYear();
                        var preyear = Convert.ToInt32(finyear.Substring(2, 2));
                        Session["fullfinancial"] = "20" + (preyear - 1) + "-20" + finyear.Substring(2, 2);
                        Session["docno"] = "0";
                        Session["Search_By"] = "";
                        Session["Seach_parameter"] = "";
                        Session["Stauts"] = "";
                        Session["From_Dtt"] = "";
                        Session["To_Dtt"] = "";

                        var dept = "";
                        if (admins.crm_user_AE == true && dept == "")
                            dept = "AE";
                        if (admins.crm_user_AI == true && dept == "")
                            dept = "AI";
                        if (admins.crm_user_SE == true && dept == "")
                            dept = "SE";
                        if (admins.crm_user_SI == true && dept == "")
                            dept = "SI";
                        if (dept == "")
                            dept = "AE";
                        Session["dept"] = dept;
                        return RedirectToAction("Dashboard", "CustomerPortal");
                    }
                    else
                    {
                        TempData["alertMessage"] = "User is not Active. Please check and try again.";
                        return RedirectToAction("Login", "CustomerPortal");
                    }
                }
                else
                {
                    TempData["alertMessage"] = "Invalid credentials. Please check and try again.";
                    return RedirectToAction("Login", "CustomerPortal");
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter _testData = new StreamWriter(Server.MapPath("~/App_Data/errors/login.txt"), true))
                {
                    _testData.WriteLine("Message: " + ex.Message + "<br/>" + Environment.NewLine + "StackTrace: " + ex.StackTrace + "<br/>" + Environment.NewLine + "DateTime: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt").Replace('-', '/') +
                        "<br/>" + Environment.NewLine + "<br/>" + Environment.NewLine); // Write the file.
                    _testData.Dispose();
                }
                TempData["alertMessage"] = ex.Message;
                return RedirectToAction("Login", "CustomerPortal");
            }
        }

        [SessionTimeOut]
        public ActionResult Dashboard()
        {
            var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
            Session["user_table"] = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).ToList();
            string dept = Session["dept"].ToString();
            switch (dept)
            {
                case "AE": return RedirectToAction("AE_DB");
                case "AI": return RedirectToAction("AI_DB");
                case "SE": return RedirectToAction("SE_DB");
                case "SI": return RedirectToAction("SI_DB");
                default: return RedirectToAction("Index", "Home");
            }
        }


        ///// <summary>
        ///// Dashboard listing and Company  wise
        ///// </summary>
        ///// <returns></returns>
        //[SessionTimeOut]
        //public ActionResult Dashboard1()
        //{
        //    string dept = Session["dept"].ToString();
        //    switch (dept)
        //    {
        //        case "AE": return RedirectToAction("AE_DB");
        //        case "AI": return RedirectToAction("AI_DB");
        //        case "SE": return RedirectToAction("SE_DB");
        //        case "SI": return RedirectToAction("SI_DB");
        //    }

        //    CustomerModal user = new CustomerModal();
        //    user.tbl_Crm_User_s = _context.tbl_crm_user_master.ToList();
        //    var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
        //    List<Customer> Cost_details = new List<Customer>();

        //    var userInDB = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).FirstOrDefault();
        //    List<int?> crm_unt_business_unit_no = new List<int?>();
        //    var Bu_unit_no = userInDB.tbl_crm_business_unit.Select(x => x.crm_unt_business_unit_no).ToList();
        //    crm_unt_business_unit_no.AddRange(Bu_unit_no);
        //    if (Session["docno"].ToString() != "0")
        //    {
        //        crm_unt_business_unit_no = new List<int?>();
        //        crm_unt_business_unit_no.Add(Convert.ToInt32(Session["docno"].ToString()));
        //    }

        //    var priviewfinyear = Session["financialyear"].ToString();//financial year
        //    var newfinanceyear = priviewfinyear.Split('-')[1];


        //    switch (dept)
        //    {
        //        case "AE":
        //            foreach (var unit_no in crm_unt_business_unit_no)
        //            {
        //                var subjob = _context.Tbl_sub_job.Where(s => s.Shipper_company == unit_no.ToString() && s.AXF_Deleted_SubJob != "Yes" && s.Reference_type == 0 && s.Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        subjob = _context.Tbl_sub_job.Where(s => s.Reference_company == unit_no && s.AXF_Deleted_SubJob != "Yes" && s.Reference_type == 1 && s.Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        subjob = _context.Tbl_sub_job.Where(s => s.Reference_company == unit_no && s.AXF_Deleted_SubJob != "Yes" && s.Reference_type == 2 && s.Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;

        //                }
        //                foreach (var item in subjob)
        //                {
        //                    Customer Customer = new Customer();

        //                    Customer.Sub_job_no = item.Sub_job_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.Created_on_datetime);

        //                    Customer.Sub_job_id = item.Sub_job_id;
        //                    Customer.Mode = "";
        //                    Customer.Master_job_id = item.Master_job_id;
        //                    Customer.JobType = item.Tbl_master_job.Master_job_type;
        //                    Customer.Department = "AEF";
        //                    var Chkclrjobno = item.Tbl_AirClearance_subjob.Where(x => x.AEC_Deleted_SubJob != "Yes").ToList();

        //                    if (Customer.JobType.ToString() == "Direct")
        //                    {
        //                        Customer.Shppr_Cnee = item.Tbl_master_job.tbl_mawb_details.Select(x => x.MAWB_Consignee_Name.ToUpper()).FirstOrDefault();
        //                    }
        //                    else
        //                    {
        //                        Customer.Shppr_Cnee = item.tbl_hawb_details.Select(x => x.HAWB_Consignee_Name.ToUpper()).FirstOrDefault();
        //                    }



        //                    List<Clr_Det> clrlist = new List<Clr_Det>();
        //                    if (Chkclrjobno.Count > 0)
        //                    {
        //                        foreach (var clrjob in Chkclrjobno)
        //                        {
        //                            Clr_Det Clr_det = new Clr_Det();
        //                            Clr_det.Clr_job_no = clrjob.AirClearance_Subjob_no;
        //                            Clr_det.Clr_job_id = clrjob.AirClearance_Subjob_id.ToString();
        //                            Clr_det.PKG = Convert.ToInt32(clrjob.AirClearance_Total_packages);
        //                            Clr_det.GRWT = Convert.ToDecimal(clrjob.AirClearance_Total_weight).ToString();
        //                            if (string.IsNullOrEmpty(clrjob.AEC_SINV_NO) && !string.IsNullOrEmpty(clrjob.AECRoyalXML))
        //                            {
        //                                clrjob.AEC_SINV_NO = GetInvNo(clrjob.AECRoyalXML, "AEC");
        //                            }

        //                            if (string.IsNullOrEmpty(clrjob.AEC_SINV_NO))
        //                            {
        //                                Clr_det.Party_InvocieNo = Customer.Party_InvocieNo == null ? "" : Customer.Party_InvocieNo;
        //                            }
        //                            else
        //                            {
        //                                Clr_det.Party_InvocieNo = clrjob.AEC_SINV_NO;
        //                                Customer.Party_InvocieNo = Clr_det.Party_InvocieNo;
        //                            }

        //                            Clr_det.sb_be_no = clrjob.shipping_bill_no;

        //                            if (string.IsNullOrEmpty(Clr_det.sb_be_no))
        //                            {
        //                                Clr_det.sb_be_no = Customer.sb_be_no == null ? "" : Customer.sb_be_no;
        //                            }
        //                            else
        //                            {
        //                                Clr_det.sb_be_no = clrjob.shipping_bill_no;
        //                                Customer.sb_be_no = Clr_det.sb_be_no;
        //                            }
        //                            Clr_det.Doctype = clrjob.AirClearance_Commodity_type;//clrjob.cust_doc_type;
        //                            Clr_det.fileref = clrjob.shipping_pdf;
        //                            Clr_det.department = "AEC";
        //                            clrlist.Add(Clr_det);
        //                        }
        //                    }
        //                    Customer.Clr_det = clrlist;

        //                    if (Chkclrjobno.Count == 0)
        //                    {
        //                        Customer.PKG = Convert.ToInt32(item.Total_packages);
        //                        Customer.GRWT = Convert.ToDecimal(item.Total_weight).ToString();
        //                    }
        //                    var airport_name = item.tbl_airport_master.airport_name;
        //                    var mastercount = item.Tbl_master_job.tbl_mawb_details.Select(x => x.MAWB_activity_status).ToList();
        //                    try
        //                    {
        //                        if (mastercount.Count > 0)
        //                        {
        //                            var masterstatus = mastercount[0];
        //                            if (masterstatus == true)
        //                                if (masterstatus == true)
        //                                {
        //                                    Customer.STATUS = "1";
        //                                }
        //                                else
        //                                {
        //                                    Customer.STATUS = "2";
        //                                }
        //                        }
        //                        else
        //                        {
        //                            var subjobcount = subjob;
        //                            if (subjobcount.Count > 0)
        //                            {
        //                                int z = 0;
        //                                for (; z < subjobcount.Count;)
        //                                {
        //                                    var hawbstaus = subjobcount[z].tbl_hawb_details.Select(x => x.HAWB_activity_status).FirstOrDefault();
        //                                    if (hawbstaus == true)
        //                                    {
        //                                        Customer.STATUS = "3";
        //                                        z++;
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        Customer.STATUS = "4";
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {

        //                                Customer.STATUS = "5";
        //                            }
        //                        }
        //                    }
        //                    catch (Exception)
        //                    {
        //                        Customer.STATUS = "6";

        //                    }

        //                    Customer.Branch = item.Executing_branch;
        //                    switch (Customer.Branch.ToUpper())
        //                    {
        //                        case "MUMBAI":
        //                            Customer.pol_code = "BOM";
        //                            break;
        //                        case "AHMEDABAD":
        //                            Customer.pol_code = "AMD";
        //                            break;
        //                        case "CHENNAI":
        //                            Customer.pol_code = "MAA";
        //                            break;
        //                    }

        //                    Customer.Destination = airport_name.ToUpper().ToString();
        //                    Customer.pod_code = item.tbl_airport_master.airport_code;
        //                    Customer.is_tracking_enabled = item.Tbl_master_job.tbl_mawb_details.Select(x => x.is_tracking_enabled).FirstOrDefault();
        //                    Customer.mawb_curr_track_status = item.Tbl_master_job.tbl_mawb_details.Select(x => x.mawb_curr_track_status).FirstOrDefault();
        //                    Customer.HAWB = item.tbl_hawb_details.Select(x => x.HAWB_stock_no).FirstOrDefault();
        //                    var mawb = item.Tbl_master_job.tbl_mawb_details.Select(x => x.MAWB_stock_no).FirstOrDefault();
        //                    var airlinedetails = item.Tbl_master_job.tbl_mawb_details.Select(x => x.tbl_mawb_airline).ToList();
        //                    if (airlinedetails.Count > 0)
        //                    {
        //                        Customer.ETD = airlinedetails[0].Select(x => x.MAWB_Departure_date).FirstOrDefault();
        //                        var compno = airlinedetails[0].Select(x => x.MAWB_Airline_company_id).FirstOrDefault();
        //                        var flightno = airlinedetails[0].Select(x => x.MAWB_Airline_flight_no).FirstOrDefault();
        //                        Customer.flightno = _context.tbl_company_master.Where(x => x.company_no == compno).Select(x => x.Ar_Code).FirstOrDefault() + "-" + flightno;
        //                        if (airlinedetails.Count > 1)
        //                        {
        //                            for (int l = 0; l < airlinedetails.Count; l++)
        //                            {
        //                                Customer.ETA = airlinedetails[l].Select(x => x.MAWB_Departure_date).FirstOrDefault();
        //                            }
        //                        }
        //                        else
        //                        {
        //                            Customer.ETA = Customer.ETD;
        //                        }
        //                    }
        //                    var aec_mawb = "";
        //                    try
        //                    {
        //                        if (item.Sub_job_id > 0)
        //                        {
        //                            var mjob_id = item.Master_job_id;
        //                            var stock_num = _context.tbl_mawb_details.Where(s => s.MAWB_master_job_id == mjob_id).Select(s => s.MAWB_stock_no).FirstOrDefault();
        //                            if (!string.IsNullOrEmpty(stock_num))
        //                            {
        //                                var unitnum = _context.tbl_mawb_details.Where(s => s.MAWB_master_job_id == mjob_id).Select(s => s.MAWB_AirLine_code).FirstOrDefault();
        //                                var prefix = _context.tbl_company_business_unit.Where(s => s.unit_no.ToString() == unitnum).Select(s => s.tbl_company_master.Prefix).FirstOrDefault();
        //                                aec_mawb = prefix + "-" + stock_num.Substring(0, 4) + " " + stock_num.Substring(4, 4);
        //                            }
        //                        }
        //                    }
        //                    catch (Exception)
        //                    {
        //                        aec_mawb = "000-0000 0000";
        //                    }
        //                    Customer.MAWB = aec_mawb;
        //                    if (string.IsNullOrEmpty(Customer.HAWB))
        //                    {
        //                        Customer.HAWB = Customer.HAWB == null ? "" : Customer.MAWB;
        //                    }
        //                    else
        //                    {
        //                        Customer.HAWB = Customer.HAWB;
        //                    }

        //                    if (string.IsNullOrEmpty(Customer.MAWB))
        //                    {
        //                        Customer.MAWB = Customer.MAWB == null ? "" : Customer.HAWB;
        //                    }
        //                    else
        //                    {
        //                        Customer.MAWB = Customer.MAWB;
        //                    }

        //                    Cost_details.Add(Customer);

        //                }

        //                var clrsubjob = _context.Tbl_AirClearance_subjob.Where(s => s.AirClearance_Shipper_company == unit_no.ToString() && s.AEC_Deleted_SubJob != "Yes" && s.AirClearance_Reference_type == 0 && s.AirExport_Subjob_id == null && s.AirClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        clrsubjob = _context.Tbl_AirClearance_subjob.Where(s => s.AirClearance_Reference_company == unit_no && s.AEC_Deleted_SubJob != "Yes" && s.AirClearance_Reference_type == 1 && s.AirExport_Subjob_id == null && s.AirClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        clrsubjob = _context.Tbl_AirClearance_subjob.Where(s => s.AirClearance_Reference_company == unit_no && s.AEC_Deleted_SubJob != "Yes" && s.AirClearance_Reference_type == 2 && s.AirExport_Subjob_id == null && s.AirClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                }
        //                foreach (var item in clrsubjob)
        //                {
        //                    Customer Customer = new Customer();
        //                    Customer.Sub_job_no = item.AirClearance_Subjob_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.AirClearance_Created_on_datetime);
        //                    Customer.Sub_job_id = item.AirClearance_Subjob_id;
        //                    Customer.Master_job_id = Convert.ToInt32(item.AirClearance_Master_job_id);
        //                    Customer.JobType = "con";
        //                    Customer.Mode = "";
        //                    Customer.Department = "AEC";
        //                    Customer.Shppr_Cnee = item.AirClearance_Consignee_company.ToUpper();
        //                    Customer.JobType = item.AirClearance_Commodity_type;//item.cust_doc_type;
        //                    Customer.sb_be_no = item.shipping_bill_no;

        //                    Customer.sb_be_no = item.shipping_bill_no;
        //                    Customer.fileref = item.shipping_pdf;
        //                    Customer.PKG = Convert.ToInt32(item.AirClearance_Total_packages);
        //                    Customer.GRWT = Convert.ToDecimal(item.AirClearance_Total_weight).ToString();
        //                    Customer.Destination = item.tbl_airport_master.airport_name.ToUpper();
        //                    Customer.pod_code = item.tbl_airport_master.airport_code.ToUpper();


        //                    if (!string.IsNullOrEmpty(item.shipping_pdf)) Customer.STATUS = "1";
        //                    else Customer.STATUS = "2";

        //                    switch (item.AEC_segment_no)
        //                    {
        //                        case 0:
        //                            Customer.Branch = "MUMBAI";
        //                            Customer.pol_code = "BOM";
        //                            break;
        //                        case 1:
        //                            Customer.Branch = "CHENNAI";
        //                            Customer.pol_code = "MAA";
        //                            break;
        //                        case 2:
        //                            Customer.Branch = "AHMEDABAD";
        //                            Customer.pol_code = "AMD";
        //                            break;
        //                    }

        //                    Customer.HAWB = "";
        //                    Customer.MAWB = "";
        //                    if (string.IsNullOrEmpty(item.AEC_SINV_NO) && !string.IsNullOrEmpty(item.AECRoyalXML))
        //                    {
        //                        item.AEC_SINV_NO = GetInvNo(item.AECRoyalXML, "AEC");
        //                    }

        //                    Customer.Party_InvocieNo = item.AEC_SINV_NO;
        //                    List<Clr_Det> clrlist = new List<Clr_Det>();
        //                    Clr_Det Clr_det = new Clr_Det();
        //                    Clr_det.Clr_job_no = item.AirClearance_Subjob_no;
        //                    Clr_det.Clr_job_id = item.AirClearance_Subjob_id.ToString();
        //                    Clr_det.PKG = Convert.ToInt32(item.AirClearance_Total_packages);
        //                    Clr_det.GRWT = Convert.ToDecimal(item.AirClearance_Total_weight).ToString();
        //                    Clr_det.Party_InvocieNo = item.AEC_SINV_NO;


        //                    Clr_det.sb_be_no = item.shipping_bill_no;
        //                    Clr_det.Doctype = item.AirClearance_Commodity_type;//item.cust_doc_type;
        //                    Clr_det.fileref = item.shipping_pdf;
        //                    clrlist.Add(Clr_det);
        //                    Customer.Clr_det = clrlist;
        //                    Customer.Department = "AEC";


        //                    Cost_details.Add(Customer);

        //                }

        //            }
        //            break;
        //        case "AI":
        //            foreach (var unit_no in crm_unt_business_unit_no)
        //            {
        //                var subjob = _context.Tbl_AirImport_sub_job.Where(s => s.AirImport_Shipper_company == unit_no.ToString() && s.AIF_Deleted_SubJob != "Yes" && s.AirImport_Reference_type == 0 && s.AirImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        subjob = _context.Tbl_AirImport_sub_job.Where(s => s.AirImport_Reference_company == unit_no && s.AIF_Deleted_SubJob != "Yes" && s.AirImport_Reference_type == 1 && s.AirImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        subjob = _context.Tbl_AirImport_sub_job.Where(s => s.AirImport_Reference_company == unit_no && s.AIF_Deleted_SubJob != "Yes" && s.AirImport_Reference_type == 2 && s.AirImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;

        //                }
        //                foreach (var item in subjob)
        //                {
        //                    Customer Customer = new Customer();
        //                    Customer.Sub_job_no = item.AirImport_Sub_job_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.AirImport_Created_on_datetime);

        //                    Customer.Sub_job_id = item.AirImport_Sub_job_id;
        //                    Customer.Master_job_id = item.AirImport_Master_job_id;
        //                    Customer.JobType = "CON";
        //                    Customer.Mode = "";
        //                    Customer.Shppr_Cnee = item.AirImport_Consignee_company.ToUpper();
        //                    Customer.PKG = Convert.ToInt32(item.AirImport_Package_Count);
        //                    Customer.GRWT = Convert.ToDecimal(item.AirImport_Gross_Weight).ToString();
        //                    Customer.Department = "AIF";
        //                    var airport_name = item.tbl_airport_master.airport_name;
        //                    Customer.pod_code = item.tbl_airport_master.airport_code;
        //                    var mastercount = item.Tbl_AirImport_master_job.AirImport_Activity_Status;
        //                    Customer.ETD = item.Tbl_AirImport_master_job.AirImport_ETD;
        //                    Customer.ETA = item.Tbl_AirImport_master_job.AirImport_ETA;
        //                    var chkarrival = _context.Tbl_AirImport_Arrival.Where(x => x.airimport_subjobid == item.AirImport_Sub_job_id).Select(x => x.arrival_date).FirstOrDefault();
        //                    if (chkarrival != null)
        //                        Customer.ETA = chkarrival;

        //                    if (mastercount == true)
        //                    {
        //                        Customer.STATUS = "1";
        //                    }
        //                    else if (mastercount == false)
        //                    {
        //                        Customer.STATUS = "2";
        //                    }
        //                    else
        //                    {
        //                        Customer.STATUS = "3";
        //                    }

        //                    switch (item.AIF_segment_no)
        //                    {
        //                        case 0:
        //                            Customer.Branch = "MUMBAI";
        //                            Customer.pol_code = "BOM";
        //                            break;
        //                        case 1:
        //                            Customer.Branch = "CHENNAI";
        //                            Customer.pol_code = "MAA";
        //                            break;
        //                        case 2:
        //                            Customer.Branch = "AHMEDABAD";
        //                            Customer.pol_code = "AMD";
        //                            break;
        //                    }

        //                    Customer.Destination = airport_name.ToUpper().ToString();
        //                    Customer.HAWB = item.AirImport_Hawb_No;
        //                    var mawb = item.Tbl_AirImport_master_job.AirImport_Mawb_No;
        //                    Customer.MAWB = mawb;
        //                    if (Customer.HAWB == null)
        //                    {
        //                        Customer.HAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.HAWB = Customer.HAWB.ToUpper();
        //                    }
        //                    if (Customer.MAWB == null)
        //                    {
        //                        Customer.MAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.MAWB = Customer.MAWB;
        //                    }
        //                    Cost_details.Add(Customer);
        //                }

        //                var clrsubjob = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Shipper_company == unit_no.ToString() && s.AIC_Deleted_SubJob != "Yes" && s.AirImpClearance_Reference_type == 0 && s.AirImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        clrsubjob = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Reference_company == unit_no && s.AIC_Deleted_SubJob != "Yes" && s.AirImpClearance_Reference_type == 1 && s.AirImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        clrsubjob = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Reference_company == unit_no && s.AIC_Deleted_SubJob != "Yes" && s.AirImpClearance_Reference_type == 2 && s.AirImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                }
        //                foreach (var item in clrsubjob)
        //                {
        //                    Customer Customer = new Customer();
        //                    Customer.Sub_job_no = item.AirImpClearance_Subjob_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.AirImpClearance_Created_on_datetime);
        //                    Customer.Sub_job_id = item.AirImpClearance_Subjob_id;
        //                    Customer.fileref = item.billof_entry_pdf;
        //                    Customer.Master_job_id = Convert.ToInt32(item.AirImpClearance_Master_job_id);
        //                    Customer.JobType = "con";
        //                    Customer.Mode = "";
        //                    Customer.Shppr_Cnee = item.AirImpClearance_Consignee_company.ToUpper();
        //                    Customer.PKG = Convert.ToInt32(item.AirImpClearance_Total_packages);
        //                    Customer.GRWT = Convert.ToDecimal(item.AirImpClearance_Total_weight).ToString();
        //                    var airport_name = item.tbl_airport_master.airport_name;
        //                    Customer.pod_code = item.tbl_airport_master.airport_code;
        //                    if (!string.IsNullOrEmpty(item.billof_entry_pdf)) Customer.STATUS = "1";
        //                    else Customer.STATUS = "2";
        //                    Customer.Department = "AIC";
        //                    if (Customer.JobType.ToString() == "Direct")
        //                    {
        //                        Customer.Party_InvocieNo = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.Party_InvocieNo = "";

        //                    }


        //                    switch (item.AIC_segment_no)
        //                    {
        //                        case 0:
        //                            Customer.Branch = "MUMBAI";
        //                            Customer.pol_code = "BOM";
        //                            break;
        //                        case 1:
        //                            Customer.Branch = "CHENNAI";
        //                            Customer.pol_code = "MAA";
        //                            break;
        //                        case 2:
        //                            Customer.Branch = "AHMEDABAD";
        //                            Customer.pol_code = "AMD";
        //                            break;
        //                    }

        //                    Customer.Destination = airport_name.ToUpper();
        //                    Customer.HAWB = item.AirImpClearance_hawb;

        //                    //if (string.IsNullOrEmpty(Customer.HAWB))
        //                    //{
        //                    //    Customer.HAWB = Customer.HAWB == null ? "" : Customer.HAWB;
        //                    //}

        //                    if (Customer.HAWB == null)
        //                    {
        //                        Customer.HAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.HAWB = Customer.HAWB.ToUpper();
        //                    }

        //                    var aic_mawb = item.AirImpClearance_mawb;
        //                    Customer.MAWB = aic_mawb;
        //                    if (Customer.MAWB == null)
        //                    {
        //                        Customer.MAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.MAWB = Customer.MAWB;
        //                    }
        //                    //if (string.IsNullOrEmpty(Customer.MAWB))
        //                    //{
        //                    //    Customer.MAWB = Customer.MAWB == null ? "" : Customer.MAWB;
        //                    //}
        //                    Customer.Doctype = item.AirImpClearance_Commodity_type;//item.cust_doc_type;
        //                    Customer.sb_be_no = item.billof_entry_no;
        //                    if (string.IsNullOrEmpty(item.AIC_SINV_NO) && !string.IsNullOrEmpty(item.AICRoyalXML))
        //                    {
        //                        item.AIC_SINV_NO = GetInvNo(item.AICRoyalXML, "AIC");
        //                    }
        //                    Customer.Party_InvocieNo = item.AIC_SINV_NO;



        //                    Cost_details.Add(Customer);

        //                }
        //            }
        //            break;
        //        case "SE":
        //            foreach (var unit_no in crm_unt_business_unit_no)
        //            {
        //                var subjob = _context.Tbl_Shiping_sub_job.Where(s => s.shipping_Shipper_company == unit_no.ToString() && s.SXF_Deleted_SubJob != "Yes" && s.shipping_Reference_type == 0 && s.shipping_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        subjob = _context.Tbl_Shiping_sub_job.Where(s => s.shipping_Reference_company == unit_no && s.SXF_Deleted_SubJob != "Yes" && s.shipping_Reference_type == 1 && s.shipping_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        subjob = _context.Tbl_Shiping_sub_job.Where(s => s.shipping_Reference_company == unit_no && s.SXF_Deleted_SubJob != "Yes" && s.shipping_Reference_type == 2 && s.shipping_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;

        //                }
        //                foreach (var item in subjob)
        //                {
        //                    Customer Customer = new Customer();
        //                    Customer.Sub_job_no = item.shipping_Sub_job_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.shipping_Created_on_datetime);
        //                    Customer.Sub_job_id = item.shipping_Sub_job_id;
        //                    Customer.Mode = item.shipping_shipment_type;
        //                    Customer.Master_job_id = item.shipping_Master_job_id;
        //                    Customer.Shppr_Cnee = item.shipping_Consignee_company.ToUpper();
        //                    Customer.JobType = item.Tbl_shipping_master_job.shipping_Master_job_type;
        //                    if (Customer.JobType == "Direct")
        //                    {
        //                        Customer.vessel_voyag = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Vessel_Name).FirstOrDefault() + " - " + item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Voyage_No).FirstOrDefault();
        //                        //var shiptype = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_TypeOfMove).FirstOrDefault();

        //                        var containerdet = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.tbl_mbl_container.ToList()).FirstOrDefault();
        //                        if (containerdet != null)
        //                        {
        //                            foreach (var container in containerdet)
        //                            {
        //                                Customer.Container_no = container.MBL_Container_No + "-";
        //                                if (Customer.Mode == "FCL")
        //                                {
        //                                    Customer.Container_no += container.MBL_Container_Type + "-";
        //                                }
        //                                else
        //                                {
        //                                    Customer.vol = container.MBL_Container_Type + "-" + item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Volume).FirstOrDefault();
        //                                    break;
        //                                }
        //                            }
        //                            if (!string.IsNullOrEmpty(Customer.vol))
        //                                Customer.vol = Customer.vol.TrimEnd('-');
        //                            if (!string.IsNullOrEmpty(Customer.Container_no))
        //                                Customer.Container_no = Customer.Container_no.TrimEnd('-');
        //                        }
        //                        else
        //                        {
        //                            if (Customer.Mode != "FCL")
        //                                Customer.vol = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Volume).FirstOrDefault();
        //                        }

        //                    }
        //                    else
        //                    {
        //                        Customer.vessel_voyag = item.tbl_hbl_details.Select(x => x.HBL_Vessel_Name).FirstOrDefault() + " - " + item.tbl_hbl_details.Select(x => x.HBL_Voyage_No).FirstOrDefault();

        //                        var shiptype = item.tbl_hbl_details.Select(x => x.HBL_TypeOfMove).FirstOrDefault();

        //                        var containerdet = item.tbl_hbl_details.Select(x => x.tbl_hbl_container.ToList()).FirstOrDefault();
        //                        if (containerdet != null)
        //                        {
        //                            foreach (var container in containerdet)
        //                            {
        //                                Customer.Container_no += container.HBL_Container_No + "-";
        //                                if (shiptype == "FCL/FCL")
        //                                {
        //                                    Customer.vol += container.HBL_Container_Type + "-";
        //                                }
        //                                else
        //                                {
        //                                    Customer.vol = item.tbl_hbl_details.Select(x => x.HBL_Volume).FirstOrDefault();
        //                                    break;
        //                                }
        //                            }
        //                            if (!string.IsNullOrEmpty(Customer.vol))
        //                                Customer.vol = Customer.vol.TrimEnd('-');
        //                            if (!string.IsNullOrEmpty(Customer.Container_no))
        //                                Customer.Container_no = Customer.Container_no.TrimEnd('-');
        //                        }
        //                        else
        //                        {
        //                            if (shiptype != "FCL/FCL")
        //                                Customer.vol = item.tbl_hbl_details.Select(x => x.HBL_Volume).FirstOrDefault();
        //                        }
        //                    }

        //                    var Chkclrjobno = item.Tbl_SeaClearance_subjob.Where(x => x.SEC_Deleted_SubJob != "Yes").ToList();
        //                    List<Clr_Det> clrlist = new List<Clr_Det>();
        //                    if (Chkclrjobno.Count > 0)
        //                    {
        //                        foreach (var clrjob in Chkclrjobno)
        //                        {
        //                            Clr_Det Clr_det = new Clr_Det();
        //                            Clr_det.Clr_job_no = clrjob.SeaClearance_Subjob_no;
        //                            if (string.IsNullOrEmpty(clrjob.SEC_SINV_NO) && !string.IsNullOrEmpty(clrjob.SECRoyalXML))
        //                            {
        //                                clrjob.SEC_SINV_NO = GetInvNo(clrjob.SECRoyalXML, "SEC");
        //                            }
        //                            if (string.IsNullOrEmpty(clrjob.SEC_SINV_NO))
        //                            {
        //                                Clr_det.Party_InvocieNo = Customer.Party_InvocieNo == null ? "" : Customer.Party_InvocieNo;
        //                            }
        //                            else
        //                            {
        //                                Clr_det.Party_InvocieNo = clrjob.SEC_SINV_NO;
        //                                Customer.Party_InvocieNo = Clr_det.Party_InvocieNo;
        //                            }
        //                            Clr_det.sb_be_no = clrjob.shipping_bill_no;
        //                            if (string.IsNullOrEmpty(Clr_det.sb_be_no))
        //                            {
        //                                Clr_det.sb_be_no = Customer.sb_be_no == null ? "" : Customer.sb_be_no;
        //                            }
        //                            else
        //                            {
        //                                Clr_det.sb_be_no = clrjob.shipping_bill_no;
        //                                Customer.sb_be_no = Clr_det.sb_be_no;
        //                            }
        //                            Clr_det.Clr_job_id = clrjob.SeaClearance_Subjob_id.ToString();
        //                            Clr_det.PKG = Convert.ToInt32(clrjob.SeaClearance_Total_packages);
        //                            Clr_det.GRWT = Convert.ToDecimal(clrjob.SeaClearance_Total_weight).ToString();

        //                            Clr_det.Doctype = clrjob.SeaClearance_Commodity_type;//clrjob.cust_doc_type;
        //                            Clr_det.fileref = clrjob.shipping_pdf;
        //                            Clr_det.department = "SEC";
        //                            clrlist.Add(Clr_det);
        //                        }
        //                    }
        //                    Customer.Clr_det = clrlist;

        //                    if (Chkclrjobno.Count == 0)
        //                    {
        //                        Customer.PKG = Convert.ToInt32(item.shipping_Total_packages);
        //                        Customer.GRWT = Convert.ToDecimal(item.shipping_Total_weight).ToString();
        //                    }


        //                    var airport_name = item.tbl_seaport_master1.seaport_name;
        //                    Customer.Branch = item.tbl_seaport_master.seaport_name;

        //                    Customer.pod_code = item.tbl_seaport_master1.seaport_code;
        //                    Customer.pol_code = item.tbl_seaport_master.seaport_code;
        //                    Customer.pol_code = item.tbl_seaport_master1.seaport_code;
        //                    Customer.pol_code = item.tbl_seaport_master.seaport_code;
        //                    Customer.Department = "SEF";
        //                    if (Customer.JobType.ToString() == "Direct")
        //                    {
        //                        if (Chkclrjobno.Count == 0)
        //                            Customer.Party_InvocieNo = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_exp_invoice_no).FirstOrDefault();
        //                        Customer.ETA = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_ETA).FirstOrDefault();
        //                        Customer.ETD = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_ETD).FirstOrDefault();
        //                    }
        //                    else
        //                    {
        //                        if (Chkclrjobno.Count == 0)
        //                            Customer.Party_InvocieNo = item.tbl_hbl_details.Select(x => x.HBL_exp_invoice_no).FirstOrDefault();
        //                        Customer.ETA = item.tbl_hbl_details.Select(x => x.HBL_ETA).FirstOrDefault();
        //                        Customer.ETD = item.tbl_hbl_details.Select(x => x.HBL_ETD).FirstOrDefault();


        //                    }

        //                    var mastercount = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_activity_status).ToList();
        //                    try
        //                    {
        //                        if (mastercount.Count > 0)
        //                        {
        //                            var masterstatus = mastercount[0];
        //                            if (masterstatus == true)
        //                                if (masterstatus == true)
        //                                {
        //                                    Customer.STATUS = "1";
        //                                }
        //                                else
        //                                {
        //                                    Customer.STATUS = "2";
        //                                }
        //                        }
        //                        else
        //                        {
        //                            var subjobcount = subjob;
        //                            if (subjobcount.Count > 0)
        //                            {
        //                                int z = 0;
        //                                for (; z < subjobcount.Count;)
        //                                {
        //                                    var hawbstaus = subjobcount[z].tbl_hbl_details.Select(x => x.HBL_activity_status).FirstOrDefault();
        //                                    if (hawbstaus == true)
        //                                    {
        //                                        Customer.STATUS = "3";
        //                                        z++;
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        Customer.STATUS = "4";
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {

        //                                Customer.STATUS = "5";
        //                            }
        //                        }
        //                    }
        //                    catch (Exception)
        //                    {
        //                        Customer.STATUS = "6";

        //                    }

        //                    Customer.Destination = airport_name.ToUpper().ToString();
        //                    //Customer.Destination = item.red;
        //                    Customer.HAWB = item.tbl_hbl_details.Select(x => x.HBL_BLNumber).FirstOrDefault();
        //                    var Mbl = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_BLNumber).FirstOrDefault();
        //                    Customer.MAWB = Mbl == null ? "" : Mbl.ToUpper();


        //                    if (string.IsNullOrEmpty(Customer.HAWB))
        //                    {
        //                        Customer.HAWB = Customer.HAWB == null ? "" : Customer.MAWB;
        //                    }


        //                    if (string.IsNullOrEmpty(Customer.MAWB))
        //                    {
        //                        Customer.MAWB = Customer.MAWB == null ? "" : Customer.HAWB;
        //                    }

        //                    //Document Details
        //                    var mblfilref = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid == item.shipping_Master_job_id && x.doc_locker_docno == 47).Select(x => x.doc_locker_file).FirstOrDefault();
        //                    if (mblfilref == null)
        //                    {
        //                        //mblfilref = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid == item.shipping_Sub_job_id && x.doc_locker_docno == 47).Select(x => x.doc_locker_file).FirstOrDefault();
        //                        var hblfilref = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid == item.shipping_Sub_job_id && x.doc_locker_docno == 48).Select(x => x.doc_locker_file).FirstOrDefault();
        //                        Customer.Hawb_Hbl_Fileref = hblfilref;
        //                    }
        //                    Customer.Mawb_Mbl_Fileref = mblfilref;
        //                    if (string.IsNullOrEmpty(Customer.Hawb_Hbl_Fileref))
        //                    {
        //                        Customer.Hawb_Hbl_Fileref = Customer.Hawb_Hbl_Fileref == null ? "" : Customer.Hawb_Hbl_Fileref;
        //                    }
        //                    if (string.IsNullOrEmpty(Customer.Mawb_Mbl_Fileref))
        //                    {
        //                        Customer.Mawb_Mbl_Fileref = Customer.Mawb_Mbl_Fileref == null ? "" : Customer.Mawb_Mbl_Fileref;
        //                    }

        //                    Cost_details.Add(Customer);

        //                }

        //                var clrsubjob = _context.Tbl_SeaClearance_subjob.Where(s => s.SeaClearance_Shipper_company == unit_no.ToString() && s.SEC_Deleted_SubJob != "Yes" && s.SeaClearance_Reference_type == 0 && s.SeaExport_Subjob_id == null && s.SeaClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        clrsubjob = _context.Tbl_SeaClearance_subjob.Where(s => s.SeaClearance_Reference_company == unit_no && s.SEC_Deleted_SubJob != "Yes" && s.SeaClearance_Reference_type == 1 && s.SeaExport_Subjob_id == null && s.SeaClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        clrsubjob = _context.Tbl_SeaClearance_subjob.Where(s => s.SeaClearance_Reference_company == unit_no && s.SEC_Deleted_SubJob != "Yes" && s.SeaClearance_Reference_type == 2 && s.SeaExport_Subjob_id == null && s.SeaClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                }
        //                foreach (var item in clrsubjob)
        //                {
        //                    Customer Customer = new Customer();
        //                    Customer.Sub_job_no = item.SeaClearance_Subjob_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.SeaClearance_Created_on_datetime);
        //                    Customer.Sub_job_id = item.SeaClearance_Subjob_id;
        //                    Customer.Master_job_id = Convert.ToInt32(item.SeaClearance_Master_job_id);
        //                    Customer.JobType = "con";
        //                    Customer.Department = "SEC";
        //                    Customer.Shppr_Cnee = item.SeaClearance_Consignee_company.ToUpper();
        //                    Customer.Doctype = item.SeaClearance_Commodity_type;//item.cust_doc_type;
        //                    Customer.sb_be_no = item.shipping_bill_no;
        //                    Customer.sb_be_no = item.shipping_bill_no;
        //                    Customer.Mode = item.SeaClearance_shiptype;
        //                    Customer.fileref = item.shipping_pdf;
        //                    Customer.MAWB = "only clearance";
        //                    Customer.HAWB = "only clearance";
        //                    Customer.PKG = Convert.ToInt32(item.SeaClearance_Total_packages);
        //                    Customer.GRWT = Convert.ToDecimal(item.SeaClearance_Total_weight).ToString();
        //                    Customer.Destination = item.tbl_seaport_master.seaport_name.ToUpper();
        //                    Customer.pod_code = item.tbl_seaport_master.seaport_code.ToUpper();
        //                    if (!string.IsNullOrEmpty(item.shipping_pdf)) Customer.STATUS = "1";
        //                    else Customer.STATUS = "2";

        //                    switch (item.SEC_segment_no)
        //                    {
        //                        case 0:
        //                            Customer.Branch = "NHAVA SHEVA";
        //                            Customer.pol_code = "INNSA";
        //                            break;
        //                        case 1:
        //                            Customer.Branch = "CHENNAI";
        //                            Customer.pol_code = "INMAA";
        //                            break;
        //                        case 2:
        //                            Customer.Branch = "MUNDRA";
        //                            Customer.pol_code = "INMUN1";
        //                            break;
        //                    }
        //                    //Customer.Party_InvocieNo = item.SEC_SINV_NO;
        //                    if (string.IsNullOrEmpty(item.SEC_SINV_NO) && !string.IsNullOrEmpty(item.SECRoyalXML))
        //                    {
        //                        item.SEC_SINV_NO = GetInvNo(item.SECRoyalXML, "SEC");
        //                    }
        //                    Customer.Party_InvocieNo = item.SEC_SINV_NO;
        //                    List<Clr_Det> clrlist = new List<Clr_Det>();
        //                    Clr_Det Clr_det = new Clr_Det();
        //                    Clr_det.Clr_job_no = item.SeaClearance_Subjob_no;
        //                    Clr_det.Clr_job_id = item.SeaClearance_Subjob_id.ToString();
        //                    Clr_det.PKG = Convert.ToInt32(item.SeaClearance_Total_packages);
        //                    Clr_det.GRWT = Convert.ToDecimal(item.SeaClearance_Total_weight).ToString();
        //                    Clr_det.Party_InvocieNo = item.SEC_SINV_NO;
        //                    Clr_det.sb_be_no = item.shipping_bill_no;
        //                    Clr_det.Doctype = item.SeaClearance_Commodity_type;//item.cust_doc_type;
        //                    Clr_det.fileref = item.shipping_pdf;
        //                    clrlist.Add(Clr_det);
        //                    Customer.Clr_det = clrlist;
        //                    Customer.Department = "SEC";
        //                    Cost_details.Add(Customer);

        //                }


        //            }

        //            break;
        //        case "SI":
        //            foreach (var unit_no in crm_unt_business_unit_no)
        //            {
        //                var subjob = _context.Tbl_SeaImport_sub_job.Where(s => s.SeaImport_Shipper_company == unit_no.ToString() && s.SIF_Deleted_SubJob != "Yes" && s.SeaImport_Reference_type == 0 && s.SeaImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        subjob = _context.Tbl_SeaImport_sub_job.Where(s => s.SeaImport_Reference_company == unit_no && s.SIF_Deleted_SubJob != "Yes" && s.SeaImport_Reference_type == 1 && s.SeaImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        subjob = _context.Tbl_SeaImport_sub_job.Where(s => s.SeaImport_Reference_company == unit_no && s.SIF_Deleted_SubJob != "Yes" && s.SeaImport_Reference_type == 2 && s.SeaImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;

        //                }
        //                foreach (var item in subjob)
        //                {
        //                    Customer Customer = new Customer();
        //                    Customer.Sub_job_no = item.SeaImport_Sub_job_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.SeaImport_Created_on_datetime);
        //                    Customer.Sub_job_id = item.SeaImport_Sub_job_id;
        //                    Customer.Master_job_id = item.SeaImport_Master_job_id;
        //                    Customer.JobType = "CON";
        //                    Customer.Department = "SIF";
        //                    Customer.Shppr_Cnee = item.SeaImport_Consignee_company.ToUpper();
        //                    if (Customer.JobType.ToString() == "Direct")
        //                    {
        //                        Customer.Party_InvocieNo = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.Party_InvocieNo = "";

        //                    }

        //                    var shiptype = item.SeaImport_Shipment_Type;
        //                    var containerdet = item.Tbl_SeaImport_HBL_Container.ToList();
        //                    foreach (var container in containerdet)
        //                    {
        //                        Customer.Container_no = container.SeaImport_HBL_Container_No + "-";
        //                        if (shiptype == "FCL")
        //                        {
        //                            Customer.vol = container.SeaImport_HBL_Container_Type + "-";
        //                        }
        //                        else
        //                        {
        //                            Customer.vol = item.SeaImport_Shipment_Volume.ToString();
        //                            break;
        //                        }
        //                    }
        //                    if (!string.IsNullOrEmpty(Customer.vol))
        //                        Customer.vol = Customer.vol.TrimEnd('-');
        //                    if (!string.IsNullOrEmpty(Customer.vol))
        //                        Customer.Container_no = Customer.Container_no.TrimEnd('-');


        //                    Customer.Mode = item.SeaImport_Shipment_Type;
        //                    Customer.PKG = Convert.ToInt32(item.SeaImport_Package_Count);
        //                    Customer.GRWT = Convert.ToDecimal(item.SeaImport_Gross_Weight).ToString();
        //                    var airport_name = item.tbl_seaport_master.seaport_name;
        //                    Customer.pod_code = item.tbl_seaport_master.seaport_code;
        //                    var mastercount = item.Tbl_SeaImport_master_job.SeaImport_Activity_Status;
        //                    Customer.ETD = item.Tbl_SeaImport_master_job.SeaImport_ETD;
        //                    Customer.ETA = item.Tbl_SeaImport_master_job.SeaImport_ETA;

        //                    var chkarrival = _context.Tbl_SeaImport_Arrival.Where(x => x.SeaImport_arrival_subjob_id == item.SeaImport_Sub_job_id).Select(x => x.SeaImport_arrival_ETA).FirstOrDefault();
        //                    if (chkarrival != null)
        //                        Customer.ETA = chkarrival;

        //                    var vesselname = _context.Tbl_SeaImport_Arrival.Where(x => x.SeaImport_arrival_subjob_id == item.SeaImport_Sub_job_id).Select(x => x.SeaImport_arrival_vessel_name).FirstOrDefault();
        //                    if (vesselname != null)
        //                        Customer.vessel_voyag = vesselname;
        //                    var voyag = _context.Tbl_SeaImport_Arrival.Where(x => x.SeaImport_arrival_subjob_id == item.SeaImport_Sub_job_id).Select(x => x.SeaImport_arrival_Voyage).FirstOrDefault();
        //                    if (voyag != null)
        //                        Customer.vessel_voyag += " - " + voyag;


        //                    if (mastercount == true)
        //                    {
        //                        Customer.STATUS = "1";/* "<p class='fas fa-check' style='color:green;width:20px;text-align:center;'></p>";*/
        //                    }
        //                    else if (mastercount == false)
        //                    {
        //                        Customer.STATUS = "2";/* "<p class='fas fa-check' style='color:green;width:20px;text-align:center;'></p>";*/
        //                    }
        //                    else
        //                    {
        //                        Customer.STATUS = "3"; /*"<p class='las la-user-clock' style='color:#fd397a;font-size:20px;position:relative;top:2px;'></p>";*/
        //                    }

        //                    switch (item.SIF_segment_no)
        //                    {
        //                        case 0:
        //                            Customer.Branch = "MUMBAI";
        //                            Customer.pol_code = "INNSA";
        //                            break;
        //                        case 1:
        //                            Customer.Branch = "CHENNAI";
        //                            Customer.pol_code = "INMAA";
        //                            break;
        //                        case 2:
        //                            Customer.Branch = "AHMEDABAD";
        //                            Customer.pol_code = "INMUN1";
        //                            break;
        //                    }
        //                    Customer.Destination = airport_name.ToUpper().ToString();
        //                    Customer.HAWB = item.SeaImport_Hbl_No;
        //                    var mawb = item.Tbl_SeaImport_master_job.SeaImport_Mbl_No.ToUpper();
        //                    if (Customer.HAWB == null)
        //                    {
        //                        Customer.HAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.HAWB = Customer.HAWB.ToUpper();
        //                    }


        //                    if (Customer.MAWB == null)
        //                    {
        //                        Customer.MAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.MAWB = Customer.MAWB.ToUpper();
        //                    }
        //                    Customer.MAWB = mawb.ToUpper();
        //                    Cost_details.Add(Customer);
        //                }

        //                var clrsubjob = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Shipper_company == unit_no.ToString() && s.
        //                SIC_Deleted_SubJob != "Yes" && s.SeaImpClearance_Reference_type == 0 && s.SeaImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                switch (userInDB.crm_user_party_type)
        //                {
        //                    case "LOCAL AGENT":
        //                        clrsubjob = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Reference_company == unit_no && s.SIC_Deleted_SubJob != "Yes" && s.SeaImpClearance_Reference_type == 1 && s.SeaImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                    case "LOCAL CUSTOMER":
        //                        break;
        //                    default:
        //                        clrsubjob = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Reference_company == unit_no && s.SIC_Deleted_SubJob != "Yes" && s.SeaImpClearance_Reference_type == 2 && s.SeaImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
        //                        break;
        //                }
        //                foreach (var item in clrsubjob)
        //                {
        //                    Customer Customer = new Customer();
        //                    Customer.Sub_job_no = item.SeaImpClearance_Subjob_no;
        //                    Customer.Sub_job_date = Convert.ToDateTime(item.SeaImpClearance_Created_on_datetime);
        //                    Customer.Sub_job_id = item.SeaImpClearance_Subjob_id;
        //                    Customer.Master_job_id = Convert.ToInt32(item.SeaImpClearance_Master_job_id);
        //                    Customer.JobType = "con";
        //                    Customer.Shppr_Cnee = item.SeaImpClearance_Consignee_company.ToUpper();
        //                    Customer.Mode = item.SeaImpClearance_shiptype;
        //                    Customer.fileref = item.billof_entry_pdf;
        //                    Customer.PKG = Convert.ToInt32(item.SeaImpClearance_Total_packages);
        //                    Customer.GRWT = Convert.ToDecimal(item.SeaImpClearance_Total_weight).ToString();
        //                    var airport_name = item.tbl_seaport_master.seaport_name;
        //                    Customer.pod_code = item.tbl_seaport_master.seaport_name;
        //                    if (!string.IsNullOrEmpty(item.billof_entry_pdf)) Customer.STATUS = "1";
        //                    else Customer.STATUS = "2";
        //                    Customer.Department = "SIC";


        //                    switch (item.SIC_segment_no)
        //                    {
        //                        case 0:
        //                            Customer.Branch = "MUMBAI";
        //                            Customer.Branch = "INNSA";
        //                            break;
        //                        case 1:
        //                            Customer.Branch = "CHENNAI";
        //                            Customer.Branch = "INMAA";
        //                            break;
        //                        case 2:
        //                            Customer.Branch = "AHMEDABAD";
        //                            Customer.Branch = "INMUN1";
        //                            break;
        //                    }

        //                    Customer.Destination = airport_name.ToUpper().ToString();
        //                    Customer.HAWB = item.SeaImpClearance_hbl;
        //                    var sic_mawb = item.SeaImpClearance_mbl;
        //                    Customer.MAWB = sic_mawb;

        //                    if (Customer.HAWB == null)
        //                    {
        //                        Customer.HAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.HAWB = Customer.HAWB.ToUpper();
        //                    }


        //                    if (Customer.MAWB == null)
        //                    {
        //                        Customer.MAWB = "";
        //                    }
        //                    else
        //                    {
        //                        Customer.MAWB = Customer.MAWB;
        //                    }


        //                    Customer.Doctype = item.SeaImpClearance_Commodity_type;//item.cust_doc_type;
        //                    Customer.sb_be_no = item.billof_entry_no;
        //                    if (string.IsNullOrEmpty(item.SIC_SINV_NO) && !string.IsNullOrEmpty(item.SICRoyalXML))
        //                    {
        //                        item.SIC_SINV_NO = GetInvNo(item.SICRoyalXML, "SIC");
        //                    }
        //                    Customer.Party_InvocieNo = item.SIC_SINV_NO;

        //                    Cost_details.Add(Customer);
        //                }

        //            }
        //            break;
        //    }
        //    if (Session["Search_By"].ToString() != "" && Session["Seach_parameter"].ToString() != "")
        //    {
        //        var Search_By = Session["Search_By"].ToString();
        //        var Seach_parameter = Session["Seach_parameter"].ToString();


        //        switch (Search_By)
        //        {
        //            case "JOB":
        //                Cost_details = Cost_details.Where(x => x.Sub_job_no.Contains(Seach_parameter)).ToList();
        //                break;
        //            case "MAWB":

        //                Cost_details = Cost_details.Where(x => x.MAWB != null).ToList();
        //                Cost_details = Cost_details.Where(x => x.MAWB.Contains(Seach_parameter) || x.HAWB.Contains((Seach_parameter))).ToList();


        //                break;
        //            case "POD":
        //                Cost_details = Cost_details.Where(x => x.Destination.StartsWith(Seach_parameter)).ToList();

        //                break;
        //            case "CONSIGNEE":
        //                Cost_details = Cost_details.Where(x => x.Shppr_Cnee.Contains(Seach_parameter)).ToList();
        //                break;

        //            case "INVOICE":
        //                if (dept == "AI" || dept == "SI")
        //                {
        //                    Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
        //                    Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter)).ToList();
        //                }
        //                else
        //                {
        //                    Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
        //                    Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter) || x.Clr_det.All(y => y.Party_InvocieNo.Contains(Seach_parameter))).ToList();
        //                }


        //                break;


        //            case "SB":
        //                if (dept == "AI" || dept == "SI")
        //                {
        //                    Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
        //                    Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter)).ToList();
        //                }
        //                else
        //                {
        //                    Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
        //                    Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter) || x.Clr_det.All(y => y.sb_be_no.Contains(Seach_parameter))).ToList();
        //                }

        //                break;
        //        }
        //    }

        //    var From_Dtt = Session["From_Dtt"].ToString();
        //    var To_Dtt = Session["To_Dtt"].ToString();
        //    if (!string.IsNullOrEmpty(From_Dtt) && !string.IsNullOrEmpty(To_Dtt))
        //    {
        //        From_Dtt += " 00:00:00";
        //        To_Dtt += " 23:59:00";

        //        DateTime FromDt = DateTime.ParseExact(From_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
        //        DateTime ToDt = DateTime.ParseExact(To_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
        //        Cost_details = Cost_details.Where(d => d.Sub_job_date >= FromDt && d.Sub_job_date <= ToDt).ToList();
        //    }

        //    if (Session["Stauts"].ToString() != "")
        //    {
        //        var Stauts = Session["Stauts"].ToString();
        //        if (Stauts == "Completed")
        //        {
        //            string completed = "1";
        //            Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
        //        }
        //        else

        //        {
        //            string completed = "2";
        //            Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
        //        }
        //    }
        //    Cost_details = Cost_details.OrderByDescending(X => X.Sub_job_date).ToList();
        //    ViewBag.Cost_details = Cost_details;
        //    TempData["Cost_details"] = Cost_details;
        //    Session["Cost_details"] = Cost_details;
        //    return View(user);
        //}

        //Get Tracking Tratus
        [SessionTimeOut]
        public JsonResult GetStatus(string stockno)
        {
            stockno = stockno.Replace(" ", "");
            var track_traceDb = _context.tbl_mawb_tracking.Where(x => x.mawb_track_stock_no == stockno).OrderByDescending(x => x.mawb_track_id).Select(x => x.mawb_track_json).FirstOrDefault();
            return Json(track_traceDb, JsonRequestBehavior.AllowGet);
        }

        [SessionTimeOut]
        public ActionResult AE_DB()
        {
            //string dept = Session["dept"].ToString();
            CustomerModal user = new CustomerModal();
            user.tbl_Crm_User_s = _context.tbl_crm_user_master.ToList();
            var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
            List<Customer> Cost_details = new List<Customer>();

            var userInDB = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).FirstOrDefault();
            List<int?> crm_unt_business_unit_no = new List<int?>();
            var Bu_unit_no = userInDB.tbl_crm_business_unit.Select(x => x.crm_unt_business_unit_no).ToList();
            crm_unt_business_unit_no.AddRange(Bu_unit_no);
            if (Session["docno"].ToString() != "0")
            {
                crm_unt_business_unit_no = new List<int?>();
                crm_unt_business_unit_no.Add(Convert.ToInt32(Session["docno"].ToString()));
            }

            var priviewfinyear = Session["financialyear"].ToString();//financial year
            var newfinanceyear = priviewfinyear.Split('-')[1];

            foreach (var unit_no in crm_unt_business_unit_no)
            {
                var subjob = _context.Tbl_sub_job.Where(s => s.Shipper_company == unit_no.ToString() && s.AXF_Deleted_SubJob != "Yes" && s.Reference_type == 0 && s.Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        subjob = _context.Tbl_sub_job.Where(s => s.Reference_company == unit_no && s.AXF_Deleted_SubJob != "Yes" && s.Reference_type == 1 && s.Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        subjob = _context.Tbl_sub_job.Where(s => s.Reference_company == unit_no && s.AXF_Deleted_SubJob != "Yes" && s.Reference_type == 2 && s.Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;

                }
                foreach (var item in subjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.Sub_job_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.Created_on_datetime);
                    Customer.Sub_job_id = item.Sub_job_id;
                    Customer.Mode = "";
                    Customer.Master_job_id = item.Master_job_id;
                    Customer.JobType = item.Tbl_master_job.Master_job_type;
                    Customer.is_tracking_enabled = item.Tbl_master_job.tbl_mawb_details.Select(x => x.is_tracking_enabled).FirstOrDefault();
                    Customer.mawb_curr_track_status = item.Tbl_master_job.tbl_mawb_details.Select(x => x.mawb_curr_track_status).FirstOrDefault();
                    Customer.Department = "AEF";
                    var Chkclrjobno = item.Tbl_AirClearance_subjob.Where(x => x.AEC_Deleted_SubJob != "Yes").ToList();
                    if (Customer.JobType.ToString() == "Direct")
                    {
                        Customer.Shppr_Cnee = item.Tbl_master_job.tbl_mawb_details.Select(x => x.MAWB_Consignee_Name.ToUpper()).FirstOrDefault();
                    }
                    else
                    {
                        Customer.Shppr_Cnee = item.tbl_hawb_details.Select(x => x.HAWB_Consignee_Name.ToUpper()).FirstOrDefault();
                    }
                    List<Clr_Det> clrlist = new List<Clr_Det>();
                    if (Chkclrjobno.Count > 0)
                    {
                        foreach (var clrjob in Chkclrjobno)
                        {
                            Clr_Det Clr_det = new Clr_Det();
                            Clr_det.Clr_job_no = clrjob.AirClearance_Subjob_no;
                            Clr_det.Clr_job_id = clrjob.AirClearance_Subjob_id.ToString();
                            Clr_det.PKG = Convert.ToInt32(clrjob.AirClearance_Total_packages);
                            Clr_det.GRWT = Convert.ToDecimal(clrjob.AirClearance_Total_weight).ToString();
                            if (string.IsNullOrEmpty(clrjob.AEC_SINV_NO) && !string.IsNullOrEmpty(clrjob.AECRoyalXML))
                            {
                                clrjob.AEC_SINV_NO = GetInvNo(clrjob.AECRoyalXML, "AEC");
                            }
                            if (string.IsNullOrEmpty(clrjob.AEC_SINV_NO))
                            {
                                Clr_det.Party_InvocieNo = Customer.Party_InvocieNo == null ? "" : Customer.Party_InvocieNo;
                            }
                            else
                            {
                                Clr_det.Party_InvocieNo = clrjob.AEC_SINV_NO;
                                Customer.Party_InvocieNo = Clr_det.Party_InvocieNo;
                            }
                            Clr_det.sb_be_no = clrjob.shipping_bill_no;
                            if (string.IsNullOrEmpty(Clr_det.sb_be_no))
                            {
                                Clr_det.sb_be_no = Customer.sb_be_no == null ? "" : Customer.sb_be_no;
                            }
                            else
                            {
                                Clr_det.sb_be_no = clrjob.shipping_bill_no;
                                Customer.sb_be_no = Clr_det.sb_be_no;
                            }
                            Clr_det.Doctype = clrjob.AirClearance_Commodity_type;//clrjob.cust_doc_type;
                            Clr_det.fileref = clrjob.shipping_pdf;
                            Clr_det.department = "AEC";
                            clrlist.Add(Clr_det);
                        }
                    }
                    Customer.Clr_det = clrlist;
                    if (Chkclrjobno.Count == 0)
                    {
                        Customer.PKG = Convert.ToInt32(item.Total_packages);
                        Customer.GRWT = Convert.ToDecimal(item.Total_weight).ToString();
                    }
                    var airport_name = item.tbl_airport_master.airport_name;
                    var mastercount = item.Tbl_master_job.tbl_mawb_details.Select(x => x.MAWB_activity_status).ToList();
                    try
                    {
                        if (mastercount.Count > 0)
                        {
                            var masterstatus = mastercount[0];
                            if (masterstatus == true)
                                if (masterstatus == true)
                                {
                                    Customer.STATUS = "1";
                                }
                                else
                                {
                                    Customer.STATUS = "2";
                                }
                        }
                        else
                        {
                            var subjobcount = subjob;
                            if (subjobcount.Count > 0)
                            {
                                int z = 0;
                                for (; z < subjobcount.Count;)
                                {
                                    var hawbstaus = subjobcount[z].tbl_hawb_details.Select(x => x.HAWB_activity_status).FirstOrDefault();
                                    if (hawbstaus == true)
                                    {
                                        Customer.STATUS = "3";
                                        z++;
                                        break;
                                    }
                                    else
                                    {
                                        Customer.STATUS = "4";
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                Customer.STATUS = "5";
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Customer.STATUS = "6";

                    }
                    Customer.Branch = item.Executing_branch;
                    switch (Customer.Branch.ToUpper())
                    {
                        case "MUMBAI":
                            Customer.pol_code = "BOM";
                            break;
                        case "AHMEDABAD":
                            Customer.pol_code = "AMD";
                            break;
                        case "CHENNAI":
                            Customer.pol_code = "MAA";
                            break;
                    }
                    Customer.Destination = airport_name.ToUpper().ToString();
                    Customer.pod_code = item.tbl_airport_master.airport_code;
                    Customer.HAWB = item.tbl_hawb_details.Select(x => x.HAWB_stock_no).FirstOrDefault();
                    var mawb = item.Tbl_master_job.tbl_mawb_details.Select(x => x.MAWB_stock_no).FirstOrDefault();
                    var airlinedetails = item.Tbl_master_job.tbl_mawb_details.Select(x => x.tbl_mawb_airline).ToList();
                    if (airlinedetails.Count > 0)
                    {
                        Customer.ETD = airlinedetails[0].Select(x => x.MAWB_Departure_date).FirstOrDefault();
                        var compno = airlinedetails[0].Select(x => x.MAWB_Airline_company_id).FirstOrDefault();
                        var flightno = airlinedetails[0].Select(x => x.MAWB_Airline_flight_no).FirstOrDefault();
                        Customer.flightno = _context.tbl_company_business_unit.Where(x => x.unit_no == compno).Select(x => x.tbl_company_master.Ar_Code).FirstOrDefault() + "-" + flightno;
                        if (airlinedetails.Count > 1)
                        {
                            for (int l = 0; l < airlinedetails.Count; l++)
                            {
                                Customer.ETA = airlinedetails[l].Select(x => x.MAWB_Departure_date).FirstOrDefault();
                            }
                        }
                        else
                        {
                            Customer.ETA = Customer.ETD;
                        }
                    }
                    var aec_mawb = "";
                    try
                    {
                        if (item.Sub_job_id > 0)
                        {
                            var mjob_id = item.Master_job_id;
                            var stock_num = _context.tbl_mawb_details.Where(s => s.MAWB_master_job_id == mjob_id).Select(s => s.MAWB_stock_no).FirstOrDefault();
                            if (!string.IsNullOrEmpty(stock_num))
                            {
                                var unitnum = _context.tbl_mawb_details.Where(s => s.MAWB_master_job_id == mjob_id).Select(s => s.MAWB_AirLine_code).FirstOrDefault();
                                var prefix = _context.tbl_company_business_unit.Where(s => s.unit_no.ToString() == unitnum).Select(s => s.tbl_company_master.Prefix).FirstOrDefault();
                                aec_mawb = prefix + "-" + stock_num.Substring(0, 4) + " " + stock_num.Substring(4, 4);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        aec_mawb = "000-0000 0000";
                    }
                    Customer.MAWB = aec_mawb;
                    if (string.IsNullOrEmpty(Customer.HAWB))
                    {
                        Customer.HAWB = Customer.HAWB == null ? "" : Customer.MAWB;
                    }
                    else
                    {
                        Customer.HAWB = Customer.HAWB;
                    }
                    if (string.IsNullOrEmpty(Customer.MAWB))
                    {
                        Customer.MAWB = Customer.MAWB == null ? "" : Customer.HAWB;
                    }
                    else
                    {
                        Customer.MAWB = Customer.MAWB;
                    }
                    Cost_details.Add(Customer);
                }

                var clrsubjob = _context.Tbl_AirClearance_subjob.Where(s => s.AirClearance_Shipper_company == unit_no.ToString() && s.AEC_Deleted_SubJob != "Yes" && s.AirClearance_Reference_type == 0 && s.AirExport_Subjob_id == null && s.AirClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        clrsubjob = _context.Tbl_AirClearance_subjob.Where(s => s.AirClearance_Reference_company == unit_no && s.AEC_Deleted_SubJob != "Yes" && s.AirClearance_Reference_type == 1 && s.AirExport_Subjob_id == null && s.AirClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        clrsubjob = _context.Tbl_AirClearance_subjob.Where(s => s.AirClearance_Reference_company == unit_no && s.AEC_Deleted_SubJob != "Yes" && s.AirClearance_Reference_type == 2 && s.AirExport_Subjob_id == null && s.AirClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                }
                foreach (var item in clrsubjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.AirClearance_Subjob_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.AirClearance_Created_on_datetime);
                    Customer.Sub_job_id = item.AirClearance_Subjob_id;
                    Customer.Master_job_id = Convert.ToInt32(item.AirClearance_Master_job_id);
                    Customer.JobType = "con";
                    Customer.Mode = "";
                    Customer.Department = "AEC";
                    Customer.Shppr_Cnee = item.AirClearance_Consignee_company.ToUpper();
                    Customer.JobType = item.AirClearance_Commodity_type;//item.cust_doc_type;
                    Customer.sb_be_no = item.shipping_bill_no;
                    Customer.fileref = item.shipping_pdf;
                    Customer.PKG = Convert.ToInt32(item.AirClearance_Total_packages);
                    Customer.GRWT = Convert.ToDecimal(item.AirClearance_Total_weight).ToString();
                    Customer.Destination = item.tbl_airport_master.airport_name.ToUpper();
                    Customer.pod_code = item.tbl_airport_master.airport_code.ToUpper();
                    if (!string.IsNullOrEmpty(item.shipping_pdf)) Customer.STATUS = "1";
                    else Customer.STATUS = "2";
                    switch (item.AEC_segment_no)
                    {
                        case 0:
                            Customer.Branch = "MUMBAI";
                            Customer.pol_code = "BOM";
                            break;
                        case 1:
                            Customer.Branch = "CHENNAI";
                            Customer.pol_code = "MAA";
                            break;
                        case 2:
                            Customer.Branch = "AHMEDABAD";
                            Customer.pol_code = "AMD";
                            break;
                    }
                    Customer.HAWB = "";
                    Customer.MAWB = "";
                    if (string.IsNullOrEmpty(item.AEC_SINV_NO) && !string.IsNullOrEmpty(item.AECRoyalXML))
                    {
                        item.AEC_SINV_NO = GetInvNo(item.AECRoyalXML, "AEC");
                    }
                    Customer.Party_InvocieNo = item.AEC_SINV_NO;
                    List<Clr_Det> clrlist = new List<Clr_Det>();
                    Clr_Det Clr_det = new Clr_Det();
                    Clr_det.Clr_job_no = item.AirClearance_Subjob_no;
                    Clr_det.Clr_job_id = item.AirClearance_Subjob_id.ToString();
                    Clr_det.PKG = Convert.ToInt32(item.AirClearance_Total_packages);
                    Clr_det.GRWT = Convert.ToDecimal(item.AirClearance_Total_weight).ToString();
                    Clr_det.Party_InvocieNo = item.AEC_SINV_NO;
                    Clr_det.sb_be_no = item.shipping_bill_no;
                    Clr_det.Doctype = item.AirClearance_Commodity_type;//item.cust_doc_type;
                    Clr_det.fileref = item.shipping_pdf;
                    clrlist.Add(Clr_det);
                    Customer.Clr_det = clrlist;
                    Customer.Department = "AEC";
                    Cost_details.Add(Customer);
                }
            }
            if (Session["Search_By"].ToString() != "" && Session["Seach_parameter"].ToString() != "")
            {
                var Search_By = Session["Search_By"].ToString();
                var Seach_parameter = Session["Seach_parameter"].ToString();
                switch (Search_By)
                {
                    case "JOB":
                        Cost_details = Cost_details.Where(x => x.Sub_job_no.Contains(Seach_parameter)).ToList();
                        break;
                    case "MAWB":
                        Cost_details = Cost_details.Where(x => x.MAWB != null).ToList();
                        Cost_details = Cost_details.Where(x => x.MAWB.Contains(Seach_parameter) || x.HAWB.Contains((Seach_parameter))).ToList();
                        break;
                    case "POD":
                        Cost_details = Cost_details.Where(x => x.Destination.StartsWith(Seach_parameter)).ToList();
                        break;
                    case "CONSIGNEE":
                        Cost_details = Cost_details.Where(x => x.Shppr_Cnee.Contains(Seach_parameter)).ToList();
                        break;
                    case "INVOICE":
                        Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
                        Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter) || x.Clr_det.All(y => y.Party_InvocieNo.Contains(Seach_parameter))).ToList();
                        break;
                    case "SB":
                        Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
                        Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter) || x.Clr_det.All(y => y.sb_be_no.Contains(Seach_parameter))).ToList();
                        break;
                }
            }

            var From_Dtt = Session["From_Dtt"].ToString();
            var To_Dtt = Session["To_Dtt"].ToString();
            if (!string.IsNullOrEmpty(From_Dtt) && !string.IsNullOrEmpty(To_Dtt))
            {
                From_Dtt += " 00:00:00";
                To_Dtt += " 23:59:00";

                DateTime FromDt = DateTime.ParseExact(From_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                DateTime ToDt = DateTime.ParseExact(To_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                Cost_details = Cost_details.Where(d => d.Sub_job_date >= FromDt && d.Sub_job_date <= ToDt).ToList();
            }
            if (Session["Stauts"].ToString() != "")
            {
                var Stauts = Session["Stauts"].ToString();
                if (Stauts == "Completed")
                {
                    string completed = "1";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
                else
                {
                    string completed = "2";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
            }
            Cost_details = Cost_details.OrderByDescending(X => X.Sub_job_date).ToList();
            ViewBag.Cost_details = Cost_details;
            TempData["Cost_details"] = Cost_details;
            Session["Cost_details"] = Cost_details;
            return View(user);
        }

        [SessionTimeOut]
        public ActionResult AI_DB()
        {
            string dept = Session["dept"].ToString();
            CustomerModal user = new CustomerModal();
            user.tbl_Crm_User_s = _context.tbl_crm_user_master.ToList();
            var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
            List<Customer> Cost_details = new List<Customer>();

            var userInDB = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).FirstOrDefault();
            List<int?> crm_unt_business_unit_no = new List<int?>();
            var Bu_unit_no = userInDB.tbl_crm_business_unit.Select(x => x.crm_unt_business_unit_no).ToList();
            crm_unt_business_unit_no.AddRange(Bu_unit_no);
            if (Session["docno"].ToString() != "0")
            {
                crm_unt_business_unit_no = new List<int?>();
                crm_unt_business_unit_no.Add(Convert.ToInt32(Session["docno"].ToString()));
            }

            var priviewfinyear = Session["financialyear"].ToString();//financial year
            var newfinanceyear = priviewfinyear.Split('-')[1];

            foreach (var unit_no in crm_unt_business_unit_no)
            {
                var subjob = _context.Tbl_AirImport_sub_job.Where(s => s.AirImport_Shipper_company == unit_no.ToString() && s.AIF_Deleted_SubJob != "Yes" && s.AirImport_Reference_type == 0 && s.AirImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        subjob = _context.Tbl_AirImport_sub_job.Where(s => s.AirImport_Reference_company == unit_no && s.AIF_Deleted_SubJob != "Yes" && s.AirImport_Reference_type == 1 && s.AirImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        subjob = _context.Tbl_AirImport_sub_job.Where(s => s.AirImport_Reference_company == unit_no && s.AIF_Deleted_SubJob != "Yes" && s.AirImport_Reference_type == 2 && s.AirImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;

                }
                foreach (var item in subjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.AirImport_Sub_job_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.AirImport_Created_on_datetime);

                    Customer.Sub_job_id = item.AirImport_Sub_job_id;
                    Customer.Master_job_id = item.AirImport_Master_job_id;
                    Customer.JobType = "CON";
                    Customer.Mode = "";
                    Customer.Shppr_Cnee = item.AirImport_Consignee_company.ToUpper();
                    Customer.PKG = Convert.ToInt32(item.AirImport_Package_Count);
                    Customer.GRWT = Convert.ToDecimal(item.AirImport_Gross_Weight).ToString();
                    Customer.Department = "AIF";
                    var airport_name = item.tbl_airport_master.airport_name;
                    Customer.pod_code = item.tbl_airport_master.airport_code;
                    var mastercount = item.Tbl_AirImport_master_job.AirImport_Activity_Status;
                    Customer.ETD = item.Tbl_AirImport_master_job.AirImport_ETD;
                    Customer.ETA = item.Tbl_AirImport_master_job.AirImport_ETA;
                    var chkarrival = _context.Tbl_AirImport_Arrival.Where(x => x.airimport_subjobid == item.AirImport_Sub_job_id).Select(x => x.arrival_date).FirstOrDefault();
                    if (chkarrival != null)
                        Customer.ETA = chkarrival;

                    if (mastercount == true)
                    {
                        Customer.STATUS = "1";
                    }
                    else if (mastercount == false)
                    {
                        Customer.STATUS = "2";
                    }
                    else
                    {
                        Customer.STATUS = "3";
                    }

                    switch (item.AIF_segment_no)
                    {
                        case 0:
                            Customer.Branch = "MUMBAI";
                            Customer.pol_code = "BOM";
                            break;
                        case 1:
                            Customer.Branch = "CHENNAI";
                            Customer.pol_code = "MAA";
                            break;
                        case 2:
                            Customer.Branch = "AHMEDABAD";
                            Customer.pol_code = "AMD";
                            break;
                    }

                    Customer.Destination = airport_name.ToUpper().ToString();
                    Customer.HAWB = item.AirImport_Hawb_No;
                    var mawb = item.Tbl_AirImport_master_job.AirImport_Mawb_No;
                    Customer.MAWB = mawb;
                    if (Customer.HAWB == null)
                    {
                        Customer.HAWB = "";
                    }
                    else
                    {
                        Customer.HAWB = Customer.HAWB.ToUpper();
                    }
                    if (Customer.MAWB == null)
                    {
                        Customer.MAWB = "";
                    }
                    else
                    {
                        Customer.MAWB = Customer.MAWB;
                    }
                    Cost_details.Add(Customer);
                }

                var clrsubjob = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Shipper_company == unit_no.ToString() && s.AIC_Deleted_SubJob != "Yes" && s.AirImpClearance_Reference_type == 0 && s.AirImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        clrsubjob = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Reference_company == unit_no && s.AIC_Deleted_SubJob != "Yes" && s.AirImpClearance_Reference_type == 1 && s.AirImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        clrsubjob = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Reference_company == unit_no && s.AIC_Deleted_SubJob != "Yes" && s.AirImpClearance_Reference_type == 2 && s.AirImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                }
                foreach (var item in clrsubjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.AirImpClearance_Subjob_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.AirImpClearance_Created_on_datetime);
                    Customer.Sub_job_id = item.AirImpClearance_Subjob_id;
                    Customer.fileref = item.billof_entry_pdf;
                    Customer.Master_job_id = Convert.ToInt32(item.AirImpClearance_Master_job_id);
                    Customer.JobType = "con";
                    Customer.Mode = "";
                    Customer.Shppr_Cnee = item.AirImpClearance_Consignee_company.ToUpper();
                    Customer.PKG = Convert.ToInt32(item.AirImpClearance_Total_packages);
                    Customer.GRWT = Convert.ToDecimal(item.AirImpClearance_Total_weight).ToString();
                    var airport_name = item.tbl_airport_master.airport_name;
                    Customer.pod_code = item.tbl_airport_master.airport_code;
                    if (!string.IsNullOrEmpty(item.billof_entry_pdf)) Customer.STATUS = "1";
                    else Customer.STATUS = "2";
                    Customer.Department = "AIC";
                    if (Customer.JobType.ToString() == "Direct")
                    {
                        Customer.Party_InvocieNo = "";
                    }
                    else
                    {
                        Customer.Party_InvocieNo = "";

                    }


                    switch (item.AIC_segment_no)
                    {
                        case 0:
                            Customer.Branch = "MUMBAI";
                            Customer.pol_code = "BOM";
                            break;
                        case 1:
                            Customer.Branch = "CHENNAI";
                            Customer.pol_code = "MAA";
                            break;
                        case 2:
                            Customer.Branch = "AHMEDABAD";
                            Customer.pol_code = "AMD";
                            break;
                    }

                    Customer.Destination = airport_name.ToUpper();
                    Customer.HAWB = item.AirImpClearance_hawb;

                    if (Customer.HAWB == null)
                    {
                        Customer.HAWB = "";
                    }
                    else
                    {
                        Customer.HAWB = Customer.HAWB.ToUpper();
                    }

                    var aic_mawb = item.AirImpClearance_mawb;
                    Customer.MAWB = aic_mawb;
                    if (Customer.MAWB == null)
                    {
                        Customer.MAWB = "";
                    }
                    else
                    {
                        Customer.MAWB = Customer.MAWB;
                    }
                    Customer.Doctype = item.AirImpClearance_Commodity_type;//item.cust_doc_type;
                    Customer.sb_be_no = item.billof_entry_no;
                    if (string.IsNullOrEmpty(item.AIC_SINV_NO) && !string.IsNullOrEmpty(item.AICRoyalXML))
                    {
                        item.AIC_SINV_NO = GetInvNo(item.AICRoyalXML, "AIC");
                    }
                    Customer.Party_InvocieNo = item.AIC_SINV_NO;



                    Cost_details.Add(Customer);

                }
            }

            if (Session["Search_By"].ToString() != "" && Session["Seach_parameter"].ToString() != "")
            {
                var Search_By = Session["Search_By"].ToString();
                var Seach_parameter = Session["Seach_parameter"].ToString();


                switch (Search_By)
                {
                    case "JOB":
                        Cost_details = Cost_details.Where(x => x.Sub_job_no.Contains(Seach_parameter)).ToList();
                        break;
                    case "MAWB":

                        Cost_details = Cost_details.Where(x => x.MAWB != null).ToList();
                        Cost_details = Cost_details.Where(x => x.MAWB.Contains(Seach_parameter) || x.HAWB.Contains((Seach_parameter))).ToList();


                        break;
                    case "POD":
                        Cost_details = Cost_details.Where(x => x.Destination.StartsWith(Seach_parameter)).ToList();

                        break;
                    case "CONSIGNEE":
                        Cost_details = Cost_details.Where(x => x.Shppr_Cnee.Contains(Seach_parameter)).ToList();
                        break;

                    case "INVOICE":
                        if (dept == "AI" || dept == "SI")
                        {
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter)).ToList();
                        }
                        else
                        {
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter) || x.Clr_det.All(y => y.Party_InvocieNo.Contains(Seach_parameter))).ToList();
                        }


                        break;


                    case "SB":
                        if (dept == "AI" || dept == "SI")
                        {
                            Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
                            Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter)).ToList();
                        }
                        else
                        {
                            Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
                            Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter) || x.Clr_det.All(y => y.sb_be_no.Contains(Seach_parameter))).ToList();
                        }

                        break;
                }
            }

            var From_Dtt = Session["From_Dtt"].ToString();
            var To_Dtt = Session["To_Dtt"].ToString();
            if (!string.IsNullOrEmpty(From_Dtt) && !string.IsNullOrEmpty(To_Dtt))
            {
                From_Dtt += " 00:00:00";
                To_Dtt += " 23:59:00";

                DateTime FromDt = DateTime.ParseExact(From_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                DateTime ToDt = DateTime.ParseExact(To_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                Cost_details = Cost_details.Where(d => d.Sub_job_date >= FromDt && d.Sub_job_date <= ToDt).ToList();
            }

            if (Session["Stauts"].ToString() != "")
            {
                var Stauts = Session["Stauts"].ToString();
                if (Stauts == "Completed")
                {
                    string completed = "1";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
                else

                {
                    string completed = "2";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
            }
            Cost_details = Cost_details.OrderByDescending(X => X.Sub_job_date).ToList();
            ViewBag.Cost_details = Cost_details;
            TempData["Cost_details"] = Cost_details;
            Session["Cost_details"] = Cost_details;
            return View(user);
        }

        [SessionTimeOut]
        public ActionResult SE_DB()
        {
            string dept = Session["dept"].ToString();
            CustomerModal user = new CustomerModal();
            user.tbl_Crm_User_s = _context.tbl_crm_user_master.ToList();
            var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
            List<Customer> Cost_details = new List<Customer>();

            var userInDB = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).FirstOrDefault();
            List<int?> crm_unt_business_unit_no = new List<int?>();
            var Bu_unit_no = userInDB.tbl_crm_business_unit.Select(x => x.crm_unt_business_unit_no).ToList();
            crm_unt_business_unit_no.AddRange(Bu_unit_no);
            if (Session["docno"].ToString() != "0")
            {
                crm_unt_business_unit_no = new List<int?>();
                crm_unt_business_unit_no.Add(Convert.ToInt32(Session["docno"].ToString()));
            }

            var priviewfinyear = Session["financialyear"].ToString();//financial year
            var newfinanceyear = priviewfinyear.Split('-')[1];

            foreach (var unit_no in crm_unt_business_unit_no)
            {
                var subjob = _context.Tbl_Shiping_sub_job.Where(s => s.shipping_Shipper_company == unit_no.ToString() && s.SXF_Deleted_SubJob != "Yes" && s.shipping_Reference_type == 0 && s.shipping_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        subjob = _context.Tbl_Shiping_sub_job.Where(s => s.shipping_Reference_company == unit_no && s.SXF_Deleted_SubJob != "Yes" && s.shipping_Reference_type == 1 && s.shipping_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        subjob = _context.Tbl_Shiping_sub_job.Where(s => s.shipping_Reference_company == unit_no && s.SXF_Deleted_SubJob != "Yes" && s.shipping_Reference_type == 2 && s.shipping_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;

                }
                foreach (var item in subjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.shipping_Sub_job_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.shipping_Created_on_datetime);
                    Customer.Sub_job_id = item.shipping_Sub_job_id;
                    Customer.Mode = item.shipping_shipment_type;
                    Customer.Master_job_id = item.shipping_Master_job_id;
                    Customer.Shppr_Cnee = item.shipping_Consignee_company.ToUpper();
                    Customer.Doctype = item.shipping_Commodity_type;//item.cust_doc_type;
                    Customer.JobType = item.Tbl_shipping_master_job.shipping_Master_job_type;
                    if (Customer.JobType == "Direct")
                    {
                        Customer.vessel_voyag = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Vessel_Name).FirstOrDefault() + " - " + item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Voyage_No).FirstOrDefault();

                        var containerdet = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.tbl_mbl_container.ToList()).FirstOrDefault();
                        if (containerdet != null)
                        {
                            foreach (var container in containerdet)
                            {
                                Customer.Container_no = container.MBL_Container_No + "-" + container.MBL_Container_Type + "~";
                                if (Customer.Mode == "FCL")
                                {
                                    Customer.vol = "FCL";
                                }
                                else
                                {
                                    Customer.vol = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Volume).FirstOrDefault();
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(Customer.vol))
                                Customer.vol = Customer.vol.TrimEnd('-');
                            if (!string.IsNullOrEmpty(Customer.Container_no))
                                Customer.Container_no = Customer.Container_no.TrimEnd('~');
                        }
                        else
                        {
                            if (Customer.Mode != "FCL")
                                Customer.vol = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_Volume).FirstOrDefault();
                        }

                    }
                    else
                    {
                        Customer.vessel_voyag = item.tbl_hbl_details.Select(x => x.HBL_Vessel_Name).FirstOrDefault() + " - " + item.tbl_hbl_details.Select(x => x.HBL_Voyage_No).FirstOrDefault();

                        var shiptype = item.tbl_hbl_details.Select(x => x.HBL_TypeOfMove).FirstOrDefault();

                        var containerdet = item.tbl_hbl_details.Select(x => x.tbl_hbl_container.ToList()).FirstOrDefault();
                        if (containerdet != null)
                        {
                            foreach (var container in containerdet)
                            {
                                Customer.Container_no += container.HBL_Container_No + "-" + container.HBL_Container_Type + "~";
                                if (shiptype == "FCL/FCL")
                                {
                                    Customer.vol = "FCL";
                                }
                                else
                                {
                                    Customer.vol = item.tbl_hbl_details.Select(x => x.HBL_Volume).FirstOrDefault();
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(Customer.vol))
                                Customer.vol = Customer.vol.TrimEnd('-');
                            if (!string.IsNullOrEmpty(Customer.Container_no))
                                Customer.Container_no = Customer.Container_no.TrimEnd('~');
                        }
                        else
                        {
                            if (shiptype != "FCL/FCL")
                                Customer.vol = item.tbl_hbl_details.Select(x => x.HBL_Volume).FirstOrDefault();
                        }
                    }

                    var Chkclrjobno = item.Tbl_SeaClearance_subjob.Where(x => x.SEC_Deleted_SubJob != "Yes").ToList();
                    List<Clr_Det> clrlist = new List<Clr_Det>();
                    if (Chkclrjobno.Count > 0)
                    {
                        foreach (var clrjob in Chkclrjobno)
                        {
                            Clr_Det Clr_det = new Clr_Det();
                            Clr_det.Clr_job_no = clrjob.SeaClearance_Subjob_no;
                            if (string.IsNullOrEmpty(clrjob.SEC_SINV_NO) && !string.IsNullOrEmpty(clrjob.SECRoyalXML))
                            {
                                clrjob.SEC_SINV_NO = GetInvNo(clrjob.SECRoyalXML, "SEC");
                            }
                            if (string.IsNullOrEmpty(clrjob.SEC_SINV_NO))
                            {
                                Clr_det.Party_InvocieNo = Customer.Party_InvocieNo == null ? "" : Customer.Party_InvocieNo;
                            }
                            else
                            {
                                Clr_det.Party_InvocieNo = clrjob.SEC_SINV_NO;
                                Customer.Party_InvocieNo = Clr_det.Party_InvocieNo;
                            }
                            Clr_det.sb_be_no = clrjob.shipping_bill_no;
                            if (string.IsNullOrEmpty(Clr_det.sb_be_no))
                            {
                                Clr_det.sb_be_no = Customer.sb_be_no == null ? "" : Customer.sb_be_no;
                            }
                            else
                            {
                                Clr_det.sb_be_no = clrjob.shipping_bill_no;
                                Customer.sb_be_no = Clr_det.sb_be_no;
                            }
                            Clr_det.Clr_job_id = clrjob.SeaClearance_Subjob_id.ToString();
                            Clr_det.PKG = Convert.ToInt32(clrjob.SeaClearance_Total_packages);
                            Clr_det.GRWT = Convert.ToDecimal(clrjob.SeaClearance_Total_weight).ToString();

                            Clr_det.Doctype = clrjob.SeaClearance_Commodity_type;//clrjob.cust_doc_type;
                            Clr_det.fileref = clrjob.shipping_pdf;
                            Clr_det.department = "SEC";
                            clrlist.Add(Clr_det);
                        }
                    }
                    Customer.Clr_det = clrlist;

                    if (Chkclrjobno.Count == 0)
                    {
                        Customer.PKG = Convert.ToInt32(item.shipping_Total_packages);
                        Customer.GRWT = Convert.ToDecimal(item.shipping_Total_weight).ToString();
                    }


                    var airport_name = item.tbl_seaport_master1.seaport_name;
                    Customer.Branch = item.tbl_seaport_master.seaport_name;

                    Customer.pod_code = item.tbl_seaport_master1.seaport_code;
                    Customer.pol_code = item.tbl_seaport_master.seaport_code;
                    Customer.pod_name = item.tbl_seaport_master1.seaport_name;
                    Customer.pol_name = item.tbl_seaport_master.seaport_name;
                    Customer.Department = "SEF";
                    if (Customer.JobType.ToString() == "Direct")
                    {
                        if (Chkclrjobno.Count == 0)
                            Customer.Party_InvocieNo = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_exp_invoice_no).FirstOrDefault();
                        Customer.ETA = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_ETA).FirstOrDefault();
                        Customer.ETD = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_ETD).FirstOrDefault();
                    }
                    else
                    {
                        if (Chkclrjobno.Count == 0)
                            Customer.Party_InvocieNo = item.tbl_hbl_details.Select(x => x.HBL_exp_invoice_no).FirstOrDefault();
                        Customer.ETA = item.tbl_hbl_details.Select(x => x.HBL_ETA).FirstOrDefault();
                        Customer.ETD = item.tbl_hbl_details.Select(x => x.HBL_ETD).FirstOrDefault();
                    }

                    var mastercount = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_activity_status).ToList();
                    try
                    {
                        if (mastercount.Count > 0)
                        {
                            var masterstatus = mastercount[0];
                            if (masterstatus == true)
                                if (masterstatus == true)
                                {
                                    Customer.STATUS = "1";
                                }
                                else
                                {
                                    Customer.STATUS = "2";
                                }
                        }
                        else
                        {
                            var subjobcount = subjob;
                            if (subjobcount.Count > 0)
                            {
                                int z = 0;
                                for (; z < subjobcount.Count;)
                                {
                                    var hawbstaus = subjobcount[z].tbl_hbl_details.Select(x => x.HBL_activity_status).FirstOrDefault();
                                    if (hawbstaus == true)
                                    {
                                        Customer.STATUS = "3";
                                        z++;
                                        break;
                                    }
                                    else
                                    {
                                        Customer.STATUS = "4";
                                        break;
                                    }
                                }
                            }
                            else
                            {

                                Customer.STATUS = "5";
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Customer.STATUS = "6";

                    }

                    Customer.Destination = airport_name.ToUpper().ToString();
                    Customer.HAWB = item.tbl_hbl_details.Select(x => x.HBL_BLNumber).FirstOrDefault();
                    var Mbl = item.Tbl_shipping_master_job.tbl_mbl_details.Select(x => x.MBL_BLNumber).FirstOrDefault();
                    Customer.MAWB = Mbl == null ? "" : Mbl.ToUpper();


                    if (string.IsNullOrEmpty(Customer.HAWB))
                    {
                        Customer.HAWB = Customer.HAWB == null ? "" : Customer.MAWB;
                    }


                    if (string.IsNullOrEmpty(Customer.MAWB))
                    {
                        Customer.MAWB = Customer.MAWB == null ? "" : Customer.HAWB;
                    }

                    //Document Details
                    var mblfilref = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid == item.shipping_Master_job_id && x.doc_locker_docno == 47).Select(x => x.doc_locker_file).FirstOrDefault();
                    if (mblfilref == null)
                    {
                        var hblfilref = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid == item.shipping_Sub_job_id && x.doc_locker_docno == 48).Select(x => x.doc_locker_file).FirstOrDefault();
                        Customer.Hawb_Hbl_Fileref = hblfilref;
                    }
                    Customer.Mawb_Mbl_Fileref = mblfilref;
                    if (string.IsNullOrEmpty(Customer.Hawb_Hbl_Fileref))
                    {
                        Customer.Hawb_Hbl_Fileref = Customer.Hawb_Hbl_Fileref == null ? "" : Customer.Hawb_Hbl_Fileref;
                    }
                    if (string.IsNullOrEmpty(Customer.Mawb_Mbl_Fileref))
                    {
                        Customer.Mawb_Mbl_Fileref = Customer.Mawb_Mbl_Fileref == null ? "" : Customer.Mawb_Mbl_Fileref;
                    }

                    Cost_details.Add(Customer);

                }

                var clrsubjob = _context.Tbl_SeaClearance_subjob.Where(s => s.SeaClearance_Shipper_company == unit_no.ToString() && s.SEC_Deleted_SubJob != "Yes" && s.SeaClearance_Reference_type == 0 && s.SeaExport_Subjob_id == null && s.SeaClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        clrsubjob = _context.Tbl_SeaClearance_subjob.Where(s => s.SeaClearance_Reference_company == unit_no && s.SEC_Deleted_SubJob != "Yes" && s.SeaClearance_Reference_type == 1 && s.SeaExport_Subjob_id == null && s.SeaClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        clrsubjob = _context.Tbl_SeaClearance_subjob.Where(s => s.SeaClearance_Reference_company == unit_no && s.SEC_Deleted_SubJob != "Yes" && s.SeaClearance_Reference_type == 2 && s.SeaExport_Subjob_id == null && s.SeaClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                }
                foreach (var item in clrsubjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.SeaClearance_Subjob_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.SeaClearance_Created_on_datetime);
                    Customer.Sub_job_id = item.SeaClearance_Subjob_id;
                    Customer.Master_job_id = Convert.ToInt32(item.SeaClearance_Master_job_id);
                    Customer.JobType = "con";
                    Customer.Department = "SEC";
                    Customer.Shppr_Cnee = item.SeaClearance_Consignee_company.ToUpper();
                    Customer.Doctype = item.SeaClearance_Commodity_type;//item.cust_doc_type;
                    Customer.sb_be_no = item.shipping_bill_no;
                    Customer.sb_be_no = item.shipping_bill_no;
                    Customer.Mode = item.SeaClearance_shiptype;
                    Customer.fileref = item.shipping_pdf;
                    Customer.MAWB = "only clearance";
                    Customer.HAWB = "only clearance";
                    Customer.PKG = Convert.ToInt32(item.SeaClearance_Total_packages);
                    Customer.GRWT = Convert.ToDecimal(item.SeaClearance_Total_weight).ToString();
                    Customer.Destination = item.tbl_seaport_master.seaport_name.ToUpper();
                    Customer.pod_code = item.tbl_seaport_master.seaport_code.ToUpper();
                    if (!string.IsNullOrEmpty(item.shipping_pdf)) Customer.STATUS = "1";
                    else Customer.STATUS = "2";

                    switch (item.SEC_segment_no)
                    {
                        case 0:
                            Customer.Branch = "NHAVA SHEVA";
                            Customer.pol_code = "INNSA";
                            break;
                        case 1:
                            Customer.Branch = "CHENNAI";
                            Customer.pol_code = "INMAA";
                            break;
                        case 2:
                            Customer.Branch = "MUNDRA";
                            Customer.pol_code = "INMUN1";
                            break;
                    }
                    if (string.IsNullOrEmpty(item.SEC_SINV_NO) && !string.IsNullOrEmpty(item.SECRoyalXML))
                    {
                        item.SEC_SINV_NO = GetInvNo(item.SECRoyalXML, "SEC");
                    }
                    Customer.Party_InvocieNo = item.SEC_SINV_NO;
                    List<Clr_Det> clrlist = new List<Clr_Det>();
                    Clr_Det Clr_det = new Clr_Det();
                    Clr_det.Clr_job_no = item.SeaClearance_Subjob_no;
                    Clr_det.Clr_job_id = item.SeaClearance_Subjob_id.ToString();
                    Clr_det.PKG = Convert.ToInt32(item.SeaClearance_Total_packages);
                    Clr_det.GRWT = Convert.ToDecimal(item.SeaClearance_Total_weight).ToString();
                    Clr_det.Party_InvocieNo = item.SEC_SINV_NO;
                    Clr_det.sb_be_no = item.shipping_bill_no;
                    Clr_det.Doctype = item.SeaClearance_Commodity_type;//item.cust_doc_type;
                    Clr_det.fileref = item.shipping_pdf;
                    clrlist.Add(Clr_det);
                    Customer.Clr_det = clrlist;
                    Customer.Department = "SEC";
                    Cost_details.Add(Customer);

                }
            }

            if (Session["Search_By"].ToString() != "" && Session["Seach_parameter"].ToString() != "")
            {
                var Search_By = Session["Search_By"].ToString();
                var Seach_parameter = Session["Seach_parameter"].ToString();


                switch (Search_By)
                {
                    case "JOB":
                        Cost_details = Cost_details.Where(x => x.Sub_job_no.Contains(Seach_parameter)).ToList();
                        break;
                    case "MAWB":

                        Cost_details = Cost_details.Where(x => x.MAWB != null).ToList();
                        Cost_details = Cost_details.Where(x => x.MAWB.Contains(Seach_parameter) || x.HAWB.Contains((Seach_parameter))).ToList();


                        break;
                    case "POD":
                        Cost_details = Cost_details.Where(x => x.Destination.StartsWith(Seach_parameter)).ToList();

                        break;
                    case "CONSIGNEE":
                        Cost_details = Cost_details.Where(x => x.Shppr_Cnee.Contains(Seach_parameter)).ToList();
                        break;

                    case "INVOICE":
                        if (dept == "AI" || dept == "SI")
                        {
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter)).ToList();
                        }
                        else
                        {
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter) || x.Clr_det.All(y => y.Party_InvocieNo.Contains(Seach_parameter))).ToList();
                        }


                        break;


                    case "SB":
                        if (dept == "AI" || dept == "SI")
                        {
                            Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
                            Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter)).ToList();
                        }
                        else
                        {
                            Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
                            Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter) || x.Clr_det.All(y => y.sb_be_no.Contains(Seach_parameter))).ToList();
                        }

                        break;
                }
            }

            var From_Dtt = Session["From_Dtt"].ToString();
            var To_Dtt = Session["To_Dtt"].ToString();
            if (!string.IsNullOrEmpty(From_Dtt) && !string.IsNullOrEmpty(To_Dtt))
            {
                From_Dtt += " 00:00:00";
                To_Dtt += " 23:59:00";

                DateTime FromDt = DateTime.ParseExact(From_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                DateTime ToDt = DateTime.ParseExact(To_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                Cost_details = Cost_details.Where(d => d.Sub_job_date >= FromDt && d.Sub_job_date <= ToDt).ToList();
            }

            if (Session["Stauts"].ToString() != "")
            {
                var Stauts = Session["Stauts"].ToString();
                if (Stauts == "Completed")
                {
                    string completed = "1";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
                else

                {
                    string completed = "2";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
            }
            Cost_details = Cost_details.OrderByDescending(X => X.Sub_job_date).ToList();
            ViewBag.Cost_details = Cost_details;
            TempData["Cost_details"] = Cost_details;
            Session["Cost_details"] = Cost_details;
            return View(user);
        }

        [SessionTimeOut]
        public ActionResult SI_DB()
        {
            string dept = Session["dept"].ToString();
            CustomerModal user = new CustomerModal();
            user.tbl_Crm_User_s = _context.tbl_crm_user_master.ToList();
            var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
            List<Customer> Cost_details = new List<Customer>();

            var userInDB = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).FirstOrDefault();
            List<int?> crm_unt_business_unit_no = new List<int?>();
            var Bu_unit_no = userInDB.tbl_crm_business_unit.Select(x => x.crm_unt_business_unit_no).ToList();
            crm_unt_business_unit_no.AddRange(Bu_unit_no);
            if (Session["docno"].ToString() != "0")
            {
                crm_unt_business_unit_no = new List<int?>();
                crm_unt_business_unit_no.Add(Convert.ToInt32(Session["docno"].ToString()));
            }

            var priviewfinyear = Session["financialyear"].ToString();//financial year
            var newfinanceyear = priviewfinyear.Split('-')[1];

            foreach (var unit_no in crm_unt_business_unit_no)
            {
                var subjob = _context.Tbl_SeaImport_sub_job.Where(s => s.SeaImport_Shipper_company == unit_no.ToString() && s.SIF_Deleted_SubJob != "Yes" && s.SeaImport_Reference_type == 0 && s.SeaImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        subjob = _context.Tbl_SeaImport_sub_job.Where(s => s.SeaImport_Reference_company == unit_no && s.SIF_Deleted_SubJob != "Yes" && s.SeaImport_Reference_type == 1 && s.SeaImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        subjob = _context.Tbl_SeaImport_sub_job.Where(s => s.SeaImport_Reference_company == unit_no && s.SIF_Deleted_SubJob != "Yes" && s.SeaImport_Reference_type == 2 && s.SeaImport_Sub_job_no.Contains("/" + priviewfinyear)).ToList();
                        break;

                }
                foreach (var item in subjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.SeaImport_Sub_job_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.SeaImport_Created_on_datetime);
                    Customer.Sub_job_id = item.SeaImport_Sub_job_id;
                    Customer.Master_job_id = item.SeaImport_Master_job_id;
                    Customer.JobType = "CON";
                    Customer.Department = "SIF";
                    Customer.Shppr_Cnee = item.SeaImport_Consignee_company.ToUpper();
                    if (Customer.JobType.ToString() == "Direct")
                    {
                        Customer.Party_InvocieNo = "";
                    }
                    else
                    {
                        Customer.Party_InvocieNo = "";

                    }

                    var shiptype = item.SeaImport_Shipment_Type;
                    var containerdet = item.Tbl_SeaImport_HBL_Container.ToList();
                    foreach (var container in containerdet)
                    {
                        Customer.Container_no = container.SeaImport_HBL_Container_No + "-";
                        if (shiptype == "FCL")
                        {
                            Customer.vol = container.SeaImport_HBL_Container_Type + "-";
                        }
                        else
                        {
                            Customer.vol = item.SeaImport_Shipment_Volume.ToString();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(Customer.vol))
                        Customer.vol = Customer.vol.TrimEnd('-');
                    if (!string.IsNullOrEmpty(Customer.vol))
                        Customer.Container_no = Customer.Container_no.TrimEnd('-');


                    Customer.Mode = item.SeaImport_Shipment_Type;
                    Customer.PKG = Convert.ToInt32(item.SeaImport_Package_Count);
                    Customer.GRWT = Convert.ToDecimal(item.SeaImport_Gross_Weight).ToString();
                    var airport_name = item.tbl_seaport_master.seaport_name;
                    Customer.pod_code = item.tbl_seaport_master.seaport_code;
                    var mastercount = item.Tbl_SeaImport_master_job.SeaImport_Activity_Status;
                    Customer.ETD = item.Tbl_SeaImport_master_job.SeaImport_ETD;
                    Customer.ETA = item.Tbl_SeaImport_master_job.SeaImport_ETA;

                    var chkarrival = _context.Tbl_SeaImport_Arrival.Where(x => x.SeaImport_arrival_subjob_id == item.SeaImport_Sub_job_id).Select(x => x.SeaImport_arrival_ETA).FirstOrDefault();
                    if (chkarrival != null)
                        Customer.ETA = chkarrival;

                    var vesselname = _context.Tbl_SeaImport_Arrival.Where(x => x.SeaImport_arrival_subjob_id == item.SeaImport_Sub_job_id).Select(x => x.SeaImport_arrival_vessel_name).FirstOrDefault();
                    if (vesselname != null)
                        Customer.vessel_voyag = vesselname;
                    var voyag = _context.Tbl_SeaImport_Arrival.Where(x => x.SeaImport_arrival_subjob_id == item.SeaImport_Sub_job_id).Select(x => x.SeaImport_arrival_Voyage).FirstOrDefault();
                    if (voyag != null)
                        Customer.vessel_voyag += " - " + voyag;


                    if (mastercount == true)
                    {
                        Customer.STATUS = "1";/* "<p class='fas fa-check' style='color:green;width:20px;text-align:center;'></p>";*/
                    }
                    else if (mastercount == false)
                    {
                        Customer.STATUS = "2";/* "<p class='fas fa-check' style='color:green;width:20px;text-align:center;'></p>";*/
                    }
                    else
                    {
                        Customer.STATUS = "3"; /*"<p class='las la-user-clock' style='color:#fd397a;font-size:20px;position:relative;top:2px;'></p>";*/
                    }

                    switch (item.SIF_segment_no)
                    {
                        case 0:
                            Customer.Branch = "MUMBAI";
                            Customer.pol_code = "INNSA";
                            break;
                        case 1:
                            Customer.Branch = "CHENNAI";
                            Customer.pol_code = "INMAA";
                            break;
                        case 2:
                            Customer.Branch = "AHMEDABAD";
                            Customer.pol_code = "INMUN1";
                            break;
                    }
                    Customer.Destination = airport_name.ToUpper().ToString();
                    Customer.HAWB = item.SeaImport_Hbl_No;
                    var mawb = item.Tbl_SeaImport_master_job.SeaImport_Mbl_No.ToUpper();
                    if (Customer.HAWB == null)
                    {
                        Customer.HAWB = "";
                    }
                    else
                    {
                        Customer.HAWB = Customer.HAWB.ToUpper();
                    }


                    if (Customer.MAWB == null)
                    {
                        Customer.MAWB = "";
                    }
                    else
                    {
                        Customer.MAWB = Customer.MAWB.ToUpper();
                    }
                    Customer.MAWB = mawb.ToUpper();
                    Cost_details.Add(Customer);
                }

                var clrsubjob = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Shipper_company == unit_no.ToString() && s.
                SIC_Deleted_SubJob != "Yes" && s.SeaImpClearance_Reference_type == 0 && s.SeaImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                switch (userInDB.crm_user_party_type)
                {
                    case "LOCAL AGENT":
                        clrsubjob = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Reference_company == unit_no && s.SIC_Deleted_SubJob != "Yes" && s.SeaImpClearance_Reference_type == 1 && s.SeaImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                    case "LOCAL CUSTOMER":
                        break;
                    default:
                        clrsubjob = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Reference_company == unit_no && s.SIC_Deleted_SubJob != "Yes" && s.SeaImpClearance_Reference_type == 2 && s.SeaImpClearance_Subjob_no.Contains("/" + priviewfinyear)).ToList();
                        break;
                }
                foreach (var item in clrsubjob)
                {
                    Customer Customer = new Customer();
                    Customer.Sub_job_no = item.SeaImpClearance_Subjob_no;
                    Customer.Sub_job_date = Convert.ToDateTime(item.SeaImpClearance_Created_on_datetime);
                    Customer.Sub_job_id = item.SeaImpClearance_Subjob_id;
                    Customer.Master_job_id = Convert.ToInt32(item.SeaImpClearance_Master_job_id);
                    Customer.JobType = "con";
                    Customer.Shppr_Cnee = item.SeaImpClearance_Consignee_company.ToUpper();
                    Customer.Mode = item.SeaImpClearance_shiptype;
                    Customer.fileref = item.billof_entry_pdf;
                    Customer.PKG = Convert.ToInt32(item.SeaImpClearance_Total_packages);
                    Customer.GRWT = Convert.ToDecimal(item.SeaImpClearance_Total_weight).ToString();
                    var airport_name = item.tbl_seaport_master.seaport_name;
                    Customer.pod_code = item.tbl_seaport_master.seaport_name;
                    if (!string.IsNullOrEmpty(item.billof_entry_pdf)) Customer.STATUS = "1";
                    else Customer.STATUS = "2";
                    Customer.Department = "SIC";
                    switch (item.SIC_segment_no)
                    {
                        case 0:
                            Customer.Branch = "MUMBAI";
                            Customer.Branch = "INNSA";
                            break;
                        case 1:
                            Customer.Branch = "CHENNAI";
                            Customer.Branch = "INMAA";
                            break;
                        case 2:
                            Customer.Branch = "AHMEDABAD";
                            Customer.Branch = "INMUN1";
                            break;
                    }

                    Customer.Destination = airport_name.ToUpper().ToString();
                    Customer.HAWB = item.SeaImpClearance_hbl;
                    var sic_mawb = item.SeaImpClearance_mbl;
                    Customer.MAWB = sic_mawb;

                    if (Customer.HAWB == null)
                    {
                        Customer.HAWB = "";
                    }
                    else
                    {
                        Customer.HAWB = Customer.HAWB.ToUpper();
                    }


                    if (Customer.MAWB == null)
                    {
                        Customer.MAWB = "";
                    }
                    else
                    {
                        Customer.MAWB = Customer.MAWB;
                    }


                    Customer.Doctype = item.SeaImpClearance_Commodity_type;//item.cust_doc_type;
                    Customer.sb_be_no = item.billof_entry_no;
                    if (string.IsNullOrEmpty(item.SIC_SINV_NO) && !string.IsNullOrEmpty(item.SICRoyalXML))
                    {
                        item.SIC_SINV_NO = GetInvNo(item.SICRoyalXML, "SIC");
                    }
                    Customer.Party_InvocieNo = item.SIC_SINV_NO;

                    Cost_details.Add(Customer);
                }

            }

            if (Session["Search_By"].ToString() != "" && Session["Seach_parameter"].ToString() != "")
            {
                var Search_By = Session["Search_By"].ToString();
                var Seach_parameter = Session["Seach_parameter"].ToString();


                switch (Search_By)
                {
                    case "JOB":
                        Cost_details = Cost_details.Where(x => x.Sub_job_no.Contains(Seach_parameter)).ToList();
                        break;
                    case "MAWB":

                        Cost_details = Cost_details.Where(x => x.MAWB != null).ToList();
                        Cost_details = Cost_details.Where(x => x.MAWB.Contains(Seach_parameter) || x.HAWB.Contains((Seach_parameter))).ToList();


                        break;
                    case "POD":
                        Cost_details = Cost_details.Where(x => x.Destination.StartsWith(Seach_parameter)).ToList();

                        break;
                    case "CONSIGNEE":
                        Cost_details = Cost_details.Where(x => x.Shppr_Cnee.Contains(Seach_parameter)).ToList();
                        break;

                    case "INVOICE":
                        if (dept == "AI" || dept == "SI")
                        {
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter)).ToList();
                        }
                        else
                        {
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo != null).ToList();
                            Cost_details = Cost_details.Where(x => x.Party_InvocieNo.Contains(Seach_parameter) || x.Clr_det.All(y => y.Party_InvocieNo.Contains(Seach_parameter))).ToList();
                        }


                        break;


                    case "SB":
                        if (dept == "AI" || dept == "SI")
                        {
                            Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
                            Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter)).ToList();
                        }
                        else
                        {
                            Cost_details = Cost_details.Where(x => x.sb_be_no != null).ToList();
                            Cost_details = Cost_details.Where(x => x.sb_be_no.Contains(Seach_parameter) || x.Clr_det.All(y => y.sb_be_no.Contains(Seach_parameter))).ToList();
                        }

                        break;
                }
            }

            var From_Dtt = Session["From_Dtt"].ToString();
            var To_Dtt = Session["To_Dtt"].ToString();
            if (!string.IsNullOrEmpty(From_Dtt) && !string.IsNullOrEmpty(To_Dtt))
            {
                From_Dtt += " 00:00:00";
                To_Dtt += " 23:59:00";

                DateTime FromDt = DateTime.ParseExact(From_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                DateTime ToDt = DateTime.ParseExact(To_Dtt, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                Cost_details = Cost_details.Where(d => d.Sub_job_date >= FromDt && d.Sub_job_date <= ToDt).ToList();
            }

            if (Session["Stauts"].ToString() != "")
            {
                var Stauts = Session["Stauts"].ToString();
                if (Stauts == "Completed")
                {
                    string completed = "1";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
                else

                {
                    string completed = "2";
                    Cost_details = Cost_details.Where(x => x.STATUS == completed).ToList();
                }
            }
            Cost_details = Cost_details.OrderByDescending(X => X.Sub_job_date).ToList();
            ViewBag.Cost_details = Cost_details;
            TempData["Cost_details"] = Cost_details;
            Session["Cost_details"] = Cost_details;
            return View(user);
        }

        [SessionTimeOut]
        public string GetInvNo(string xmldata, string dept)
        {
            xmldata = xmldata.Replace("soap:", "");
            XNamespace ns = "http://tempuri.org/";
            var invoiceno = "";
            var xDoc = XDocument.Parse(xmldata);
            XmlDocument xmldoc = new XmlDocument();
            var finalchecklist = xDoc.Descendants(ns + "Ryal_Imp_Data").FirstOrDefault().Value;
            finalchecklist = finalchecklist.Replace("&", "AND");
            switch (dept)
            {
                case "AEC":
                    try
                    {
                        xmldoc.LoadXml(finalchecklist);
                        XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                        foreach (XmlNode fsu in fsunodes)
                        {
                            invoiceno = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_NO"].InnerText;
                        }
                    }
                    catch (Exception)
                    {

                    }
                    break;
                case "SEC":
                    try
                    {
                        xmldoc.LoadXml(finalchecklist);
                        XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                        foreach (XmlNode fsu in fsunodes)
                        {
                            invoiceno = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_NO"].InnerText;
                        }
                    }
                    catch (Exception)
                    {

                    }
                    break;
                case "AIC":
                    finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_NO>");
                    finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_NO>");
                    xmldoc.LoadXml(finalchecklist);
                    try
                    {
                        XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                        foreach (XmlNode fsu in fsunodes)
                        {
                            invoiceno = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["INVOICE_NO"].InnerText;
                        }
                    }
                    catch (Exception)
                    {

                    }
                    break;
                case "SIC":
                    finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_NO>");
                    finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_NO>");
                    finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>3</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>3</INVOICE_SLNO><INVOICE_NO>");
                    finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>4</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>4</INVOICE_SLNO><INVOICE_NO>");
                    finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>5</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>5</INVOICE_SLNO><INVOICE_NO>");
                    xmldoc.LoadXml(finalchecklist);
                    try
                    {
                        XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                        foreach (XmlNode fsu in fsunodes)
                        {
                            invoiceno = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["INVOICE_NO"].InnerText;
                        }
                    }
                    catch (Exception)
                    {

                    }
                    break;
            }
            return invoiceno;
        }

        /// <summary>
        ///Direct Locker
        /// </summary>
        /// <param name="masterjobid"></param>
        /// <param name="job_dept"></param>
        /// <returns></returns>
        /// 
        [SessionTimeOut]
        [HttpPost]
        public ActionResult Get_Doclocker_Direct(string masterjobid, string job_dept)
        {
            var checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == masterjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "AEF-MAWB").ToList();
            if (job_dept != "AEF")
            {
                checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == masterjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "SEF-MBL").ToList();
            }

            var lockerdata = (from locker in checksaveupdate
                              select new
                              {
                                  lockerid = locker.doc_locker_id,
                                  docno = locker.doc_locker_docno,
                                  docname = locker.tbl_document_master.document_name,
                                  docremark = locker.doc_locker_remark,
                                  lockerfile = locker.doc_locker_file,
                                  lockerby = locker.tbl_user_master.tbl_address_book.add_contact_fname + " " + locker.tbl_user_master.tbl_address_book.add_contact_mname + " " + locker.tbl_user_master.tbl_address_book.add_contact_lname,
                                  lockeron = locker.doc_locker_on.ToString(),
                              });
            return Json(lockerdata, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// Console
        /// </summary>
        /// <param name="subjobid"></param>
        /// <param name="job_dept"></param>
        /// <returns></returns>
        /// 
        [SessionTimeOut]
        [HttpPost]
        public ActionResult Get_Doclocker_Consol(string subjobid, string job_dept)
        {
            var checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "AEF-HAWB").ToList();
            switch (job_dept)
            {
                case "AEC":
                    checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "AEC-CLEARANCE").ToList();
                    break;
                case "AIF":
                    checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "AIF-HAWB").ToList();
                    break;
                case "AIC":
                    checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "AIC-CLEARANCE").ToList();
                    break;
                case "SEC":
                    checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "SEC-CLEARANCE").ToList();
                    break;
                case "SEF":
                    checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "SEF-HBL").ToList();
                    break;
                case "SIC":
                    checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "SIC-CLEARANCE").ToList();
                    break;
                case "SIF":
                    checksaveupdate = _context.tbl_document_locker.Where(x => x.doc_locker_sub_master_jobid.ToString() == subjobid && x.tbl_document_master.document_type != "INTERNAL" && x.doc_locker_activity == "SIF-HBL").ToList();
                    break;
            }

            var lockerdata = (from locker in checksaveupdate
                              select new
                              {
                                  lockerid = locker.doc_locker_id,
                                  docno = locker.doc_locker_docno,
                                  docname = locker.tbl_document_master.document_name,
                                  docremark = locker.doc_locker_remark,
                                  lockerfile = locker.doc_locker_file,
                                  lockerby = locker.tbl_user_master.tbl_address_book.add_contact_fname + " " + locker.tbl_user_master.tbl_address_book.add_contact_mname + " " + locker.tbl_user_master.tbl_address_book.add_contact_lname,
                                  lockeron = locker.doc_locker_on.ToString(),
                              });
            return Json(lockerdata, JsonRequestBehavior.AllowGet);
        }

        [SessionTimeOut]
        [HttpPost]
        public ActionResult changedfinance(string finance)
        {
            try
            {
                Session["financialyear"] = finance.Substring(0, 2) + "-" + finance.Substring(2, 2);
                Session["financial_year"] = finance;
                var preyear = Convert.ToInt32(finance.Substring(2, 2));
                Session["fullfinancial"] = "20" + (preyear - 1) + "-20" + finance.Substring(2, 2);
                Session["mindatefinanceyear"] = "4/1/" + "20" + (preyear - 1);
            }
            catch (Exception)
            {
                var fy = new Save_Edit_Search_Delete_AuditLog();
                finance = fy.GetFinancialYear();
                Session["financialyear"] = finance.Substring(0, 2) + "-" + finance.Substring(2, 2);
                Session["financial_year"] = finance;
                var preyear = Convert.ToInt32(finance.Substring(2, 2));
                Session["fullfinancial"] = "20" + (preyear - 1) + "-20" + finance.Substring(2, 2);
                Session["mindatefinanceyear"] = "4/1/" + "20" + (preyear - 1);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [SessionTimeOut]
        [HttpPost]
        public JsonResult changedcompany(string compno)
        {
            Session["docno"] = compno;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Change department
        /// </summary>
        /// <param name="dept"></param>
        /// <returns></returns>
        /// 
        [SessionTimeOut]
        [HttpPost]
        public JsonResult changedepartment(string dept)
        {
            Session["dept"] = dept;
            return Json(dept, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// invoice list
        /// </summary>
        /// <param name="subjobid"></param>
        /// <param name="deptype1"></param>
        /// <returns></returns>
        /// 
        [SessionTimeOut]
        [HttpPost]
        public ActionResult Get_Invoice_Data(string subjobid, string deptype1)
        {
            var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
            var userInDB = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).FirstOrDefault();
            List<string> crm_unt_business_unit_no = new List<string>();
            var Bu_unit_no = userInDB.tbl_crm_business_unit.Select(x => x.tbl_company_business_unit.unit_name).ToList();
            crm_unt_business_unit_no.AddRange(Bu_unit_no);
            if (Session["docno"].ToString() != "0")
            {
                crm_unt_business_unit_no = new List<string>();
                var unitno = Convert.ToInt32(Session["docno"].ToString());
                var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no == unitno).Select(x => x.unit_name).FirstOrDefault();
                crm_unt_business_unit_no.Add(unitname);
            }
            var invoice_Details = new List<invoicedetails>();
            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            int Subjobid = Convert.ToInt32(subjobid);
            var InvoiceValue = _context.tbl_airexport_billing.Where(x => x.AirExportSubJobId == Subjobid && crm_unt_business_unit_no.Contains(x.AirExportBilling_party) && x.BillingType != "B" && x.billing_document_type == deptype1 && x.Billing_Deleted != "Yes").Select(i => new { i.AirExportInvoice_No, i.AirExportInvoice_date, i.AirExportTotal_Amount, i.AirExportBilling_id }).ToList();
            foreach (var item in InvoiceValue)
            {
                invoicedetails addinv = new invoicedetails();
                addinv.AirExportBilling_id = encryptDecrypt.EncryptString(item.AirExportBilling_id.ToString());
                addinv.AirExportInvoice_No = item.AirExportInvoice_No;
                addinv.AirExportInvoice_date = item.AirExportInvoice_date;
                addinv.AirExportTotal_Amount = item.AirExportTotal_Amount;
                invoice_Details.Add(addinv);
            }
            return Json(invoice_Details, JsonRequestBehavior.AllowGet);
        }
        public List<milestone> milestone_details = new List<milestone>();
        public void AddDetails(milestone adddetailslist)
        {
            milestone_details.Add(adddetailslist);
        }
        [SessionTimeOut]
        [HttpPost]
        public ActionResult Get_Track_Date(string subjobid, string job_dept)
        {
            int Subjobid = Convert.ToInt32(subjobid);

            switch (job_dept)
            {
                case "AEF":
                    var AC_Subjob_id = _context.Tbl_AirClearance_subjob.Where(x => x.AirExport_Subjob_id == Subjobid).Select(i => new { i.AirClearance_Subjob_id, i.cust_doc_type }).FirstOrDefault();
                    var Clr_SublobId = AC_Subjob_id.AirClearance_Subjob_id;
                    var Clr_Sub_doc_type = AC_Subjob_id.cust_doc_type;
                    var milestonelist = _context.tbl_milestone_master.Where(a => a.a_no == 31).OrderBy(x => x.milestone_order).ToList();
                    if (Clr_Sub_doc_type == "RMS")
                        milestonelist = milestonelist.Where(x => x.milestone_catg == "RMS").ToList();
                    foreach (var milestone in milestonelist)
                    {
                        var milestone_id = milestone.m_no;
                        var AEC_Milestone = _context.tbl_AEC_Milestone.Where(x => x.AEC_Subjob_id == Subjobid && x.milestone_id == milestone_id).FirstOrDefault();
                        milestone milestone_det = new milestone();
                        milestone_det.milestonename = milestone.milestone_name;
                        if (AEC_Milestone == null)
                        {
                            milestone_det.query_data = "";
                            milestone_det.resolve_data = "";
                            milestone_det.milestone_status = "New";
                            milestone_det.images = null;
                        }
                        else
                        {
                            if (AEC_Milestone.remark_datetime == null && AEC_Milestone.resolve_datetime == null)
                            {
                                milestone_det.query_data = "";
                                milestone_det.resolve_data = "";
                                milestone_det.milestone_status = "New";
                                milestone_det.images = null;
                            }
                            else
                            {
                                if (AEC_Milestone.Query_by > 0)
                                {
                                    milestone_det.query_data = AEC_Milestone.tbl_query_remarks.remark_text + "\n" + AEC_Milestone.tbl_user_master_App.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AEC_Milestone.remark_datetime));
                                    if (AEC_Milestone.Resolve_by > 0)
                                        milestone_det.resolve_data = AEC_Milestone.comment + "\n" + AEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AEC_Milestone.resolve_datetime));
                                }
                                else
                                {
                                    if (AEC_Milestone.Resolve_by > 0)
                                        milestone_det.resolve_data = AEC_Milestone.comment + "\n" + AEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AEC_Milestone.resolve_datetime));
                                }
                                if (AEC_Milestone.Query_by > 0 && AEC_Milestone.Resolve_by == null)
                                    milestone_det.milestone_status = "Query";
                                else if (AEC_Milestone.Query_by > 0 && AEC_Milestone.Resolve_by > 0)
                                    milestone_det.milestone_status = "Resolve";
                                else
                                {
                                    milestone_det.milestone_status = "Completed";
                                }
                                var milestone_image = AEC_Milestone.tbl_AEC_Milestone_Photos.ToList();
                                List<string> milestone_img = new List<string>();
                                foreach (var image in milestone_image)
                                {
                                    milestone_img.Add(image.image_path);
                                }
                                milestone_det.images = milestone_img;
                            }
                        }
                        milestone_details.Add(milestone_det);

                    }
                    break;
                case "AEC":
                    var ClrSub_doc_type = _context.Tbl_AirClearance_subjob.Where(x => x.AirClearance_Subjob_id == Subjobid).Select(i => i.cust_doc_type).FirstOrDefault();

                    var milestonelist1 = _context.tbl_milestone_master.Where(a => a.a_no == 31).OrderBy(x => x.milestone_order).ToList();
                    if (ClrSub_doc_type == "RMS")
                        milestonelist = milestonelist1.Where(x => x.milestone_catg == "RMS").ToList();
                    foreach (var milestone in milestonelist1)
                    {
                        var milestone_id = milestone.m_no;
                        var AEC_Milestone = _context.tbl_AEC_Milestone.Where(x => x.AEC_Subjob_id == Subjobid && x.milestone_id == milestone_id).FirstOrDefault();
                        milestone milestone_det = new milestone();
                        milestone_det.milestonename = milestone.milestone_name;
                        if (AEC_Milestone == null)
                        {
                            milestone_det.query_data = "";
                            milestone_det.resolve_data = "";
                            milestone_det.milestone_status = "New";
                            milestone_det.images = null;
                        }
                        else
                        {
                            if (AEC_Milestone.remark_datetime == null && AEC_Milestone.resolve_datetime == null)
                            {
                                milestone_det.query_data = "";
                                milestone_det.resolve_data = "";
                                milestone_det.milestone_status = "New";
                                milestone_det.images = null;
                            }
                            else
                            {
                                if (AEC_Milestone.Query_by > 0)
                                {
                                    milestone_det.query_data = AEC_Milestone.tbl_query_remarks.remark_text + "\n" + AEC_Milestone.tbl_user_master_App.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AEC_Milestone.remark_datetime));
                                    if (AEC_Milestone.Resolve_by > 0)
                                        milestone_det.resolve_data = AEC_Milestone.comment + "\n" + AEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AEC_Milestone.resolve_datetime));
                                }
                                else
                                {
                                    if (AEC_Milestone.Resolve_by > 0)
                                        milestone_det.resolve_data = AEC_Milestone.comment + "\n" + AEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AEC_Milestone.resolve_datetime));
                                }
                                if (AEC_Milestone.Query_by > 0 && AEC_Milestone.Resolve_by == null)
                                    milestone_det.milestone_status = "Query";
                                else if (AEC_Milestone.Query_by > 0 && AEC_Milestone.Resolve_by > 0)
                                    milestone_det.milestone_status = "Resolve";
                                else
                                {
                                    milestone_det.milestone_status = "Completed";
                                }
                                var milestone_image = AEC_Milestone.tbl_AEC_Milestone_Photos.ToList();
                                List<string> milestone_img = new List<string>();
                                foreach (var image in milestone_image)
                                {
                                    milestone_img.Add(image.image_path);
                                }
                                milestone_det.images = milestone_img;
                            }
                        }
                        milestone_details.Add(milestone_det);

                    }
                    break;
                case "AIF":
                    break;
                case "AIC":
                    var AICSub_doc_type = _context.Tbl_AirImpClearance_subjob.Where(x => x.AirImpClearance_Subjob_id == Subjobid).Select(i => i.cust_doc_type).FirstOrDefault();
                    var AICmilestonelist = _context.tbl_milestone_master.Where(a => a.a_no == 27).OrderBy(x => x.milestone_order).ToList();
                    if (AICSub_doc_type == "RMS")
                        milestonelist = AICmilestonelist.Where(x => x.milestone_catg == "RMS").ToList();
                    foreach (var milestone in AICmilestonelist)
                    {
                        var milestone_id = milestone.m_no;
                        var AIC_Milestone = _context.tbl_AIC_Milestone.Where(x => x.AIC_Subjob_id == Subjobid && x.AIC_milestone_id == milestone_id).FirstOrDefault();
                        milestone milestone_det = new milestone();
                        milestone_det.milestonename = milestone.milestone_name;
                        if (AIC_Milestone == null)
                        {
                            milestone_det.query_data = "";
                            milestone_det.resolve_data = "";
                            milestone_det.milestone_status = "New";
                            milestone_det.images = null;
                        }
                        else
                        {
                            if (AIC_Milestone.AIC_remark_datetime == null && AIC_Milestone.AIC_resolve_datetime == null)
                            {
                                milestone_det.query_data = "";
                                milestone_det.resolve_data = "";
                                milestone_det.milestone_status = "New";
                                milestone_det.images = null;
                            }
                            else
                            {
                                if (AIC_Milestone.AIC_Query_by > 0)
                                {
                                    milestone_det.query_data = AIC_Milestone.tbl_query_remarks.remark_text + "\n" + AIC_Milestone.tbl_user_master_App.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AIC_Milestone.AIC_remark_datetime));
                                    if (AIC_Milestone.AIC_Resolve_by > 0)
                                        milestone_det.resolve_data = AIC_Milestone.AIC_comment + "\n" + AIC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AIC_Milestone.AIC_resolve_datetime));
                                }
                                else
                                {
                                    if (AIC_Milestone.AIC_Resolve_by > 0)
                                        milestone_det.resolve_data = AIC_Milestone.AIC_comment + "\n" + AIC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", AIC_Milestone.AIC_resolve_datetime));
                                }
                                if (AIC_Milestone.AIC_Query_by > 0 && AIC_Milestone.AIC_Resolve_by == null)
                                    milestone_det.milestone_status = "Query";
                                else if (AIC_Milestone.AIC_Query_by > 0 && AIC_Milestone.AIC_Resolve_by > 0)
                                    milestone_det.milestone_status = "Resolve";
                                else
                                {
                                    milestone_det.milestone_status = "Completed";
                                }
                                var milestone_image = AIC_Milestone.tbl_AIC_Milestone_Photos.ToList();
                                List<string> milestone_img = new List<string>();
                                foreach (var image in milestone_image)
                                {
                                    milestone_img.Add(image.AIC_image_path);
                                }
                                milestone_det.images = milestone_img;
                            }
                        }
                        milestone_details.Add(milestone_det);

                    }
                    break;
                case "SIF":
                    break;
                case "SIC":
                    var SICSub_doc_type = _context.Tbl_SeaImpClearance_subjob.Where(x => x.SeaImpClearance_Subjob_id == Subjobid).Select(i => i.cust_doc_type).FirstOrDefault();

                    var shiptype2 = _context.Tbl_SeaImpClearance_subjob.Where(x => x.SeaImpClearance_Subjob_id == Subjobid).Select(i => i.SeaImpClearance_shiptype).FirstOrDefault();

                    var SICmilestonelist = _context.tbl_milestone_master.Where(a => a.a_no == 25).OrderBy(x => x.milestone_order).ToList();


                    if (SICSub_doc_type == "RMS")
                    {
                        milestonelist = SICmilestonelist.Where(x => x.milestone_catg == "RMS").ToList();
                        SICmilestonelist = SICmilestonelist.Where(x => x.milestone_catg == "RMS").ToList();
                    }


                    if (shiptype2 != "FCL")
                    {
                        milestonelist = SICmilestonelist.Where(x => x.milestone_stuffing_type == "lcl" || x.milestone_stuffing_type == "all").ToList();
                        SICmilestonelist = SICmilestonelist.Where(x => x.milestone_stuffing_type == "lcl" || x.milestone_stuffing_type == "all").ToList();
                    }
                    else
                    {
                        var stufftype = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaClearance_Subjob_id == Subjobid).Select(i => i.sec_stuffing_type).FirstOrDefault();
                        if (stufftype == "Dock Stuffing")
                        {
                            milestonelist = SICmilestonelist.Where(x => x.milestone_stuffing_type == "fcl dock" || x.milestone_stuffing_type == "all").ToList();
                            SICmilestonelist = SICmilestonelist.Where(x => x.milestone_stuffing_type == "fcl dock" || x.milestone_stuffing_type == "all").ToList();
                        }
                        else
                        {
                            milestonelist = SICmilestonelist.Where(x => x.milestone_stuffing_type == "fcl factory" || x.milestone_stuffing_type == "all").ToList();
                            SICmilestonelist = SICmilestonelist.Where(x => x.milestone_stuffing_type == "fcl factory" || x.milestone_stuffing_type == "all").ToList();
                        }
                    }

                    foreach (var milestone in SICmilestonelist)
                    {
                        var milestone_id = milestone.m_no;
                        var SIC_Milestone = _context.tbl_SIC_Milestone.Where(x => x.SIC_Subjob_id == Subjobid && x.SIC_milestone_id == milestone_id).FirstOrDefault();
                        milestone milestone_det = new milestone();
                        milestone_det.milestonename = milestone.milestone_name;
                        if (SIC_Milestone == null)
                        {
                            milestone_det.query_data = "";
                            milestone_det.resolve_data = "";
                            milestone_det.milestone_status = "New";
                            milestone_det.images = null;
                        }
                        else
                        {
                            if (SIC_Milestone.SIC_remark_datetime == null && SIC_Milestone.SIC_resolve_datetime == null)
                            {
                                milestone_det.query_data = "";
                                milestone_det.resolve_data = "";
                                milestone_det.milestone_status = "New";
                                milestone_det.images = null;
                            }
                            else
                            {
                                if (SIC_Milestone.SIC_Query_by > 0)
                                {
                                    milestone_det.query_data = SIC_Milestone.tbl_query_remarks.remark_text + "\n" + SIC_Milestone.tbl_user_master_App.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SIC_Milestone.SIC_remark_datetime));
                                    if (SIC_Milestone.SIC_Resolve_by > 0)
                                        milestone_det.resolve_data = SIC_Milestone.SIC_comment + "\n" + SIC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SIC_Milestone.SIC_resolve_datetime));
                                }
                                else
                                {
                                    if (SIC_Milestone.SIC_Resolve_by > 0)
                                        milestone_det.resolve_data = SIC_Milestone.SIC_comment + "\n" + SIC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SIC_Milestone.SIC_resolve_datetime));
                                }
                                if (SIC_Milestone.SIC_Query_by > 0 && SIC_Milestone.SIC_Resolve_by == null)
                                    milestone_det.milestone_status = "Query";
                                else if (SIC_Milestone.SIC_Query_by > 0 && SIC_Milestone.SIC_Resolve_by > 0)
                                    milestone_det.milestone_status = "Resolve";
                                else
                                {
                                    milestone_det.milestone_status = "Completed";
                                }
                                var milestone_image = SIC_Milestone.tbl_SIC_Milestone_Photos.ToList();
                                List<string> milestone_img = new List<string>();
                                foreach (var image in milestone_image)
                                {
                                    milestone_img.Add(image.SIC_image_path);
                                }
                                milestone_det.images = milestone_img;
                            }
                        }
                        milestone_details.Add(milestone_det);


                    }
                    break;
                case "SEC":
                    var SECSub_doc_type = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaClearance_Subjob_id == Subjobid).Select(i => i.cust_doc_type).FirstOrDefault();
                    var shiptype1 = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaClearance_Subjob_id == Subjobid).Select(i => i.SeaClearance_shiptype).FirstOrDefault();
                    var SECmilestonelist1 = _context.tbl_milestone_master.Where(a => a.a_no == 29).OrderBy(x => x.milestone_order).ToList();

                    if (SECSub_doc_type == "RMS")
                    {
                        milestonelist = SECmilestonelist1.Where(x => x.milestone_catg == "RMS").ToList();
                        SECmilestonelist1 = SECmilestonelist1.Where(x => x.milestone_catg == "RMS").ToList();
                    }


                    if (shiptype1 != "FCL")
                    {
                        milestonelist = SECmilestonelist1.Where(x => x.milestone_stuffing_type == "lcl" || x.milestone_stuffing_type == "all").ToList();
                        SECmilestonelist1 = SECmilestonelist1.Where(x => x.milestone_stuffing_type == "lcl" || x.milestone_stuffing_type == "all").ToList();
                    }
                    else
                    {
                        var stufftype = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaClearance_Subjob_id == Subjobid).Select(i => i.sec_stuffing_type).FirstOrDefault();
                        if (stufftype == "Dock Stuffing")
                        {
                            milestonelist = SECmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl dock" || x.milestone_stuffing_type == "all").ToList();
                            SECmilestonelist1 = SECmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl dock" || x.milestone_stuffing_type == "all").ToList();
                        }
                        else
                        {
                            milestonelist = SECmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl factory" || x.milestone_stuffing_type == "all").ToList();
                            SECmilestonelist1 = SECmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl factory" || x.milestone_stuffing_type == "all").ToList();
                        }
                    }

                    foreach (var milestone in SECmilestonelist1)
                    {
                        var milestone_id = milestone.m_no;
                        var SEC_Milestone = _context.tbl_SEC_Milestone.Where(x => x.SEC_Subjob_id == Subjobid && x.SEC_milestone_id == milestone_id).FirstOrDefault();
                        milestone milestone_det = new milestone();
                        milestone_det.milestonename = milestone.milestone_name;
                        if (SEC_Milestone == null)
                        {
                            milestone_det.query_data = "";
                            milestone_det.resolve_data = "";
                            milestone_det.milestone_status = "New";
                            milestone_det.images = null;
                        }
                        else
                        {
                            if (SEC_Milestone.SEC_remark_datetime == null && SEC_Milestone.SEC_resolve_datetime == null)
                            {
                                milestone_det.query_data = "";
                                milestone_det.resolve_data = "";
                                milestone_det.milestone_status = "New";
                                milestone_det.images = null;
                            }
                            else
                            {
                                if (SEC_Milestone.SEC_Query_by > 0)
                                {
                                    milestone_det.query_data = SEC_Milestone.tbl_query_remarks.remark_text + "\n" + SEC_Milestone.tbl_user_master_App.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SEC_Milestone.SEC_remark_datetime));
                                    if (SEC_Milestone.SEC_Resolve_by > 0)
                                        milestone_det.resolve_data = SEC_Milestone.SEC_comment + "\n" + SEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SEC_Milestone.SEC_resolve_datetime));
                                }
                                else
                                {
                                    if (SEC_Milestone.SEC_Resolve_by > 0)
                                        milestone_det.resolve_data = SEC_Milestone.SEC_comment + "\n" + SEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SEC_Milestone.SEC_resolve_datetime));
                                }
                                if (SEC_Milestone.SEC_Query_by > 0 && SEC_Milestone.SEC_Resolve_by == null)
                                    milestone_det.milestone_status = "Query";
                                else if (SEC_Milestone.SEC_Query_by > 0 && SEC_Milestone.SEC_Resolve_by > 0)
                                    milestone_det.milestone_status = "Resolve";
                                else
                                {
                                    milestone_det.milestone_status = "Completed";
                                }
                                var milestone_image = SEC_Milestone.tbl_SEC_Milestone_Photos.ToList();
                                List<string> milestone_img = new List<string>();
                                foreach (var image in milestone_image)
                                {
                                    milestone_img.Add(image.SEC_image_path);
                                }
                                milestone_det.images = milestone_img;
                            }
                        }
                        milestone_details.Add(milestone_det);

                    }
                    break;
                case "SEF":
                    var SEFSub_doc_type = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaExport_Subjob_id == Subjobid).Select(i => i.cust_doc_type).FirstOrDefault();
                    var shiptype = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaExport_Subjob_id == Subjobid).Select(i => i.SeaClearance_shiptype).FirstOrDefault();
                    var SEEclrjobid = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaExport_Subjob_id == Subjobid).Select(i => i.SeaClearance_Subjob_id).FirstOrDefault();
                    var SEFmilestonelist1 = _context.tbl_milestone_master.Where(a => a.a_no == 29).OrderBy(x => x.milestone_order).ToList();
                    if (SEFSub_doc_type == "RMS")
                    {
                        milestonelist = SEFmilestonelist1.Where(x => x.milestone_catg == "RMS").ToList();
                        SEFmilestonelist1 = SEFmilestonelist1.Where(x => x.milestone_catg == "RMS").ToList();
                    }


                    if (shiptype != "FCL")
                    {
                        milestonelist = SEFmilestonelist1.Where(x => x.milestone_stuffing_type == "lcl" || x.milestone_stuffing_type == "all").ToList();
                        SEFmilestonelist1 = SEFmilestonelist1.Where(x => x.milestone_stuffing_type == "lcl" || x.milestone_stuffing_type == "all").ToList();
                    }
                    else
                    {
                        var stufftype = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaExport_Subjob_id == Subjobid).Select(i => i.sec_stuffing_type).FirstOrDefault();
                        if (stufftype == "Dock Stuffing")
                        {
                            milestonelist = SEFmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl dock" || x.milestone_stuffing_type == "all").ToList();
                            SEFmilestonelist1 = SEFmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl dock" || x.milestone_stuffing_type == "all").ToList();
                        }
                        else
                        {
                            milestonelist = SEFmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl factory" || x.milestone_stuffing_type == "all").ToList();
                            SEFmilestonelist1 = SEFmilestonelist1.Where(x => x.milestone_stuffing_type == "fcl factory" || x.milestone_stuffing_type == "all").ToList();
                        }
                    }

                    foreach (var milestone in SEFmilestonelist1)
                    {
                        var milestone_id = milestone.m_no;
                        var SEC_Milestone = _context.tbl_SEC_Milestone.Where(x => x.SEC_Subjob_id == SEEclrjobid && x.SEC_milestone_id == milestone_id).FirstOrDefault();
                        milestone milestone_det = new milestone();
                        milestone_det.milestonename = milestone.milestone_name;
                        if (SEC_Milestone == null)
                        {
                            milestone_det.query_data = "";
                            milestone_det.resolve_data = "";
                            milestone_det.milestone_status = "New";
                            milestone_det.images = null;
                        }
                        else
                        {
                            if (SEC_Milestone.SEC_remark_datetime == null && SEC_Milestone.SEC_resolve_datetime == null)
                            {
                                milestone_det.query_data = "";
                                milestone_det.resolve_data = "";
                                milestone_det.milestone_status = "New";
                                milestone_det.images = null;
                            }
                            else
                            {
                                if (SEC_Milestone.SEC_Query_by > 0)
                                {
                                    milestone_det.query_data = SEC_Milestone.tbl_query_remarks.remark_text + "\n" + SEC_Milestone.tbl_user_master_App.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SEC_Milestone.SEC_remark_datetime));
                                    if (SEC_Milestone.SEC_Resolve_by > 0)
                                        milestone_det.resolve_data = SEC_Milestone.SEC_comment + "\n" + SEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SEC_Milestone.SEC_resolve_datetime));
                                }
                                else
                                {
                                    if (SEC_Milestone.SEC_Resolve_by > 0)
                                        milestone_det.resolve_data = SEC_Milestone.SEC_comment + "\n" + SEC_Milestone.tbl_user_master_App1.User_Name + " " + (string.Format("{0:dd-MM-yyyy hh:mm tt}", SEC_Milestone.SEC_resolve_datetime));
                                }
                                if (SEC_Milestone.SEC_Query_by > 0 && SEC_Milestone.SEC_Resolve_by == null)
                                    milestone_det.milestone_status = "Query";
                                else if (SEC_Milestone.SEC_Query_by > 0 && SEC_Milestone.SEC_Resolve_by > 0)
                                    milestone_det.milestone_status = "Resolve";
                                else
                                {
                                    milestone_det.milestone_status = "Completed";
                                }
                                var milestone_image = SEC_Milestone.tbl_SEC_Milestone_Photos.ToList();
                                List<string> milestone_img = new List<string>();
                                foreach (var image in milestone_image)
                                {
                                    milestone_img.Add(image.SEC_image_path);
                                }
                                milestone_det.images = milestone_img;
                            }
                        }
                        milestone_details.Add(milestone_det);

                    }
                    break;
            }
            return Json(milestone_details, JsonRequestBehavior.AllowGet);


        }

        [SessionTimeOut]
        public async System.Threading.Tasks.Task<ActionResult> ShowDocAws(string param, string param2)
        {
            var validdocurl = "";
            var url = "https://s3.ap-south-1.amazonaws.com/ryalfiles/" + param;
            using (HttpClient client = new HttpClient())
            {
                //Do only Head request to avoid download full file
                var response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    validdocurl = url;
                }
                else
                {
                    //Do only Head request to avoid download full file
                    url = "https://s3.ap-south-1.amazonaws.com/ryalfiles/" + param2;
                    response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        validdocurl = url;
                    }
                }
            }
            ViewBag.docurl = validdocurl;
            return View();
        }

        /// <summary>
        /// set financial year
        /// </summary>
        /// <param name="Search_By"></param>
        /// <param name="Seach_parameter"></param>
        /// <param name="Stauts"></param>
        /// <returns></returns>
        /// 
        [SessionTimeOut]
        [HttpPost]
        public ActionResult SetValue(string Search_By, string Search_Parameter, string Stauts, string From_Dt, string To_Dt)
        {
            Session["Search_By"] = Search_By;
            Session["Stauts"] = Stauts;
            Session["Seach_parameter"] = Search_Parameter;
            Session["From_Dtt"] = From_Dt;
            Session["To_Dtt"] = To_Dt;
            return RedirectToAction("Dashboard");
        }

        [SessionTimeOut]
        [HttpPost]
        public JsonResult SetValue1(string Search_By, string Seach_parameter, string Stauts, string From_Dtt, string To_Dtt)
        {
            Session["Search_By"] = Search_By;
            Session["Stauts"] = Stauts;
            Session["Seach_parameter"] = Seach_parameter;
            Session["From_Dtt"] = From_Dtt;
            Session["To_Dtt"] = To_Dtt;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// LogOut
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOut()
        {
            Session.Remove("user_login_id");
            return RedirectToAction("Login");
        }

        [HttpPost]
        public JsonResult ForgotPassword(string password, string userid, string emailid)
        {
            var returnmsg = "";
            try
            {
                AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
                var newpassword = encryptDecrypt.EncryptString(password);
                var UserInDb = _context.tbl_crm_user_master.SingleOrDefault(x => x.crm_user_id == userid && x.tbl_address_book.add_contact_Email == emailid);
                if (UserInDb == null)
                {
                    returnmsg = "Please Varify Your User and Email ID.";
                }
                else
                {
                    UserInDb.crm_user_password = newpassword;
                    _context.Entry(UserInDb).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    returnmsg = "Password Generated Successfully";
                }

            }
            catch (Exception)
            {
                returnmsg = "Password Not Getting Generated";
            }

            return Json(returnmsg, JsonRequestBehavior.AllowGet);
        }


        //aad by ak 18-04-2022
        [SessionTimeOut]
        [HttpPost]
        public JsonResult InvoiceShipment(string sub_id, string deptno)
        {
            var dept = deptno;
            if (dept == "AEF") deptno = "AEC";
            if (dept == "SEF") deptno = "SEC";
            jobdetails job_details = new jobdetails();
            switch (deptno)
            {
                case "AEF":

                    break;
                case "AEC":

                    int id = Convert.ToInt32(sub_id);

                    var AECSubJobInDB = _context.Tbl_AirClearance_subjob.SingleOrDefault(x => x.AirClearance_Subjob_id == id);
                    if (dept == "AEF")
                    {
                        AECSubJobInDB = _context.Tbl_AirClearance_subjob.SingleOrDefault(x => x.AirExport_Subjob_id == id);
                    }
                    var CompanyInDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == AECSubJobInDB.AirClearance_Shipper_company).FirstOrDefault();
                    job_details.compname = CompanyInDB.unit_name;
                    job_details.SAddr = CompanyInDB.unit_address;
                    job_details.gstin = CompanyInDB.unit_gst_no;
                    job_details.mawb_mbl = "NA";
                    job_details.hawb_hbl = "NA";
                    job_details.Packaging_type = AECSubJobInDB.AirClearance_Packaging_type;
                    job_details.Subjob_no = AECSubJobInDB.AirClearance_Subjob_no;
                    job_details.cust_subdoc_type = AECSubJobInDB.cust_subdoc_type;
                    job_details.cust_doc_type = AECSubJobInDB.cust_doc_type;
                    job_details.Created_on_datetime = string.Format("{0:dd-MM-yyyy }", AECSubJobInDB.AirClearance_Created_on_datetime);
                    job_details.Royal_Impex_Jobno = AECSubJobInDB.Royal_Impex_Jobno;
                    job_details.shipping_bill_no = AECSubJobInDB.shipping_bill_no;

                    job_details.Total_weight = AECSubJobInDB.AirClearance_Total_weight.ToString();
                    job_details.Total_packages = AECSubJobInDB.AirClearance_Total_packages.ToString();
                    job_details.shipping_bill_date = string.Format("{0:dd-MM-yyyy }", AECSubJobInDB.shipping_bill_date);

                    if (AECSubJobInDB.AirExport_Subjob_id > 0)
                    {
                        var aefmasterjobid = AECSubJobInDB.AirExport_Masterjob_id;
                        var aefsubjobid = AECSubJobInDB.AirExport_Subjob_id;
                        var MawbInDb = _context.tbl_mawb_details.Where(x => x.MAWB_master_job_id == aefmasterjobid).FirstOrDefault();
                        if (MawbInDb != null)
                        {
                            if (MawbInDb.MAWB_stock_no != null)
                            {
                                string mawb_stock = MawbInDb.MAWB_stock_no.ToString();
                                if (mawb_stock.Length == 7)
                                {
                                    mawb_stock = "0" + mawb_stock;
                                }
                                if (mawb_stock.Length == 8)
                                {

                                    var first_mawb_stock_no = mawb_stock.Substring(0, 4);
                                    var second_mawb_stock_no = mawb_stock.Substring(4, 4);
                                    var total_mawb_stock_no = first_mawb_stock_no + " " + second_mawb_stock_no;

                                    var unitno = MawbInDb.MAWB_AirLine_code;
                                    var airlinno = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == unitno).Select(x => x.company_no).FirstOrDefault();
                                    var mawin = _context.tbl_company_master.FirstOrDefault(x => x.company_no == airlinno).Prefix;
                                    string res = mawin + "-" + total_mawb_stock_no;
                                    job_details.mawb_mbl = res;
                                }
                            }
                        }
                        var HawbInDb = _context.tbl_hawb_details.Where(x => x.Hawb_sub_job_id == aefsubjobid).FirstOrDefault();
                        if (HawbInDb != null)
                        {
                            job_details.hawb_hbl = HawbInDb.HAWB_stock_no;
                        }
                    }

                    var checklist = AECSubJobInDB.AECRoyalXML;


                    if (checklist == "previous year")
                    {
                        checklist = "";
                    }
                    if (!string.IsNullOrEmpty(checklist))
                    {
                        checklist = checklist.Replace("soap:", "");
                        XNamespace ns = "http://tempuri.org/";
                        var xDoc = XDocument.Parse(checklist);
                        var finalchecklist = xDoc.Descendants(ns + "Ryal_Imp_Data").FirstOrDefault().Value;
                        finalchecklist = finalchecklist.Replace("&", "AND");
                        try
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(finalchecklist);
                            XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                            foreach (XmlNode fsu in fsunodes)
                            {
                                try
                                {
                                    job_details.iecno = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["IEC_NO"].InnerText;
                                    job_details.sbtype = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["SB_TYPE"].InnerText;
                                    job_details.SAddr = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS2"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS3"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS4"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS5"].InnerText;
                                    job_details.adcode = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADCODE"].InnerText;
                                    job_details.cname = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_NAME"].InnerText + "\n";
                                    job_details.CAddr = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD2"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD3"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD4"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD5"].InnerText;
                                    job_details.cod = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["COUNTRY_DISCHARGE"].InnerText;
                                    job_details.pod = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["PORT_DISCHARGE"].InnerText;
                                    job_details.cfd = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["COUNTRY_FINAL_DEST"].InnerText;
                                    job_details.fpod = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["PORT_FINAL_DEST"].InnerText;
                                    job_details.cuser = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["USERID"].InnerText;
                                    job_details.invno = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_NO"].InnerText;
                                    job_details.invdt = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_DATE"].InnerText;
                                    job_details.invvalfcr = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_VALUE"].InnerText;
                                    job_details.invcur = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_CURRENCY"].InnerText;
                                    job_details.invexrate = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["EXCHANGE_RATE"].InnerText;
                                    job_details.invinco = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["TERMS"].InnerText;
                                    //var fobvalue1 = ().ToString();
                                    var FOBVAL = Convert.ToDecimal(string.Format("{0:0.00}", (Convert.ToDecimal(job_details.invvalfcr) * Convert.ToDecimal(job_details.invexrate))));
                                    job_details.fobvalue = FOBVAL.ToString();

                                    try
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"].NextSibling["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    try
                                    {
                                        job_details.dbkval = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["DRAWBACK_VALUE"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.dbkval = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"].NextSibling["DRAWBACK_VALUE"].InnerText;
                                    }
                                    try
                                    {
                                        job_details.gstin = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["GSTN_ID"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.gstin = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"].NextSibling["GSTN_ID"].InnerText;
                                    }
                                    if (string.IsNullOrEmpty(job_details.gstin))
                                    {
                                        job_details.gstin = CompanyInDB.unit_gst_no;
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        catch (Exception)
                        {
                        }

                    }
                    break;
                case "SEF":

                    break;


                case "SEC":
                    int SECid = Convert.ToInt32(sub_id);

                    var SECSubJobInDB = _context.Tbl_SeaClearance_subjob.SingleOrDefault(x => x.SeaClearance_Subjob_id == SECid);
                    if (dept == "SEF")
                    {
                        SECSubJobInDB = _context.Tbl_SeaClearance_subjob.SingleOrDefault(x => x.SeaExport_Subjob_id == SECid);
                    }
                    var SECCompanyInDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == SECSubJobInDB.SeaClearance_Shipper_company).FirstOrDefault();
                    job_details.compname = SECCompanyInDB.unit_name;
                    job_details.SAddr = SECCompanyInDB.unit_address;
                    job_details.gstin = SECCompanyInDB.unit_gst_no;
                    job_details.mawb_mbl = "NA";
                    job_details.hawb_hbl = "NA";
                    job_details.Packaging_type = SECSubJobInDB.SeaClearance_Packaging_type;
                    job_details.Subjob_no = SECSubJobInDB.SeaClearance_Subjob_no;
                    job_details.cust_subdoc_type = SECSubJobInDB.cust_subdoc_type;
                    job_details.cust_doc_type = SECSubJobInDB.cust_doc_type;
                    job_details.Created_on_datetime = string.Format("{0:dd-MM-yyyy }", SECSubJobInDB.SeaClearance_Created_on_datetime);
                    job_details.Royal_Impex_Jobno = SECSubJobInDB.Royal_Impex_Jobno;
                    job_details.shipping_bill_no = SECSubJobInDB.shipping_bill_no;

                    job_details.SINV_NO = SECSubJobInDB.SEC_SINV_NO;
                    job_details.Total_weight = SECSubJobInDB.SeaClearance_Total_weight.ToString();
                    job_details.Total_packages = SECSubJobInDB.SeaClearance_Total_packages.ToString();
                    job_details.CFS_NO = SECSubJobInDB.SEC_CFS;
                    job_details.Cargo_Forwording = SECSubJobInDB.SEC_Forworder;
                    job_details.shipping_bill_date = string.Format("{0:dd-MM-yyyy }", SECSubJobInDB.shipping_bill_date);

                    if (SECSubJobInDB.SeaExport_Subjob_id > 0)
                    {
                        var aefmasterjobid = SECSubJobInDB.SeaExport_Masterjob_id;
                        var aefsubjobid = SECSubJobInDB.SeaExport_Subjob_id;
                        var MawbInDb = _context.tbl_mawb_details.Where(x => x.MAWB_master_job_id == aefmasterjobid).FirstOrDefault();
                        if (MawbInDb != null)
                        {
                            if (MawbInDb.MAWB_stock_no != null)
                            {
                                string mawb_stock = MawbInDb.MAWB_stock_no.ToString();
                                if (mawb_stock.Length == 7)
                                {
                                    mawb_stock = "0" + mawb_stock;
                                }
                                if (mawb_stock.Length == 8)
                                {

                                    var first_mawb_stock_no = mawb_stock.Substring(0, 4);
                                    var second_mawb_stock_no = mawb_stock.Substring(4, 4);
                                    var total_mawb_stock_no = first_mawb_stock_no + " " + second_mawb_stock_no;

                                    var unitno = MawbInDb.MAWB_AirLine_code;
                                    var airlinno = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == unitno).Select(x => x.company_no).FirstOrDefault();
                                    var mawin = _context.tbl_company_master.FirstOrDefault(x => x.company_no == airlinno).Prefix;
                                    string res = mawin + "-" + total_mawb_stock_no;
                                    job_details.mawb_mbl = res;
                                }
                            }
                        }
                        var HawbInDb = _context.tbl_hawb_details.Where(x => x.Hawb_sub_job_id == aefsubjobid).FirstOrDefault();
                        if (HawbInDb != null)
                        {
                            job_details.hawb_hbl = HawbInDb.HAWB_stock_no;
                        }
                    }

                    var checklist1 = SECSubJobInDB.SECRoyalXML;


                    if (checklist1 == "previous year")
                    {
                        checklist1 = "";
                    }
                    if (!string.IsNullOrEmpty(checklist1))
                    {
                        checklist1 = checklist1.Replace("soap:", "");
                        XNamespace ns = "http://tempuri.org/";
                        var xDoc = XDocument.Parse(checklist1);
                        var finalchecklist = xDoc.Descendants(ns + "Ryal_Imp_Data").FirstOrDefault().Value;
                        finalchecklist = finalchecklist.Replace("&", "AND");
                        try
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(finalchecklist);
                            XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                            foreach (XmlNode fsu in fsunodes)
                            {
                                try
                                {
                                    job_details.iecno = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["IEC_NO"].InnerText;
                                    job_details.sbtype = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["SB_TYPE"].InnerText;
                                    job_details.SAddr = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS2"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS3"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS4"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADDRESS5"].InnerText;
                                    job_details.adcode = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["ADCODE"].InnerText;
                                    job_details.cname = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_NAME"].InnerText;
                                    job_details.CAddr = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD2"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD3"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD4"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["CONSIGNEE_ADD5"].InnerText;
                                    job_details.cod = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["COUNTRY_DISCHARGE"].InnerText;
                                    job_details.pod = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["PORT_DISCHARGE"].InnerText;
                                    job_details.cfd = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["COUNTRY_FINAL_DEST"].InnerText;
                                    job_details.fpod = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["PORT_FINAL_DEST"].InnerText;
                                    job_details.cuser = fsu["Ryal_Imp_Data"]["EXPORT_GENERAL_DETAILS"]["USERID"].InnerText;
                                    job_details.invno = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_NO"].InnerText;
                                    job_details.invdt = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_DATE"].InnerText;
                                    job_details.invvalfcr = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_VALUE"].InnerText;
                                    job_details.invcur = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["INVOICE_CURRENCY"].InnerText;
                                    job_details.invexrate = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["EXCHANGE_RATE"].InnerText;
                                    job_details.invinco = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["TERMS"].InnerText;
                                    //var fobvalue1 = ().ToString();
                                    var FOBVAL = Convert.ToDecimal(string.Format("{0:0.00}", (Convert.ToDecimal(job_details.invvalfcr) * Convert.ToDecimal(job_details.invexrate))));
                                    job_details.fobvalue = FOBVAL.ToString();

                                    try
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"].NextSibling["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    try
                                    {
                                        job_details.dbkval = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["DRAWBACK_VALUE"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.dbkval = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"].NextSibling["DRAWBACK_VALUE"].InnerText;
                                    }
                                    try
                                    {
                                        job_details.gstin = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"]["GSTN_ID"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.gstin = fsu["Ryal_Imp_Data"]["EXPORT_INVOICE_DETAILS"].NextSibling["GSTN_ID"].InnerText;
                                    }
                                    if (string.IsNullOrEmpty(job_details.gstin))
                                    {
                                        job_details.gstin = SECCompanyInDB.unit_gst_no;
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        catch (Exception)
                        {
                        }

                    }
                    break;

                case "SIC":
                    int SICId = Convert.ToInt32(sub_id);

                    var SICSubJobInDB = _context.Tbl_SeaImpClearance_subjob.SingleOrDefault(x => x.SeaImpClearance_Subjob_id == SICId);
                    var SICCompanyInDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == SICSubJobInDB.SeaImpClearance_Shipper_company).FirstOrDefault();
                    job_details.compname = SICCompanyInDB.unit_name;
                    job_details.SAddr = SICCompanyInDB.unit_address;
                    job_details.gstin = SICCompanyInDB.unit_gst_no;
                    //job_details.mawb_mbl = SICSubJobInDB.SeaImpClearance_hbl;
                    //job_details.hawb_hbl = SICSubJobInDB.SeaImpClearance_mbl;
                    job_details.invdt = string.Format("{0:dd-MM-yyyy }", SICSubJobInDB.SIC_SINV_DATE);
                    job_details.Packaging_type = SICSubJobInDB.SeaImpClearance_Packaging_type;
                    job_details.Subjob_no = SICSubJobInDB.SeaImpClearance_Subjob_no;
                    job_details.cust_subdoc_type = SICSubJobInDB.cust_subdoc_type;
                    job_details.cust_doc_type = SICSubJobInDB.cust_doc_type;
                    job_details.igmno = SICSubJobInDB.SeaImpClearance_igmno;
                    job_details.Created_on_datetime = string.Format("{0:dd-MM-yyyy }", SICSubJobInDB.SeaImpClearance_Created_on_datetime);
                    job_details.Royal_Impex_Jobno = SICSubJobInDB.Royal_Impex_Jobno;
                    job_details.shipping_bill_no = SICSubJobInDB.billof_entry_no;

                    job_details.SINV_NO = SICSubJobInDB.SIC_SINV_NO;
                    job_details.CFS_NO = SICSubJobInDB.SIC_CFS;
                    job_details.invinco = SICSubJobInDB.SIC_Incoterm;
                    job_details.Cargo_Forwording = SICSubJobInDB.SIC_Forworder;
                    job_details.shipping_bill_date = string.Format("{0:dd-MM-yyyy }", SICSubJobInDB.billof_entry_date);


                    var checklist3 = SICSubJobInDB.SICRoyalXML;


                    if (checklist3 == "previous year")
                    {
                        checklist3 = "";
                    }
                    if (!string.IsNullOrEmpty(checklist3))
                    {
                        checklist1 = checklist3.Replace("soap:", "");
                        XNamespace ns = "http://tempuri.org/";
                        var xDoc = XDocument.Parse(checklist1);
                        var finalchecklist = xDoc.Descendants(ns + "Ryal_Imp_Data").FirstOrDefault().Value;
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>3</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>3</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>4</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>4</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>5</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>5</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("&", "AND");
                        try
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(finalchecklist);
                            XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                            foreach (XmlNode fsu in fsunodes)
                            {
                                try
                                {
                                    job_details.iecno = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["IEC_NO"].InnerText;
                                    job_details.betype = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["BE_TYPE"].InnerText;
                                    job_details.CAddr = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS2"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS3"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS4"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS5"].InnerText;
                                    job_details.cname = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_NAME"].InnerText;
                                    job_details.SAddr = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_ADDRESS"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_ADDRESS1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_ADDRESS2"].InnerText;
                                    job_details.coo = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["COUNTRY_ORIGIN"].InnerText;
                                    job_details.pol = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["PORT_ORIGIN"].InnerText;
                                    job_details.coc = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["COUNTRY_SHIPMENT"].InnerText;
                                    job_details.mawb_mbl = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_MAWB_MBL"].InnerText;
                                    job_details.hawb_hbl = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_HAWB_HBLNO"].InnerText;
                                    job_details.Total_packages = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_NOOFPACKAGE"].InnerText;
                                    job_details.Total_weight = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_GROSSWEIGHT"].InnerText;
                                    job_details.invno = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["INVOICE_NO"].InnerText;
                                    job_details.invdt = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_DATE"].InnerText;
                                    job_details.invvalfcr = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_VALUE"].InnerText;
                                    job_details.invcur = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_CURRENCY"].InnerText;
                                    job_details.invexrate = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_EXRATE"].InnerText;
                                    job_details.invinco = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["TERMS"].InnerText;
                                    job_details.cduty = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["DUTY_VALUE"].InnerText;
                                    XmlNodeList elemList = xmldoc.GetElementsByTagName("ASSESSABLE_VALUE");
                                    for (int i = 0; i < elemList.Count; i++)
                                    {
                                        job_details.avalue = (Convert.ToDecimal(job_details.avalue) + Convert.ToDecimal(elemList[i].InnerXml)).ToString();
                                    }

                                    try
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"].NextSibling["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    if (string.IsNullOrEmpty(job_details.gstin))
                                    {
                                        job_details.gstin = ViewBag.gstin;
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        catch (Exception)
                        {
                        }

                    }
                    break;
                case "AIC":
                    int AICId = Convert.ToInt32(sub_id);

                    var AICSubJobInDB = _context.Tbl_AirImpClearance_subjob.SingleOrDefault(x => x.AirImpClearance_Subjob_id == AICId);
                    var AICCompanyInDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == AICSubJobInDB.AirImpClearance_Shipper_company).FirstOrDefault();
                    job_details.compname = AICCompanyInDB.unit_name;
                    job_details.SAddr = AICCompanyInDB.unit_address;
                    job_details.gstin = AICCompanyInDB.unit_gst_no;

                    job_details.ImpClearance_igmno = AICSubJobInDB.AirImpClearance_igmno;

                    job_details.invdt = string.Format("{0:dd-MM-yyyy }", AICSubJobInDB.AIC_SINV_DATE);
                    job_details.Packaging_type = AICSubJobInDB.AirImpClearance_Packaging_type;
                    job_details.Subjob_no = AICSubJobInDB.AirImpClearance_Subjob_no;
                    job_details.cust_subdoc_type = AICSubJobInDB.cust_subdoc_type;
                    job_details.cust_doc_type = AICSubJobInDB.cust_doc_type;
                    job_details.CFS_NO = AICSubJobInDB.AirImpClearance_igmno;
                    job_details.igmno_date = string.Format("{0:dd-MM-yyyy }", AICSubJobInDB.AirImpClearance_igmdate);
                    job_details.Created_on_datetime = string.Format("{0:dd-MM-yyyy }", AICSubJobInDB.AirImpClearance_Created_on_datetime);

                    job_details.Royal_Impex_Jobno = AICSubJobInDB.Royal_Impex_Jobno;
                    job_details.shipping_bill_no = AICSubJobInDB.billof_entry_no;

                    job_details.SINV_NO = AICSubJobInDB.AIC_SINV_NO;
                    job_details.invinco = AICSubJobInDB.AIC_Incoterm;
                    job_details.shipping_bill_date = string.Format("{0:dd-MM-yyyy }", AICSubJobInDB.billof_entry_date);


                    var checklist4 = AICSubJobInDB.AICRoyalXML;


                    if (checklist4 == "previous year")
                    {
                        checklist3 = "";
                    }
                    if (!string.IsNullOrEmpty(checklist4))
                    {
                        checklist1 = checklist4.Replace("soap:", "");
                        XNamespace ns = "http://tempuri.org/";
                        var xDoc = XDocument.Parse(checklist1);
                        var finalchecklist = xDoc.Descendants(ns + "Ryal_Imp_Data").FirstOrDefault().Value;
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>1</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>2</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>3</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>3</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>4</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>4</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("<INVOICE_SLNO>5</INVOICE_SLNO><INVOICE_SLNO>", "<INVOICE_SLNO>5</INVOICE_SLNO><INVOICE_NO>");
                        finalchecklist = finalchecklist.Replace("&", "AND");
                        try
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(finalchecklist);
                            XmlNodeList fsunodes = xmldoc.SelectNodes("Envelope/Body");
                            foreach (XmlNode fsu in fsunodes)
                            {
                                try
                                {
                                    job_details.iecno = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["IEC_NO"].InnerText;
                                    job_details.betype = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["BE_TYPE"].InnerText;
                                    job_details.CAddr = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS2"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS3"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS4"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["ADDRESS5"].InnerText;
                                    job_details.cname = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_NAME"].InnerText;
                                    job_details.SAddr = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_ADDRESS"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_ADDRESS1"].InnerText + "\n" + fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["SUPPLIER_ADDRESS2"].InnerText;
                                    job_details.coo = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["COUNTRY_ORIGIN"].InnerText;
                                    job_details.pol = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["PORT_ORIGIN"].InnerText;
                                    job_details.coc = fsu["Ryal_Imp_Data"]["GENERAL_DETAILS"]["COUNTRY_SHIPMENT"].InnerText;
                                    job_details.mawb_mbl = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_MAWB_MBL"].InnerText;
                                    job_details.hawb_hbl = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_HAWB_HBLNO"].InnerText;
                                    job_details.Total_packages = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_NOOFPACKAGE"].InnerText;
                                    job_details.Total_weight = fsu["Ryal_Imp_Data"]["IGM_DETAILS"]["IGM_GROSSWEIGHT"].InnerText;
                                    job_details.invno = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["INVOICE_NO"].InnerText;
                                    // job_details.invdt = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_DATE"].InnerText;
                                    job_details.invvalfcr = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_VALUE"].InnerText;
                                    job_details.invcur = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_CURRENCY"].InnerText;
                                    job_details.invexrate = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["INVOICE_EXRATE"].InnerText;
                                    job_details.invinco = fsu["Ryal_Imp_Data"]["INVOICE_DETAILS"]["TERMS"].InnerText;
                                    job_details.CFS_NO = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["DUTY_VALUE"].InnerText;
                                    XmlNodeList elemList = xmldoc.GetElementsByTagName("ASSESSABLE_VALUE");
                                    for (int i = 0; i < elemList.Count; i++)
                                    {
                                        job_details.avalue = (Convert.ToDecimal(job_details.avalue) + Convert.ToDecimal(elemList[i].InnerXml)).ToString();
                                    }

                                    try
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"]["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    catch (Exception)
                                    {
                                        job_details.descr = fsu["Ryal_Imp_Data"]["ITEM_PRODUCT_DETAILS"].NextSibling["ITEM_DESCRIPTION"].InnerText;
                                    }
                                    if (string.IsNullOrEmpty(job_details.gstin))
                                    {
                                        job_details.gstin = ViewBag.gstin;
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        catch (Exception)
                        {
                        }

                    }
                    break;
            }

            return Json(job_details, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CustomerPdf()
        {
            var bb = Session["Cost_details"];
            return View();
        }

        public void Dashboard_Excel()
        {
            char[] alphabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            int charcount = 0;
            var userno = Convert.ToInt32(Session["crm_user_no"].ToString());
            var Department = _context.tbl_crm_user_master.Where(x => x.crm_user_no == userno).FirstOrDefault();
            int row = 2;
            var Count = 0;
            var finyear = Session["financialyear"].ToString();
            List<Customer> detdata = Session["Cost_details"] as List<Customer>;
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            Sheet.Cells[alphabet[charcount] + "1"].Value = "SR";
            charcount++;
            Sheet.Cells[alphabet[charcount] + "1"].Value = "INVOICE";
            charcount++;
            if (Session["dept"].ToString() == "AE" || Session["dept"].ToString() == "SE")
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "CONSIGNEE";
                charcount++;
            }
            else
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "SHIPPER";
                charcount++;
            }
            Sheet.Cells[alphabet[charcount] + "1"].Value = "JOB NO / Date";
            charcount++;
            Sheet.Cells[alphabet[charcount] + "1"].Value = "MODE";
            charcount++;

            if (Session["dept"].ToString() == "AE" || Session["dept"].ToString() == "AI")
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "MAWB";
                charcount++;
                Sheet.Cells[alphabet[charcount] + "1"].Value = "HAWB";
                charcount++;
            }
            else if (Session["dept"].ToString() == "SI")
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "MAWB";
                charcount++;
                Sheet.Cells[alphabet[charcount] + "1"].Value = "HAWB";
                charcount++;
            }
            else
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "BL";
                charcount++;
            }
            if (Session["dept"].ToString() == "AE" || Session["dept"].ToString() == "SE")
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "SB NO";
                charcount++;
            }
            else
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "BOE NO";
                charcount++;
            }

            Sheet.Cells[alphabet[charcount] + "1"].Value = "TYPE";
            charcount++;
            Sheet.Cells[alphabet[charcount] + "1"].Value = "PKG / WT";
            charcount++;
            if (Session["dept"].ToString() == "SI" || Session["dept"].ToString() == "SE")
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "VOL";
                charcount++;
                Sheet.Cells[alphabet[charcount] + "1"].Value = "CONTAINER";
                charcount++;
            }
            Sheet.Cells[alphabet[charcount] + "1"].Value = "POL";
            charcount++;
            Sheet.Cells[alphabet[charcount] + "1"].Value = "POD";
            charcount++;
            Sheet.Cells[alphabet[charcount] + "1"].Value = "ETD";
            charcount++;
            Sheet.Cells[alphabet[charcount] + "1"].Value = "ETA";
            charcount++;
            if (Session["dept"].ToString() == "AE")
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "FLIGHT";
                charcount++;
            }
            else
            {
                Sheet.Cells[alphabet[charcount] + "1"].Value = "VESSEL";
                charcount++;
            }
            Sheet.Cells[alphabet[charcount] + "1"].Value = "STAT";

            Sheet.Cells["A1:" + alphabet[charcount] + "1"].Style.Font.Bold = true;
            Sheet.Cells["A1:" + alphabet[charcount] + "1"].Style.Fill.SetBackground(Color.Gray);

            Customer chargeData = new Customer();
            foreach (var item in detdata)
            {
                charcount = 0;
                chargeData = item;
                Count++;
                //1
                Sheet.Cells[string.Format("A{0}", row)].Value = Count;
                charcount++;
                List<Clr_Det> Clr_det = new List<Clr_Det>();
                Clr_det = item.Clr_det;
                if (Clr_det == null)
                {
                    Clr_det = new List<Clr_Det>();
                }
                var Sub_job_id = item.Sub_job_id;
                var lstSubJobNos1 = item;
                var HAWB = item.HAWB;
                //2
                if (Clr_det.Count > 0)
                {
                    var invno = "";
                    foreach (var clrjob in Clr_det)
                    {
                        invno += clrjob.Party_InvocieNo + ",";
                    }
                    invno = invno.TrimEnd(',');
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = invno;
                    charcount++;
                }
                else
                {
                    if (item.Party_InvocieNo != null && item.Party_InvocieNo != "")
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Party_InvocieNo;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                //3
                if (!string.IsNullOrEmpty(item.Shppr_Cnee))
                {
                    if (item.Shppr_Cnee.Length > 15)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Shppr_Cnee.Substring(0, 15);
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Shppr_Cnee;
                        charcount++;
                    }
                }
                else
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                    charcount++;
                }
                //4
                if (!string.IsNullOrEmpty(item.Sub_job_no))
                {
                    var clrjobno = "";
                    if (Clr_det.Count > 0)
                    {
                        foreach (var clrjob in Clr_det)
                        {
                            clrjobno += clrjob.Clr_job_no.Replace("/" + finyear, "") + ",";
                        }
                        clrjobno = clrjobno.TrimEnd(',');
                        clrjobno = clrjobno.Replace(",", "\n");
                    }
                    if (clrjobno != "")
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = string.Format("{0:dd/MM/yyyy}", item.Sub_job_date);
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Sub_job_no.Replace("/" + finyear, "") + string.Format("{0:dd/MM/yyyy}", item.Sub_job_date);
                        charcount++;
                    }
                }
                else
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Sub_job_no.Replace("/" + finyear, "") + string.Format("{0:dd/MM/yyyy}", item.Sub_job_date);
                    charcount++;
                }
                //5
                if (item.Mode != null && item.Mode != "")
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Mode;
                    charcount++;
                }
                else
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                    charcount++;
                }
                //6
                if (Session["dept"].ToString() == "AE")
                {
                    if (item.MAWB != "" && item.MAWB != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.MAWB;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                    if (HAWB != null && HAWB != "")
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.HAWB;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                else if (Session["dept"].ToString() != "SE")
                {
                    if (item.MAWB != "" && item.MAWB != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.MAWB;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                    if (HAWB != null && HAWB != "")
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = HAWB;
                        charcount++;

                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                else
                {
                    if (HAWB != null && HAWB != "")
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = HAWB;
                        charcount++;
                    }
                    else if (item.MAWB != "" && item.MAWB != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.MAWB;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                //7
                if (Clr_det.Count > 0)
                {
                    var sb_be_no = "";
                    foreach (var clrjob in Clr_det)
                    {
                        sb_be_no += clrjob.sb_be_no + ",";
                        //Sheet.Cells[string.Format(alphabet[charcount]+"{0}", row)].Value = clrjob.sb_be_no;
                        //charcount++;
                    }
                    sb_be_no = sb_be_no.TrimEnd(',');
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = sb_be_no;
                    charcount++;
                }
                else
                {
                    if (!string.IsNullOrEmpty(item.sb_be_no))
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.sb_be_no;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                //8
                if (Clr_det.Count > 0)
                {
                    var doctype = "";
                    foreach (var clrjob in Clr_det)
                    {
                        doctype += clrjob.Doctype + ",";
                        //if (!string.IsNullOrEmpty(clrjob.Doctype))
                        //{
                        //    Sheet.Cells[string.Format(alphabet[charcount]+"{0}", row)].Value = clrjob.Doctype;
                        //    charcount++;

                        //}
                        //else
                        //{
                        //    Sheet.Cells[string.Format(alphabet[charcount]+"{0}", row)].Value = "";
                        //    charcount++;
                        //}
                    }
                    doctype = doctype.TrimEnd(',');
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = doctype;
                    charcount++;
                }
                else
                {
                    if (!string.IsNullOrEmpty(item.Doctype))
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Doctype;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                //9
                if (Clr_det.Count > 0)
                {
                    var grwt = "";
                    foreach (var clrjob in Clr_det)
                    {
                        grwt += clrjob.PKG + "/" + clrjob.GRWT + ",";
                        //Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = clrjob.PKG + "/" + clrjob.GRWT;
                        //charcount++;
                    }
                    grwt = grwt.TrimEnd(',');
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = grwt;
                    charcount++;
                }
                else
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.PKG + "/" + item.GRWT;
                    charcount++;
                }
                //10
                if (Session["dept"].ToString() == "SI" || Session["dept"].ToString() == "SE")
                {
                    if (item.HAWB == "only clearance")
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "NA";
                        charcount++;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(item.vol))
                        {
                            Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                            charcount++;
                        }
                        else
                        {
                            Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.vol;
                            charcount++;
                        }
                    }
                    if (item.HAWB == "only clearance")
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "NA";
                        charcount++;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(item.Container_no))
                        {
                            Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                            charcount++;
                        }
                        else
                        {
                            Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Container_no;
                            charcount++;
                        }
                    }
                }
                //11
                if (Session["dept"].ToString() == "AE" || Session["dept"].ToString() == "SE")
                {
                    if (item.Branch != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.pol_code;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                else
                {
                    if (item.Destination != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Destination;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                if (Session["dept"].ToString() == "AE" || Session["dept"].ToString() == "SE")
                {
                    if (item.Destination != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Destination;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                else
                {
                    if (item.Branch != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = item.Branch;
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }

                if (item.HAWB == "only clearance")
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "NA";
                    charcount++;
                }
                else
                {
                    if (item.ETD != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = string.Format("{0:dd/MM/yyyy}", item.ETD);
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                if (item.HAWB == "only clearance")
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "NA";
                    charcount++;
                }
                else
                {
                    if (item.ETA != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = string.Format("{0:dd/MM/yyyy}", item.ETA);
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }

                if (item.HAWB == "only clearance")
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "NA";
                    charcount++;
                }
                else
                {
                    if (item.vessel_voyag != null)
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = string.Format("{0:dd/MM/yyyy}", item.vessel_voyag);
                        charcount++;
                    }
                    else
                    {
                        Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "";
                        charcount++;
                    }
                }
                if (item.STATUS == "1")
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "True";
                    charcount++;
                }
                else
                {
                    Sheet.Cells[string.Format(alphabet[charcount] + "{0}", row)].Value = "False";
                    charcount++;
                }

                row++;
            }
            //Sheet.Cells["A1:" + alphabet[charcount] +"1"].Style.Font.Bold = true;
            //Sheet.Cells["A" + row + ":" + alphabet[charcount] + row].Style.Fill.SetBackground(Color.Gray);



            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=Dashboard_Report.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }

        // Generate PDF File Changes by Rituraj
        [HttpPost, ValidateInput(false)]
        public ActionResult GeneratePdfFile_WithHeader(string htmlString, string formatted_job_no)
        {
            string finalHtmlString = @"<html><body>" + htmlString.ToUpper() + "" + "</body></html>";

            string response = "";

            try
            {
                string baseUrl = "";
                PdfPageSize pageSize = PdfPageSize.A4;
                PdfPageOrientation pdfOrientation = PdfPageOrientation.Landscape;
                int webPageWidth = 297;

                var headerstring = "<div style='border:1px solid !important;></div>";

                PdfHtmlSection htmlHeader = new PdfHtmlSection(headerstring, baseUrl);
                htmlHeader.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
                htmlHeader.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;



                // create a new pdf document converting an url
                PdfDocument doc = new PdfDocument();

                doc.DocumentInformation.Title = formatted_job_no;
                int webPageHeight = 700;

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();
                // set converter options
                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = webPageWidth;
                converter.Options.WebPageHeight = webPageHeight;
                converter.Options.MarginRight = 15;
                converter.Header.Height = 1;
                converter.Options.MarginTop = 15;
                converter.Options.MarginBottom = 5;

                converter.Options.DisplayHeader = true;
                converter.Options.DisplayFooter = true;
                converter.Options.DisplayCutText = true;
                //converter.Footer.Add(new PdfTextSection(775, 0, 50, 5, "Page: {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8)));
                converter.Header.Add(htmlHeader);
                //converter.Header.DisplayOnFirstPage = false;

                doc = converter.ConvertHtmlString(finalHtmlString, baseUrl);

                // save pdf document
                //doc.Save(Server.MapPath("~/Sample.pdf"));      

                #region 
                string path = AppDomain.CurrentDomain.BaseDirectory + "uploads\\PDF";

                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }
                string filename = Path.GetFileName(Server.MapPath("~/Sample.pdf"));
                string pth = Path.Combine(path, filename);
                doc.Save(pth);
                #endregion

                // Delete pdf document
                #region
                IAmazonS3 clientDel = new AmazonS3Client(RegionEndpoint.APSouth1);

                TransferUtility utilityDel = new TransferUtility(clientDel);
                TransferUtilityUploadRequest requestDel = new TransferUtilityUploadRequest();

                requestDel.BucketName = "ryalfiles";
                string unique_file_name_del = formatted_job_no + ".pdf";
                requestDel.Key = unique_file_name_del;

                clientDel.DeleteObject(requestDel.BucketName, requestDel.Key);
                clientDel.Dispose();
                #endregion


                // Save pdf document
                #region 
                AmazonUploader myUploader = new AmazonUploader();

                string unique_file_name = "";

                IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);

                TransferUtility utility = new TransferUtility(client);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

                request.BucketName = "ryalfiles";

                unique_file_name = formatted_job_no + ".pdf";

                request.Key = unique_file_name;

                byte[] files = System.IO.File.ReadAllBytes(pth);

                Stream stream = new MemoryStream(files);

                request.InputStream = stream;
                utility.Upload(request);

                response = "true|" + unique_file_name;

                // close pdf document
                doc.Close();
                #endregion
            }
            catch (Exception ex)
            {
                using (StreamWriter _testData = new StreamWriter(Server.MapPath("~/App_Data/errors/AirExportInvoice.txt"), true))
                {
                    _testData.WriteLine("Message: " + ex.Message + "<br/>" + Environment.NewLine + "StackTrace: " + ex.StackTrace + "<br/>" + Environment.NewLine + "DateTime: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt").Replace('-', '/') +
                        "<br/>" + Environment.NewLine + "<br/>" + Environment.NewLine); // Write the file.
                    _testData.Dispose();
                }
                response = "false|" + ex.Message;
                return Content("Invalid json");
            }

            string doc_name = "NA";

            if (response.Split('|')[0] == "true")
            {
                doc_name = response.Split('|')[1];
            }
            return Content("https://s3.ap-south-1.amazonaws.com/ryalfiles/" + doc_name);


        }

        public class jobdetails
        {
            public string Subjob_no { get; set; }
            public string Created_on_datetime { get; set; }
            public string Royal_Impex_Jobno { get; set; }
            public string shipping_bill_no { get; set; }
            public string shipping_bill_date { get; set; }
            public string gstin { get; set; }
            public string adcode { get; set; }
            public string iecno { get; set; }
            public string compname { get; set; }
            public string SAddr { get; set; }
            public string cname { get; set; }
            public string CAddr { get; set; }
            public string cod { get; set; }
            public string cfd { get; set; }
            public string fpod { get; set; }
            public string cuser { get; set; }
            public string invno { get; set; }
            public string invdt { get; set; }
            public string invvalfcr { get; set; }
            public string invcur { get; set; }
            public string invexrate { get; set; }
            public string invinco { get; set; }
            public string fobvalue { get; set; }
            public string descr { get; set; }
            public string SINV_NO { get; set; }
            public string CFS { get; set; }
            public string Packaging_type { get; set; }
            public string cust_subdoc_type { get; set; }
            public string cust_doc_type { get; set; }
            public string sbtype { get; set; }
            public string mawb_mbl { get; set; }
            public string hawb_hbl { get; set; }
            public string Total_packages { get; set; }
            public string Total_weight { get; set; }
            public string pod { get; set; }
            public string pol { get; set; }
            public string coo { get; set; }
            public string coc { get; set; }
            public string dbkval { get; set; }
            public string Cargo_Forwording { get; set; }
            public string CFS_NO { get; set; }
            public string avalue { get; set; }
            public string betype { get; set; }
            public string sname { get; set; }
            public string cduty { get; set; }
            public string igmno { get; set; }
            public string igmno_date { get; set; }
            public string ImpClearance_igmno { get; set; }

        }
        public class milestone
        {
            public string milestonename { get; set; }
            public string query_data { get; set; }
            public string resolve_data { get; set; }
            public string milestone_status { get; set; }
            public List<string> images { get; set; }
        }
        public class invoicedetails
        {
            public string AirExportInvoice_No { get; set; }
            public DateTime? AirExportInvoice_date { get; set; }
            public decimal? AirExportTotal_Amount { get; set; }
            public string AirExportBilling_id { get; set; }
        }
    }
    public class Customer
    {
        public string Sub_job_no { get; set; }
        public DateTime? Sub_job_date { get; set; }
        public int Master_job_id { get; set; }
        public int Sub_job_id { get; set; }
        public string Total_weight { get; set; }
        public int PKG { get; set; }
        public string GRWT { get; set; }
        public string Party_InvocieNo { get; set; }
        public string JobType { get; set; }
        public string MAWB { get; set; }
        public string HAWB { get; set; }
        public string Destination { get; set; }
        //public string POD { get; set; }
        public string pod_code { get; set; }
        public string pol_code { get; set; }
        public string pod_name { get; set; }
        public string pol_name { get; set; }
        public string Branch { get; set; }
        public DateTime? ETD { get; set; }
        public DateTime? ETA { get; set; }
        public string STATUS { get; set; }
        public string Track { get; set; }
        public string LOCKER { get; set; }
        public string Mode { get; set; }
        public string Department { get; set; }
        public List<Clr_Det> Clr_det { get; set; }
        public string Doctype { get; set; }
        public string sb_be_no { get; set; }
        public string fileref { get; set; }
        public string Mawb_Mbl_Fileref { get; set; }
        public string Hawb_Hbl_Fileref { get; set; }
        public string Shppr_Cnee { get; set; } //If Export then Consignee else Shipper
        public string vessel_voyag { get; set; }
        public string flightno { get; set; }
        public bool? is_tracking_enabled { get; set; }
        public string mawb_curr_track_status { get; set; }
        public string vol { get; set; }//if FCL then show container type else volume
        public string Container_no { get; set; }
        public string Container_type { get; set; }
    }
    public class Clr_Det
    {
        public string Party_InvocieNo { get; set; }
        public string Clr_job_no { get; set; }
        public string Clr_job_id { get; set; }
        public string sb_be_no { get; set; }
        public int PKG { get; set; }
        public string GRWT { get; set; }
        public string Doctype { get; set; }
        public string fileref { get; set; }
        public string department { get; set; }

    }
}