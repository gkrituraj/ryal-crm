﻿using CRMAdmin.Models;
using Ryal_CRM.Models;
using Ryal_CRM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ryal_CRM.Controllers
{
    [SessionTimeOut]
    public class EnquiryController : Controller
    {
        private RyalErpDbEntities _context = null;
        public EnquiryController()
        {
            _context = new RyalErpDbEntities();
        }

        // GET: Enquiry
        public ActionResult Index(string Select_Parameter, string Search_Value, string Search_To_Dt, string Search_From_Dt, string Enquiry_Status,string Dept)
        {
            if (string.IsNullOrEmpty(Search_To_Dt) && !string.IsNullOrEmpty(Search_From_Dt))
                Search_To_Dt = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            var priviewfinyear = Session["financialyear"].ToString();//financial year
            var enqyear = priviewfinyear.Substring(3, 2);

            var crm_user_no = Convert.ToInt32(Session["crm_user_no"]);
            var userInDB = _context.tbl_crm_user_master.Where(x => x.crm_user_no == crm_user_no).FirstOrDefault();
            List<int?> crm_unt_business_unit_no = new List<int?>();
            var Bu_unit_no = userInDB.tbl_crm_business_unit.Select(x => x.crm_unt_business_unit_no).ToList();
            crm_unt_business_unit_no.AddRange(Bu_unit_no);
            if (Session["docno"].ToString() != "0")
            {
                crm_unt_business_unit_no = new List<int?>();
                crm_unt_business_unit_no.Add(Convert.ToInt32(Session["docno"].ToString()));
            }

            var EnquiryDb = _context.tbl_enquiry_management.Where(x => x.Enquiry_Number.Contains("/" + enqyear) && crm_unt_business_unit_no.Contains(x.Enquiry_Party_No)).ToList();

            var EnquiryList = from ab in EnquiryDb where ab.Enquiry_Deleted != "Yes" && ab.Enquiry_Enable_Disable != "Disabled" select ab;

            var endtime = " 23:59:59";
            ViewBag.Select_Parameter = Select_Parameter;
            ViewBag.Search_Value = Search_Value;
            ViewBag.SearchFromDt = Search_From_Dt;
            ViewBag.SearchToDt = Search_To_Dt;
            ViewBag.Enquiry_Status = Enquiry_Status;
            ViewBag.Dept = Dept;

            switch (Select_Parameter)
            {
                case "ENQUIRY NO":
                    if (!string.IsNullOrEmpty(Search_Value))
                    {
                        EnquiryList = EnquiryList.Where(x => x.Enquiry_Number.Contains(Search_Value)).OrderByDescending(x => x.Enquiry_ID);
                    }
                    break;
                case "POL":
                    if (!string.IsNullOrEmpty(Search_Value))
                    {
                        EnquiryList = EnquiryList.Where(x => x.Enquiry_Port_Loading_Name.ToUpper().Contains(Search_Value.ToUpper())).OrderByDescending(x => x.Enquiry_ID);
                    }
                    break;
                case "POD":
                    if (!string.IsNullOrEmpty(Search_Value))
                    {
                        EnquiryList = EnquiryList.Where(x => x.Enquiry_Port_Discharge_Name.ToUpper().Contains(Search_Value.ToUpper())).OrderByDescending(x => x.Enquiry_ID);
                    }
                    break;
            }

            if (!string.IsNullOrEmpty(Search_From_Dt) && !string.IsNullOrEmpty(Search_To_Dt))
            {
                DateTime FromDt = DateTime.Parse(Search_From_Dt);
                DateTime ToDt = DateTime.Parse(Search_To_Dt + endtime);
                EnquiryList = EnquiryList.Where(d => d.Enquiry_Date >= FromDt && d.Enquiry_Date <= ToDt).OrderBy(ab => ab.Enquiry_ID);
            }
            if (!string.IsNullOrEmpty(Enquiry_Status))
            {
                EnquiryList = EnquiryList.Where(x => x.Enquiry_final_status != null);
                EnquiryList = EnquiryList.Where(x => x.Enquiry_final_status.Contains(Enquiry_Status)).OrderBy(ab => ab.Enquiry_ID);
            }
            if (!string.IsNullOrEmpty(Dept))
            {
                EnquiryList = EnquiryList.Where(x => x.Enquiry_Number.Contains(Dept)).OrderBy(ab => ab.Enquiry_ID);
            }

            if (TempData["AlertMessage"] != null)
            {
                ViewBag.alertMessage = TempData["alertMessage"].ToString();
            }

            /*View model added*/
            EnquiryVm viewModel = new EnquiryVm();
            viewModel.EnquiryList = EnquiryList.ToList();
            return View(viewModel);
        }
    }
}