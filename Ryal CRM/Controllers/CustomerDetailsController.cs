﻿using AESEncryptDecrypt;
using CRMAdmin.Models;
using Ryal_CRM.Models;
using RyalErp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ryal_CRM.Controllers
{
    [SessionTimeOut]
    public class CustomerDetailsController : Controller
    {
        RyalErpDbEntities _context = null;

        //call constuctor
        public CustomerDetailsController()
        {
            _context = new RyalErpDbEntities();
        }

        // GET: CustomerDetails/Edit/1  
        public ActionResult Cust_Profile(string mid)
        {
            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var decryptedid = encryptDecrypt.DecryptString(mid);
            int id = Convert.ToInt32(decryptedid);
            var addressBookInDb = _context.tbl_address_book.SingleOrDefault(d => d.add_contact_no == id);
            if (addressBookInDb == null)
                return HttpNotFound();
            return View("Profile", addressBookInDb);
        }

        // POST: /AddressBookMaster/Save
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(tbl_address_book address_book, string phone_label, string phone_type, string country_code, string area_code, string mobile_no, string extension)
        {
            try
            {
                var addressBookInDb = _context.tbl_address_book.Single(d => d.add_contact_no == address_book.add_contact_no);

                addressBookInDb.add_contact_prefix = address_book.add_contact_prefix;
                addressBookInDb.add_contact_gender = address_book.add_contact_gender;
                addressBookInDb.add_contact_dob = address_book.add_contact_dob;
                addressBookInDb.add_contact_age = address_book.add_contact_age;
                addressBookInDb.add_contact_fname = address_book.add_contact_fname;
                addressBookInDb.add_contact_mname = address_book.add_contact_mname;
                addressBookInDb.add_contact_lname = address_book.add_contact_lname;
                addressBookInDb.add_contact_EmailType = address_book.add_contact_EmailType;
                addressBookInDb.add_contact_Email = address_book.add_contact_Email;
                addressBookInDb.updated_on = DateTime.Now;
                addressBookInDb.updated_by = int.Parse(Session["crm_user_no"].ToString());

                //Update Phone
                var tbl_add_phone = _context.tbl_add_phone.Where(x => x.add_contact_no == address_book.add_contact_no);
                _context.tbl_add_phone.RemoveRange(tbl_add_phone);
                _context.SaveChanges();
                if (!string.IsNullOrEmpty(phone_label) && !string.IsNullOrEmpty(phone_type) && !string.IsNullOrEmpty(country_code) && !string.IsNullOrEmpty(mobile_no))
                {
                    #region
                    tbl_add_phone add_phone = new tbl_add_phone();
                    var PhoneLabel = phone_label.Split(',');
                    var PhoneType = phone_type.Split(',');
                    var CountryCode = country_code.Split(',');
                    var AreaCode = area_code.Split(',');
                    var MobileNo = mobile_no.Split(',');
                    var Extension = extension.Split(',');
                    var phonetype = "";
                    for (int i = 0; i < PhoneLabel.Length; i++)
                    {
                        add_phone.add_contact_no = address_book.add_contact_no;
                        add_phone.add_phone_label = PhoneLabel[i];
                        if (PhoneType[i].ToLower() == "landline")
                        {
                            phonetype = "0";
                        }
                        else
                        {
                            phonetype = "1";
                        }
                        add_phone.add_phone_type = phonetype;
                        add_phone.add_mobile_country_code = CountryCode[i];
                        add_phone.add_phone_isd_code = AreaCode[i];
                        add_phone.add_phone_mobile_no = MobileNo[i];
                        if (!string.IsNullOrEmpty(Extension[i]))
                        {
                            try
                            {
                                add_phone.add_ext = int.Parse(Extension[i]);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        add_phone.updated_on = DateTime.Now;
                        add_phone.updated_by = int.Parse(Session["crm_user_no"].ToString());
                        add_phone.is_default = false;

                        _context.tbl_add_phone.Add(add_phone);
                        _context.SaveChanges();

                    }
                    #endregion
                }
            }
            catch (Exception)
            {
            }

            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var Encryptedid = encryptDecrypt.EncryptString(address_book.add_contact_no.ToString());
            return RedirectToAction("Cust_Profile", "CustomerDetails", new { mid = Encryptedid });
        }


        [HttpPost]
        public JsonResult ResetPassword(string newpassword, string oldpassword, int addbookno)
        {
            var returnmsg = "";
            try
            {
                AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
                var new_password = encryptDecrypt.EncryptString(newpassword);
                var old_password = encryptDecrypt.EncryptString(oldpassword);
                var userno = Convert.ToInt32(Session["crm_user_no"].ToString());
                var UserInDb = _context.tbl_crm_user_master.SingleOrDefault(x => x.crm_user_no == userno && x.crm_user_password == old_password);
                if (UserInDb == null)
                {
                    returnmsg = "Please Enter Correct Old Password";
                }
                else
                {
                    UserInDb.crm_user_password = new_password;
                    _context.Entry(UserInDb).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    returnmsg = "Password Reset Successful";
                }
            }
            catch (Exception)
            {
                returnmsg = "Password Not Reset";
            }
            return Json(returnmsg, JsonRequestBehavior.AllowGet);
        }


        // Attach Single Soft Copy
        [HttpPost]
        public ActionResult ProfilePhoto(HttpPostedFileBase attach_softcopy, string already_attach_ref)
        {
            var returnmsg = "Not Updated";
            var userno = Convert.ToInt32(Session["crm_user_no"].ToString());
            var add_contact_no = _context.tbl_crm_user_master.SingleOrDefault(x => x.crm_user_no == userno).crm_user_add_contact_no;
            try
            {
                if (attach_softcopy != null && attach_softcopy.ContentLength > 0)
                {
                    Stream st = attach_softcopy.InputStream;

                    string file_name = Path.GetFileName(attach_softcopy.FileName);
                    string s3_bucket_name = "ryalfiles/User";
                    string s3_bucket_dir_name = "";
                    string s3_bucket_file_name = file_name;

                    AmazonUploader myUploader = new AmazonUploader();

                    if (!string.IsNullOrWhiteSpace(already_attach_ref))
                    {
                        // Modify         
                        bool deleteFile = myUploader.DeletMyFileToS3(st, s3_bucket_name, s3_bucket_dir_name, s3_bucket_file_name, null, already_attach_ref);

                        if (deleteFile)
                        {
                            // File delete successfully.

                            // Add new file then.
                            string uploadFile = myUploader.SendMyFileToS3(st, s3_bucket_name, s3_bucket_dir_name, s3_bucket_file_name, add_contact_no.ToString());
                            string response = uploadFile.Split('|')[0];

                            if (response == "true")
                            {
                                var Add_contact_Db = _context.tbl_address_book.Find(add_contact_no);
                                Add_contact_Db.add_refrence_id = uploadFile.Split('|')[1].Trim();
                                _context.Entry(Add_contact_Db).State = System.Data.Entity.EntityState.Modified;
                                _context.SaveChanges();
                                returnmsg = "Profile Photo Updated^" + Add_contact_Db.add_refrence_id;
                            }
                        }
                    }
                    else
                    {
                        // Add
                        string uploadFile = myUploader.SendMyFileToS3(st, s3_bucket_name, s3_bucket_dir_name, s3_bucket_file_name, add_contact_no.ToString());
                        string response = uploadFile.Split('|')[0];

                        if (response == "true")
                        {
                            var Add_contact_Db = _context.tbl_address_book.Find(add_contact_no);
                            Add_contact_Db.add_refrence_id = uploadFile.Split('|')[1].Trim();
                            _context.Entry(Add_contact_Db).State = System.Data.Entity.EntityState.Modified;
                            _context.SaveChanges();
                            returnmsg = "Profile Photo Updated^" + Add_contact_Db.add_refrence_id;
                        }
                    }
                }
                else
                {
                    returnmsg = "Upload document can't blank.";
                    TempData["AlertMessage"] = "Upload document can't blank.";
                }
            }
            catch (Exception ex)
            {
                returnmsg = "Something went wrong, Error = " + ex.Message;
                TempData["AlertMessage"] = "Something went wrong, Error = " + ex.Message;
            }


            //AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            //var Encryptedid = encryptDecrypt.EncryptString(add_contact_no.ToString());
            return Json(returnmsg, JsonRequestBehavior.AllowGet);
        }

    }
}