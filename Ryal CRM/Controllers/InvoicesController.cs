﻿using AESEncryptDecrypt;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using CRMAdmin.Models;
using Ryal_CRM.Models;
using RyalErp.Models;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ryal_CRM.Controllers
{
    [SessionTimeOut]
    public class InvoicesController : Controller
    {
        AESEncrypt_Decrypt encryptDecrypt = null;
        Save_Edit_Search_Delete_AuditLog commclass = null;
        private RyalErpDbEntities _context = null;
        public InvoicesController()
        {
            encryptDecrypt = new AESEncrypt_Decrypt();
            commclass = new Save_Edit_Search_Delete_AuditLog();
            _context = new RyalErpDbEntities();
        }


        // Save pdf document
        #region 
        AmazonUploader myUploader = new AmazonUploader();

        //string unique_file_name = "";

        IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);


        #endregion

        public ActionResult AEC_PDF(string mid)
        {

            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var decryptedid = encryptDecrypt.DecryptString(mid);
            int id = Convert.ToInt32(decryptedid);
            var invoiceDB = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id == id).FirstOrDefault();
            var billingpartyname1 = _context.tbl_company_business_unit.Where(x => x.unit_name.ToUpper() == invoiceDB.AirExportBilling_party.ToUpper()).FirstOrDefault();
            var billingparty = invoiceDB.AirExportBilling_party;
            var billingpartyaddress = invoiceDB.AirExportBillingParty_address + "<br>" + billingpartyname1.city_id_text_box + " - " + billingpartyname1.unit_pincode;

            var subjobDB = _context.Tbl_AirClearance_subjob.Where(x => x.AirClearance_Subjob_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
            var shippername = "";
            if (subjobDB.AirClearance_Reference_type != 0)
            {
                var refcmpny = subjobDB.AirClearance_Shipper_company;
                var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == refcmpny).Select(x => x.unit_name).FirstOrDefault();
                if (unitname.Length > 30)
                    shippername = unitname.Substring(0, 30);
                else
                    shippername = unitname;
            }


            var businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_name.ToLower() == billingparty.ToLower()).FirstOrDefault();
            if (businessuitDB == null)
            {
                businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == subjobDB.AirClearance_Shipper_company).FirstOrDefault();
            }
            var country = businessuitDB.tbl_country_master.country_name;
            var pos = "";
            if (country.ToLower() == "india")
            {
                var statename = businessuitDB.tbl_state_master.state_name;
                var stateno = businessuitDB.tbl_state_master.state_GST_id;
                pos = stateno + "-" + statename;
            }
            var gstno = businessuitDB.unit_gst_no;
            var panno = businessuitDB.unit_pan_no;

            var segment = subjobDB.AEC_segment_no;
            var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
            switch (segment)
            {
                case 0:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    break;
                case 1:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                    break;
                case 2:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                    break;
            }
            var branchaddr = branchInDb.ryal_branch_address;
            if (!string.IsNullOrEmpty(branchaddr))
            {
                branchaddr = branchaddr.Replace("\n", "<br>");
            }
            var ourgstin = branchInDb.ryal_branch_GST_no;
            var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;

            var accountname = "";
            if (subjobDB.AirClearance_Reference_type != 0)
            {
                var accountid = subjobDB.AirClearance_Shipper_company;
                accountname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == accountid).Select(x => x.unit_name).FirstOrDefault();
            }


            var invoiceno = invoiceDB.AirExportInvoice_No;
            var invoicedate = string.Format("{0:dd-MM-yyyy}", invoiceDB.AirExportInvoice_date);
            var subjobno = invoiceDB.AirExportSubJobNo;
            var masterjobno = invoiceDB.AirExportMasterJobNo;
            var package = subjobDB.AirClearance_Total_packages + " " + subjobDB.AirClearance_Packaging_type;
            var grosswt = subjobDB.AirClearance_Total_weight;// + " / " + subjobDB.chargeable_wt;
            var currency = invoiceDB.billing_currency + " / " + invoiceDB.billing_exchrate;
            var Mainremark = invoiceDB.AirExportRemarks;

            var subjobid = invoiceDB.AirExportSubJobId;
            var originid = _context.Tbl_AirClearance_subjob.Where(s => s.AirClearance_Subjob_id == subjobid).Select(x => x.AirClearance_Destination_airport_id).FirstOrDefault();
            var Description = "";// _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Subjob_id == subjobid).Select(x => x.AirImpImport_Description).FirstOrDefault();
            var shipperref = "";//_context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Subjob_id == subjobid).Select(x => x.AirImpImport_InvoiceNo).FirstOrDefault();
            var originname = _context.tbl_airport_master.Where(x => x.airport_no == originid).Select(x => x.airport_name).FirstOrDefault();

            var irnno = invoiceDB.EInvoice_IrnNo;
            if (irnno == null)
            {
                irnno = "";
            }
            var filename = invoiceDB.Qr_code_file_name;
            if (filename == null)
            {
                filename = "";
            }



            var rocount = 0; var charge_name = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
            var cgstrate = ""; var sgstrate = ""; var igstrate = "";
            var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
            var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == invoiceDB.AirExportBilling_id).ToList();
            for (int i = 0; i < chargeslist.Count(); i++)
            {
                if (i > 0)
                {
                    charge_name = charge_name + ",";
                    hsnno = hsnno + ",";
                    cgst = cgst + ",";
                    sgst = sgst + ",";
                    igst = igst + ",";
                    Amount = Amount + ",";
                    cgstrate = cgstrate + ",";
                    sgstrate = sgstrate + ",";
                    igstrate = igstrate + ",";
                }
                var cgstrate1 = "";
                var sgstrate1 = "";
                var igstrate1 = "";
                rocount += 1;
                var charge_no = chargeslist[i].AirExportCharge_Code;
                charge_name = charge_name + _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_name).FirstOrDefault();

                cgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_cgst.ToString()).FirstOrDefault();
                sgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_sgst.ToString()).FirstOrDefault();
                igstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_igst.ToString()).FirstOrDefault();


                if (string.IsNullOrWhiteSpace(sgstrate1))
                {
                    sgstrate1 = "0 ";
                    igstrate1 = "0 ";
                    cgstrate1 = "0 ";
                }

                sgstrate = sgstrate + sgstrate1;
                igstrate = igstrate + igstrate1;
                cgstrate = cgstrate + cgstrate1;

                var charge_remark = chargeslist[i].billing_charges_remark;
                if (!string.IsNullOrEmpty(charge_remark))
                {
                    charge_name += " ( " + charge_remark + " )";
                }
                var hsnid = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no).charge_sac;

                hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsnid).sac_code.Split(' ')[1];
              
                cgst = cgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_CGST);
                sgst = sgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_SGST);
                igst = igst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_IGST);
                Amount = Amount + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_Amount);
           
                CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
            }

            var finalNetamt = string.Format("{0:0.00}", NetAmount);
            var netamt = NetAmount + CGSTAmt + SGSTAmt + IGSTAmt;
            var netamt2 = string.Format("{0:0.00}", netamt);

            var netamt1 = netamt2.ToString().Split('.')[1];
            var finalroundof = "";
            if (Convert.ToDecimal(netamt1) >= 50)
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]) + 1;
                Round_off = 100 - Convert.ToDouble(netamt1);
                if (Round_off > 9)
                    finalroundof = "0." + Round_off.ToString();
                else
                    finalroundof = "0.0" + Round_off.ToString();
            }
            else
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]);
                Round_off = Convert.ToDouble(netamt2.ToString().Split('.')[1]);
                if (Round_off > 0)
                    if (Round_off > 9)
                        finalroundof = "-0." + Round_off.ToString();
                    else
                        finalroundof = "-0.0" + Round_off.ToString();
                else
                    finalroundof = "0." + Round_off.ToString();
            }
            var CGSTAmt1 = string.Format("{0:0.00}", CGSTAmt);
            var SGSTAmt1 = string.Format("{0:0.00}", SGSTAmt);
            var IGSTAmt1 = string.Format("{0:0.00}", IGSTAmt);


            var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
            var NumToWord = commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));

            if (invoiceDB.billing_currency == "INR")
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }
            else if (invoiceDB.billing_currency == "USD")
            {
                NumToWord = "USD " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "EUR")
            {
                NumToWord = "EURO " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "GBP")
            {
                NumToWord = "GBP " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " Pence";
                finalroundof = "0.00";
            }
            else
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }

            var drafedorapprover = "";
            var approvedornot = "";
            if (invoiceDB.billing_Approval == "yes")
            {
                var approvebyid = _context.tbl_user_master.Where(x => x.user_no == invoiceDB.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd-MM-yyyy hh:mm tt}", invoiceDB.billing_Approval_on);
                approvedornot = "yes";
            }
            else
            {
                drafedorapprover = "DRAFT BILL";
                approvedornot = "no";
            }


            var Invoice_type = invoiceDB.invoice_type;

            var hawbno = ""; var hawbdate = ""; var mawbno = ""; var mawbdate = "";
            if (invoiceDB.AirExportSubJobId != null)
            {
                var subid = invoiceDB.AirExportSubJobId;
                var mainmasterjobid = _context.Tbl_sub_job.Where(x => x.Sub_job_id == subid).Select(x => x.Master_job_id).FirstOrDefault();
                var MawbDB = _context.tbl_mawb_details.Where(x => x.MAWB_master_job_id == mainmasterjobid).FirstOrDefault();
                if (MawbDB != null)
                    try
                    {
                        mawbdate = string.Format("{0:dd-MM-yyyy}", MawbDB.MAWB_Created_datetime);
                        var mawbstockno = MawbDB.MAWB_stock_no.ToString();
                        var mawb1 = "";
                        var mawb_no = _context.tbl_mawb_stock_details.SingleOrDefault(x => x.mawb_stock_no == mawbstockno).mawb_no;
                        var mawbinitials = _context.tbl_mawb_stock.SingleOrDefault(x => x.mawb_no == mawb_no).mawb_initials;
                        string abc = mawbstockno.ToString();

                        if (abc.Length == 8)
                        {
                            mawb1 = abc.Substring(0, 4) + " ";
                            mawb1 += abc.Substring(4, 4);
                            mawbno = mawbinitials + "-" + mawb1;
                        }
                        else
                        {
                            mawbno = mawbinitials + "-" + abc;
                        }
                    }
                    catch (Exception)
                    {
                        mawbno = "000-0000 0000";
                    }
                var jobtype = _context.Tbl_master_job.Where(x => x.Master_job_id == mainmasterjobid).Select(X => X.Master_job_type).FirstOrDefault();
                if (jobtype != "Direct")
                {
                    var HawbDB = _context.tbl_hawb_details.Where(x => x.Hawb_sub_job_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
                    if (HawbDB != null)
                    {
                        hawbdate = string.Format("{0:dd-MM-yyyy}", HawbDB.HAWB_Created_datetime);
                        hawbno = HawbDB.HAWB_stock_no;
                    }
                }
            }

            string jsonString = billingparty + "|" + billingpartyaddress + "|" + pos + "|" + gstno + "|" + panno + "|" + accountname + "|" + Description + "|" + "" + "|" + invoiceno + "|" + invoicedate + "|" + originname + "|" + "" + "|" + "" + "|" + "" + "|" + currency + "|" + masterjobno + "|" + subjobno + "|" + "" + "|" + package + "|" + grosswt + "|" + shipperref + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + igst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt1 + "|" + SGSTAmt1 + "|" + IGSTAmt1 + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + "" + "|" + Mainremark + "|" + drafedorapprover + "|" + approvedornot + "|" + cgstrate + "|" + sgstrate + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + irnno + "|" + filename + "|" + Invoice_type + "|" + shippername;

            ViewBag.InvoicePrint = jsonString;
            return View();

        }
        public ActionResult AEF_PDF(string mid)
        {

            var decryptedid = encryptDecrypt.DecryptString(mid);
            int id = Convert.ToInt32(decryptedid);
            var invoiceDB = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id == id).FirstOrDefault();
            var billingpartyname1 = _context.tbl_company_business_unit.Where(x => x.unit_name.ToUpper() == invoiceDB.AirExportBilling_party.ToUpper()).FirstOrDefault();
            var billingparty = invoiceDB.AirExportBilling_party;
            var billingpartyaddress = invoiceDB.AirExportBillingParty_address + "<br>" + billingpartyname1.city_id_text_box + " - " + billingpartyname1.unit_pincode;
            if (!string.IsNullOrEmpty(billingpartyaddress))
            {
                billingpartyaddress = billingpartyaddress.Replace("\n", "<br>");
            }

            var subjobDB = _context.Tbl_sub_job.Where(x => x.Sub_job_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
            var shippername = "";
            if (subjobDB.Reference_type != 0)
            {
                var refcmpny = subjobDB.Shipper_company;
                var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == refcmpny).Select(x => x.unit_name).FirstOrDefault();
                if (unitname.Length > 30)
                    shippername = unitname.Substring(0, 30);
                else
                    shippername = unitname;
            }

            var businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_name.ToLower() == billingparty.ToLower()).FirstOrDefault();
            if (businessuitDB == null)
            {
                businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == subjobDB.Shipper_company).FirstOrDefault();
            }
            var country = businessuitDB.tbl_country_master.country_name;
            var pos = "";
            if (country.ToLower() == "india")
            {
                var statename = businessuitDB.tbl_state_master.state_name;
                var stateno = businessuitDB.tbl_state_master.state_GST_id;
                pos = stateno + "-" + statename;
            }
            var gstno = businessuitDB.unit_gst_no;
            var panno = businessuitDB.unit_pan_no;

            var accountname = "";
            if (subjobDB.Reference_type != 0)
            {
                var accountid = subjobDB.Shipper_company;
                accountname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == accountid).Select(x => x.unit_name).FirstOrDefault();
            }

            var segment = subjobDB.AEF_segment_no;
            var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
            switch (segment)
            {
                case 0:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    break;
                case 1:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                    break;
                case 2:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                    break;
            }
            var branchaddr = branchInDb.ryal_branch_address;
            if (!string.IsNullOrEmpty(branchaddr))
            {
                branchaddr = branchaddr.Replace("\n", "<br>");
            }
            var ourgstin = branchInDb.ryal_branch_GST_no;
            var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;

            var mjobid = invoiceDB.AirExportMaster_Job_id;
            var MawbDB = _context.tbl_mawb_details.Where(x => x.MAWB_master_job_id == mjobid).FirstOrDefault();


            var package = subjobDB.Total_packages;
            var grosswt = subjobDB.Total_weight + " / " + subjobDB.chargeable_wt;

            var masterjobid = invoiceDB.AirExportMaster_Job_id;
            if (invoiceDB.billing_Against == "hawb" || invoiceDB.billing_Against == null)
            {
                var jobtype = _context.Tbl_master_job.FirstOrDefault(x => x.Master_job_id == masterjobid).Master_job_type;
                if (jobtype == "Direct")
                {
                    var mawbid = _context.tbl_mawb_details.FirstOrDefault(x => x.MAWB_master_job_id == masterjobid).MAWB_id;
                    var MawbInDb = _context.tbl_mawb_cargo_details1.Where(x => x.MAWB_id == mawbid).ToList();
                    package = MawbInDb.Sum(x => x.MAWB_no_of_pieces);
                    grosswt = MawbInDb.Sum(x => x.MAWB_gross_Wt) + " / " + MawbInDb.Sum(x => x.MAWB_charge_wt);
                }
                else
                {
                    var hawbid = _context.tbl_hawb_details.FirstOrDefault(x => x.Hawb_sub_job_id == invoiceDB.AirExportSubJobId).Hawb_id;
                    var HawbInDb = _context.tbl_hawb_cargo_details1.Where(x => x.HAWB_id == hawbid).ToList();
                    package = HawbInDb.Sum(x => x.HAWB_no_of_pieces);
                    grosswt = HawbInDb.Sum(x => x.HAWB_gross_Wt) + " / " + HawbInDb.Sum(x => x.HAWB_charge_wt);
                }
            }
            else
            {
                var mawbid = _context.tbl_mawb_details.FirstOrDefault(x => x.MAWB_master_job_id == masterjobid).MAWB_id;
                var MawbInDb = _context.tbl_mawb_cargo_details1.Where(x => x.MAWB_id == mawbid).ToList();
                package = MawbInDb.Sum(x => x.MAWB_no_of_pieces);
                grosswt = MawbInDb.Sum(x => x.MAWB_gross_Wt) + " / " + MawbInDb.Sum(x => x.MAWB_charge_wt);
            }




            var sbno_date = "";
            var subjobid = _context.tbl_hawb_details.Where(x => x.Hawb_sub_job_id == invoiceDB.AirExportSubJobId).ToList();
            var hawbno = ""; var hawbdate = ""; var destination = ""; var expno = "";
            if (subjobid.Count == 0)
            {
                destination = MawbDB.mawb_print_destination;
                expno = MawbDB.MAWB_exp_invoice_hash;
                sbno_date = MawbDB.MAWB_shipping_bill_no + " / " + string.Format("{0:dd-MM-yyyy}", MawbDB.MAWB_shipping_bill_date);
            }
            else
            {
                var hawbInDb = _context.tbl_hawb_details.Where(x => x.Hawb_sub_job_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
                hawbno = hawbInDb.HAWB_stock_no;
                hawbdate = string.Format("{0:dd-MM-yyyy}", hawbInDb.HAWB_csr_date);
                destination = hawbInDb.hawb_print_destination;
                expno = hawbInDb.HAWB_exp_invoice_hash;
                sbno_date = hawbInDb.HAWB_shipping_bill_no + " / " + string.Format("{0:dd-MM-yyyy}", hawbInDb.Shipping_bill_date);

            }
            var airlineid = MawbDB.tbl_mawb_airline.Where(x => x.MAWB_id == MawbDB.MAWB_id).Select(x => x.MAWB_Airline_company_id).FirstOrDefault();
            var airlinename = _context.tbl_company_master.Where(x => x.company_no == airlineid).Select(x => x.company_name).FirstOrDefault();

            var description = MawbDB.mawb_billing_description;
            if (!string.IsNullOrEmpty(description))
            {
                description = description.Replace('\n', ' ');
            }
            var invoiceno = invoiceDB.AirExportInvoice_No;
            var invoicedate = string.Format("{0:dd-MM-yyyy}", invoiceDB.AirExportInvoice_date);
            var mawbstockno = MawbDB.MAWB_stock_no;
            var mawbno = "";
            var mawb1 = "";
            try
            {
                var unitno = MawbDB.MAWB_AirLine_code;
                var compno = _context.tbl_company_business_unit.SingleOrDefault(x => x.unit_no.ToString() == unitno).company_no;
                var Mawb_No = (from det in _context.tbl_mawb_stock_details join main in _context.tbl_mawb_stock on det.mawb_no equals main.mawb_no where det.mawb_stock_no == mawbstockno.ToString() && main.airline == compno select new { mawbno = main.mawb_no }).FirstOrDefault();
                var mawbstockno1 = Convert.ToInt32(Mawb_No.mawbno);
                var mawbinitials = _context.tbl_mawb_stock.SingleOrDefault(x => x.mawb_no == mawbstockno1).mawb_initials;
                string abc = mawbstockno.ToString();

                if (abc.Length == 8)
                {
                    mawb1 = abc.Substring(0, 4) + " ";
                    mawb1 += abc.Substring(4, 4);
                    mawbno = mawbinitials + "-" + mawb1;
                }
                else
                {
                    mawbno = mawbinitials + "-" + abc;
                }
            }
            catch (Exception)
            {
                mawbno = "000-0000 0000";
            }


            var irnno = invoiceDB.EInvoice_IrnNo;
            if (irnno == null)
            {
                irnno = "";
            }
            var filename = invoiceDB.Qr_code_file_name;
            if (filename == null)
            {
                filename = "";
            }

            var mawbdate = string.Format("{0:dd-MM-yyyy}", MawbDB.MAWB_csr_date);
            var subjobno = invoiceDB.AirExportSubJobNo;
            var masterjobno = invoiceDB.AirExportMasterJobNo;
            var currency = invoiceDB.billing_currency + " / " + invoiceDB.billing_exchrate;
            var Mainremark = invoiceDB.AirExportRemarks;

            var rocount = 0; var charge_name = ""; var charge_remark = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
            var cgstrate = ""; var sgstrate = ""; var igstrate = "";
            var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
            var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == invoiceDB.AirExportBilling_id).ToList();
            for (int i = 0; i < chargeslist.Count(); i++)
            {
                if (i > 0)
                {
                    charge_name = charge_name + ",";
                    charge_remark = charge_remark + ",";
                    hsnno = hsnno + ",";
                    cgst = cgst + ",";
                    sgst = sgst + ",";
                    igst = igst + ",";
                    Amount = Amount + ",";
                    cgstrate = cgstrate + ",";
                    sgstrate = sgstrate + ",";
                    igstrate = igstrate + ",";
                }
                var cgstrate1 = "";
                var sgstrate1 = "";
                var igstrate1 = "";
                rocount += 1;
                var charge_no = chargeslist[i].AirExportCharge_Code;
                charge_name = charge_name + _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_name).FirstOrDefault();
                cgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_cgst.ToString()).FirstOrDefault();
                sgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_sgst.ToString()).FirstOrDefault();
                igstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_igst.ToString()).FirstOrDefault();


                if (string.IsNullOrWhiteSpace(sgstrate1))
                {
                    sgstrate1 = "0 ";
                    igstrate1 = "0 ";
                    cgstrate1 = "0 ";
                }

                sgstrate = sgstrate + sgstrate1;
                igstrate = igstrate + igstrate1;
                cgstrate = cgstrate + cgstrate1;

                var charge_remark1 = chargeslist[i].billing_charges_remark;
                if (!string.IsNullOrEmpty(charge_remark1))
                {
                    charge_remark += charge_remark1;
                    charge_name += " ( " + charge_remark1 + " ) ";
                }
                var hsnid = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no).charge_sac;

                hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsnid).sac_code.Split(' ')[1];
                cgst = cgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_CGST);
                CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                sgst = sgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_SGST);
                SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                igst = igst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_IGST);
                IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                Amount = Amount + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_Amount);
                NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
            }

            var finalNetamt = string.Format("{0:0.00}", NetAmount);
            var netamt = NetAmount + CGSTAmt + SGSTAmt + IGSTAmt;
            var netamt2 = string.Format("{0:0.00}", netamt);

            var netamt1 = netamt2.ToString().Split('.')[1];
            var finalroundof = "";
            if (Convert.ToDecimal(netamt1) >= 50)
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]) + 1;
                Round_off = 100 - Convert.ToDouble(netamt1);
                if (Round_off > 9)
                    finalroundof = "0." + Round_off.ToString();
                else
                    finalroundof = "0.0" + Round_off.ToString();
            }
            else
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]);
                Round_off = Convert.ToDouble(netamt2.ToString().Split('.')[1]);
                if (Round_off > 0)
                    if (Round_off > 9)
                        finalroundof = "-0." + Round_off.ToString();
                    else
                        finalroundof = "-0.0" + Round_off.ToString();
                else
                    finalroundof = "0." + Round_off.ToString();
            }
            var CGSTAmt1 = string.Format("{0:0.00}", CGSTAmt);
            var SGSTAmt1 = string.Format("{0:0.00}", SGSTAmt);
            var IGSTAmt1 = string.Format("{0:0.00}", IGSTAmt);


            var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
            var NumToWord = commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));

            if (invoiceDB.billing_currency == "INR")
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }
            else if (invoiceDB.billing_currency == "USD")
            {
                NumToWord = "USD " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "EUR")
            {
                NumToWord = "EURO " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "GBP")
            {
                NumToWord = "GBP " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " Pence";
                finalroundof = "0.00";
            }
            else
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }

            var drafedorapprover = "";
            var approvedornot = "";
            if (invoiceDB.billing_Approval == "yes")
            {
                var approvebyid = _context.tbl_user_master.Where(x => x.user_no == invoiceDB.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd/MM/yyyy hh:mm tt}", invoiceDB.billing_Approval_on);
                approvedornot = "yes";
            }
            else
            {
                drafedorapprover = "DRAFT BILL";
                approvedornot = "no";
            }


            var Invoice_type = invoiceDB.invoice_type;
            var BillingType = invoiceDB.BillingType;

            string jsonString = billingparty + "|" + billingpartyaddress + "|" + pos + "|" + gstno + "|" + panno + "|" + accountname + "|" + airlinename + "|" + description + "|" + invoiceno + "|" + invoicedate + "|" + mawbno + "|" + mawbdate + "|" + hawbno + "|" + hawbdate + "|" + currency + "|" + masterjobno + "|" + subjobno + "|" + destination + "|" + package + "|" + grosswt + "|" + expno + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + igst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt1 + "|" + SGSTAmt1 + "|" + IGSTAmt1 + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + sbno_date + "|" + Mainremark + "|" + drafedorapprover + "|" + approvedornot + "|" + cgstrate + "|" + sgstrate + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + irnno + "|" + filename + "|" + charge_remark + "|" + Invoice_type + "|" + shippername + "|" + BillingType;

            ViewBag.InvoicePrint = jsonString;
            return View();
            //return RedirectToAction("AEF_PDF", "Invoices", ViewBag.InvoicePrint);

        }
        public ActionResult AIC_PDF(string mid)
        {
            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var decryptedid = encryptDecrypt.DecryptString(mid);
            int id = Convert.ToInt32(decryptedid);
            var invoiceDB = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id == id).FirstOrDefault();
            var billingparty = invoiceDB.AirExportBilling_party;
            var billingpartyaddress = invoiceDB.AirExportBillingParty_address;
            var BillingInDb = _context.tbl_company_business_unit.FirstOrDefault(x => x.unit_name.ToUpper() == billingparty.ToUpper());
            if (BillingInDb != null)
            {
                billingpartyaddress += "<br>" + BillingInDb.city_id_text_box + " - " + BillingInDb.unit_pincode;
            }


            var subjobDB = _context.Tbl_AirImpClearance_subjob.Where(x => x.AirImpClearance_Subjob_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
            var Consigneename = "";
            var shippername = "";
            if (subjobDB.AirImpClearance_Reference_type != 0)
            {
                var refcmpny = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == subjobDB.AirImpClearance_Shipper_company).Select(X => X.unit_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(refcmpny))
                {
                    if (refcmpny.Length > 30)
                        Consigneename = refcmpny.Substring(0, 30);
                    else
                        Consigneename = refcmpny;
                }
            }
            else
            {
                shippername = subjobDB.AirImpClearance_Consignee_company;
            }

            var businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_name.ToLower() == billingparty.ToLower()).FirstOrDefault();
            if (businessuitDB == null)
            {
                businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == subjobDB.AirImpClearance_Shipper_company).FirstOrDefault();
            }
            var country = businessuitDB.tbl_country_master.country_name;
            var pos = "";
            if (country.ToLower() == "india")
            {
                var statename = businessuitDB.tbl_state_master.state_name;
                var stateno = businessuitDB.tbl_state_master.state_GST_id;
                pos = stateno + "-" + statename;
            }
            var gstno = businessuitDB.unit_gst_no;
            var panno = businessuitDB.unit_pan_no;
            var BoeNo = subjobDB.billof_entry_no + " / " + string.Format("{0:dd-MM-yyyy}", subjobDB.billof_entry_date);
            var HawbNo = subjobDB.AirImpClearance_hawb;
            var MawbNo = subjobDB.AirImpClearance_mawb;
            var segment = subjobDB.AIC_segment_no;
            var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
            switch (segment)
            {
                case 0:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    break;
                case 1:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                    break;
                case 2:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                    break;
            }
            var branchaddr = branchInDb.ryal_branch_address;
            if (!string.IsNullOrEmpty(branchaddr))
            {
                branchaddr = branchaddr.Replace("\n", "<br>");
            }
            var ourgstin = branchInDb.ryal_branch_GST_no;
            var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;

            var accountname = "";
            if (subjobDB.AirImpClearance_Reference_type != 0)
            {
                var accountid = subjobDB.AirImpClearance_Shipper_company;
                accountname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == accountid).Select(x => x.unit_name).FirstOrDefault();
            }


            var invoiceno = invoiceDB.AirExportInvoice_No;
            var invoicedate = string.Format("{0:dd-MM-yyyy}", invoiceDB.AirExportInvoice_date);
            var subjobno = invoiceDB.AirExportSubJobNo;
            var masterjobno = invoiceDB.AirExportMasterJobNo;
            var package = subjobDB.AirImpClearance_Total_packages + " " + subjobDB.AirImpClearance_Packaging_type;
            var grosswt = subjobDB.AirImpClearance_Total_weight;// + " / " + subjobDB.chargeable_wt;
            var currency = invoiceDB.billing_currency + " / " + invoiceDB.billing_exchrate;
            var Mainremark = invoiceDB.AirExportRemarks;

            var subjobid = invoiceDB.AirExportSubJobId;
            var originid = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Subjob_id == subjobid).Select(x => x.AirImpClearance_Origin_airport_id).FirstOrDefault();
            var Description = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Subjob_id == subjobid).Select(x => x.AirImpImport_Description).FirstOrDefault();
            var shipperref = _context.Tbl_AirImpClearance_subjob.Where(s => s.AirImpClearance_Subjob_id == subjobid).Select(x => x.AirImpImport_InvoiceNo).FirstOrDefault();
            var originname = _context.tbl_airport_master.Where(x => x.airport_no == originid).Select(x => x.airport_name).FirstOrDefault();

            var irnno = invoiceDB.EInvoice_IrnNo;
            if (irnno == null)
            {
                irnno = "";
            }
            var filename = invoiceDB.Qr_code_file_name;
            if (filename == null)
            {
                filename = "";
            }


            var rocount = 0; var charge_name = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
            var cgstrate = ""; var sgstrate = ""; var igstrate = "";
            var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
            var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == invoiceDB.AirExportBilling_id).ToList();
            for (int i = 0; i < chargeslist.Count(); i++)
            {
                if (i > 0)
                {
                    charge_name = charge_name + ",";
                    hsnno = hsnno + ",";
                    cgst = cgst + ",";
                    sgst = sgst + ",";
                    igst = igst + ",";
                    Amount = Amount + ",";
                    cgstrate = cgstrate + ",";
                    sgstrate = sgstrate + ",";
                    igstrate = igstrate + ",";
                }
                var cgstrate1 = "";
                var sgstrate1 = "";
                var igstrate1 = "";
                rocount += 1;
                var charge_no = chargeslist[i].AirExportCharge_Code;
                charge_name = charge_name + _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_name).FirstOrDefault();
                cgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_cgst.ToString()).FirstOrDefault();
                sgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_sgst.ToString()).FirstOrDefault();
                igstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_igst.ToString()).FirstOrDefault();


                if (string.IsNullOrWhiteSpace(sgstrate1))
                {
                    sgstrate1 = "0 ";
                    igstrate1 = "0 ";
                    cgstrate1 = "0 ";
                }

                sgstrate = sgstrate + sgstrate1;
                igstrate = igstrate + igstrate1;
                cgstrate = cgstrate + cgstrate1;

                var charge_remark = chargeslist[i].billing_charges_remark;
                if (!string.IsNullOrEmpty(charge_remark))
                {
                    charge_name += " ( " + charge_remark + " )";
                }
                var hsnid = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no).charge_sac;

                hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsnid).sac_code.Split(' ')[1];
                cgst = cgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_CGST);
                CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                sgst = sgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_SGST);
                SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                igst = igst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_IGST);
                IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                Amount = Amount + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_Amount);
                NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
            }

            var finalNetamt = string.Format("{0:0.00}", NetAmount);
            var netamt = NetAmount + CGSTAmt + SGSTAmt + IGSTAmt;
            var netamt2 = string.Format("{0:0.00}", netamt);

            var netamt1 = netamt2.ToString().Split('.')[1];
            var finalroundof = "";
            if (Convert.ToDecimal(netamt1) >= 50)
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]) + 1;
                Round_off = 100 - Convert.ToDouble(netamt1);
                if (Round_off > 9)
                    finalroundof = "0." + Round_off.ToString();
                else
                    finalroundof = "0.0" + Round_off.ToString();
            }
            else
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]);
                Round_off = Convert.ToDouble(netamt2.ToString().Split('.')[1]);
                if (Round_off > 0)
                    if (Round_off > 9)
                        finalroundof = "-0." + Round_off.ToString();
                    else
                        finalroundof = "-0.0" + Round_off.ToString();
                else
                    finalroundof = "0." + Round_off.ToString();
            }
            var CGSTAmt1 = string.Format("{0:0.00}", CGSTAmt);
            var SGSTAmt1 = string.Format("{0:0.00}", SGSTAmt);
            var IGSTAmt1 = string.Format("{0:0.00}", IGSTAmt);


            var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
            var NumToWord = commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));

            if (invoiceDB.billing_currency == "INR")
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }
            else if (invoiceDB.billing_currency == "USD")
            {
                NumToWord = "USD " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "EUR")
            {
                NumToWord = "EURO " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "GBP")
            {
                NumToWord = "GBP " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " Pence";
                finalroundof = "0.00";
            }
            else
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }

            var drafedorapprover = "";
            var approvedornot = "";
            if (invoiceDB.billing_Approval == "yes")
            {
                var approvebyid = _context.tbl_user_master.Where(x => x.user_no == invoiceDB.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd/MM/yyyy hh:mm tt}", invoiceDB.billing_Approval_on);
                approvedornot = "yes";
            }
            else
            {
                drafedorapprover = "DRAFT BILL";
                approvedornot = "no";
            }
            var advanceamt = invoiceDB.billing_advance_amt;
            if (advanceamt == null)
            {
                advanceamt = 0;
            }


            var Invoice_type = invoiceDB.invoice_type;

            string jsonString = billingparty + "|" + billingpartyaddress + "|" + pos + "|" + gstno + "|" + panno + "|" + accountname + "|" + Description + "|" + "" + "|" + invoiceno + "|" + invoicedate + "|" + originname + "|" + "" + "|" + "" + "|" + "" + "|" + currency + "|" + masterjobno + "|" + subjobno + "|" + "" + "|" + package + "|" + grosswt + "|" + shipperref + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + igst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt1 + "|" + SGSTAmt1 + "|" + IGSTAmt1 + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + BoeNo + "|" + Mainremark + "|" + drafedorapprover + "|" + approvedornot + "|" + cgstrate + "|" + sgstrate + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + advanceamt + "|" + MawbNo + "|" + HawbNo + "|" + irnno + "|" + filename + "|" + Invoice_type + "|" + Consigneename + "|" + shippername;

            ViewBag.InvoicePrint = jsonString;

            return View();


        }
        public ActionResult AIF_PDF(string mid)
        {

            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            string id = encryptDecrypt.DecryptString(mid.ToString());
            var InvoiceInDb = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id.ToString() == id).FirstOrDefault();
            var mawbid = 0;
            var subjobid = InvoiceInDb.AirExportSubJobId;
            var remark = InvoiceInDb.AirExportRemarks;
            try
            {
                if (subjobid != 0)
                {
                    var subjobInDb = _context.Tbl_AirImport_sub_job.SingleOrDefault(x => x.AirImport_Sub_job_id == subjobid);
                    var shippername = "";
                    if (subjobInDb.AirImport_Reference_type != 0)
                    {
                        var refcmpny = subjobInDb.AirImport_Shipper_company;
                        var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == refcmpny).Select(x => x.unit_name).FirstOrDefault();
                        if (unitname.Length > 30)
                            shippername = unitname.Substring(0, 30);
                        else
                            shippername = unitname;
                    }
                    mawbid = subjobInDb.AirImport_Master_job_id;
                    var billingpartyname = _context.tbl_company_business_unit.Where(x => x.unit_name.ToLower() == InvoiceInDb.AirExportBilling_party.ToLower()).FirstOrDefault();

                    var consinee_name = billingpartyname.unit_name;
                    var consinee_addr = billingpartyname.unit_address + "<br>" + billingpartyname.city_id_text_box + " - " + billingpartyname.unit_pincode;
                    var consignee = consinee_name + "<br>" + consinee_addr.Replace("\n", "<br>");

                    var statename = billingpartyname.state_id_text_box;// "";
                    var state_id = billingpartyname.state_id;
                    var pos = "";
                    if (state_id > 0)
                    {
                        statename = _context.tbl_state_master.SingleOrDefault(x => x.state_no == state_id).state_name;
                        var stateno = billingpartyname.tbl_state_master.state_GST_id;
                        pos = stateno + "-" + statename;
                    }
                    var gstno = billingpartyname.unit_gst_no;


                    var segment = subjobInDb.AIF_segment_no;
                    var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    switch (segment)
                    {
                        case 0:
                            branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                            break;
                        case 1:
                            branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                            break;
                        case 2:
                            branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                            break;
                    }
                    var branchaddr = branchInDb.ryal_branch_address;
                    if (!string.IsNullOrEmpty(branchaddr))
                    {
                        branchaddr = branchaddr.Replace("\n", "<br>");
                    }
                    var ourgstin = branchInDb.ryal_branch_GST_no;
                    var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;


                    var irnno = InvoiceInDb.EInvoice_IrnNo;
                    if (irnno == null)
                    {
                        irnno = "";
                    }
                    var filename = InvoiceInDb.Qr_code_file_name;
                    if (filename == null)
                    {
                        filename = "";
                    }

                    var notify1 = subjobInDb.AirImport_Notify1_name;
                    var notify = "";
                    if (notify1 != null && notify1 != "")
                    {
                        notify = notify1;// + "<br>" + notify1_add.Replace("\n", "<br>");
                    }
                    var shipper_name = "";
                    if (subjobInDb.AirImport_Consignee_company != null && subjobInDb.AirImport_Consignee_company != "" && subjobInDb.AirImport_Consignee_company != "NA")
                    {
                        shipper_name = subjobInDb.AirImport_Consignee_company;
                    }

                    var InvoiceNo = InvoiceInDb.AirExportInvoice_No;
                    var InvoiceDt = string.Format("{0:dd-MM-yyyy}", InvoiceInDb.AirExportInvoice_date);
                    var mawb_no = _context.Tbl_AirImport_master_job.SingleOrDefault(x => x.AirImport_Master_job_id == subjobInDb.AirImport_Master_job_id).AirImport_Mawb_No;
                    var mawb_date = string.Format("{0:dd-MM-yyyy}", _context.Tbl_AirImport_master_job.SingleOrDefault(x => x.AirImport_Master_job_id == subjobInDb.AirImport_Master_job_id).AirImport_Mawb_Date);
                    var mawbno_date = mawb_no + " / " + mawb_date;

                    var hawb_no = subjobInDb.AirImport_Hawb_No;
                    var hawb_date = string.Format("{0:dd-MM-yyyy}", subjobInDb.AirImport_Hawb_Date);
                    var hawbno_date = hawb_no + " / " + hawb_date;

                    var NoOfPackage = subjobInDb.AirImport_Package_Count;
                    var packageType = subjobInDb.AirImport_Package_Type;
                    var package = NoOfPackage + " " + packageType;
                    var Gross_weight = string.Format("{0:0.00}", subjobInDb.AirImport_Gross_Weight);
                    var chargeable_weight = string.Format("{0:0.00}", subjobInDb.AirImport_Chargeable_Weight);
                    var gross_chargeable = Gross_weight + " / " + chargeable_weight + "Kgs";

                    var masterjobid = InvoiceInDb.AirExportMaster_Job_id;
                    if (InvoiceInDb.billing_Against == "hawb" || InvoiceInDb.billing_Against == null)
                    {
                        var HawbInDb = _context.Tbl_AirImport_sub_job.Where(x => x.AirImport_Sub_job_id == InvoiceInDb.AirExportSubJobId).ToList();
                        package = HawbInDb.Sum(x => x.AirImport_Package_Count) + " " + HawbInDb[0].AirImport_Package_Type;
                        gross_chargeable = HawbInDb.Sum(x => x.AirImport_Gross_Weight) + " / " + HawbInDb.Sum(x => x.AirImport_Chargeable_Weight);
                    }
                    else
                    {
                        var MawbInDb = _context.Tbl_AirImport_sub_job.Where(x => x.AirImport_Master_job_id == InvoiceInDb.AirExportMaster_Job_id).ToList();
                        package = MawbInDb.Sum(x => x.AirImport_Package_Count) + " " + MawbInDb[0].AirImport_Package_Type;
                        gross_chargeable = MawbInDb.Sum(x => x.AirImport_Gross_Weight) + " / " + MawbInDb.Sum(x => x.AirImport_Chargeable_Weight);
                    }



                    var exchangerate = InvoiceInDb.billing_currency + " / " + string.Format("{0:0.00}", InvoiceInDb.billing_exchrate);
                    var Rno = _context.Tbl_AirImport_Arrival.Where(x => x.airimport_subjobid == subjobid).Select(x => x.rotation_no).FirstOrDefault();
                    var IgmNo = _context.Tbl_AirImport_Arrival.Where(x => x.airimport_subjobid == subjobid).Select(x => x.igm_no).FirstOrDefault();
                    var igmNo_Rono = IgmNo + " / " + Rno;

                    var flight_no = _context.Tbl_AirImport_Arrival.Where(x => x.airimport_subjobid == subjobid).Select(x => x.flight_no).FirstOrDefault();
                    var flight_date = string.Format("{0:dd-MM-yyyy}", _context.Tbl_AirImport_Arrival.Where(x => x.airimport_subjobid == subjobid).Select(x => x.flight_date).FirstOrDefault());
                    var flightNo_Date = flight_no + " / " + flight_date;

                    var origin = _context.tbl_airport_master.SingleOrDefault(x => x.airport_no == subjobInDb.AirImport_origin).airport_name.Split('?')[0];
                    var Destination = _context.tbl_airport_master.SingleOrDefault(x => x.airport_no == subjobInDb.AirImport_Destination).airport_name.Split('?')[0];
                    var masterjobNo = _context.Tbl_AirImport_master_job.SingleOrDefault(x => x.AirImport_Master_job_id == subjobInDb.AirImport_Master_job_id).AirImport_Master_job_no;
                    var subjobNo = subjobInDb.AirImport_Sub_job_no;
                    var jobno = subjobNo;


                    var userbranch = segment + 4;
                    var userstate = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == userbranch).Select(x => x.tbl_state_master.state_name).FirstOrDefault();


                    var description = subjobInDb.AirImport_Self_Details;
                    var billingid = InvoiceInDb.AirExportBilling_id;
                    var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == billingid).ToList();
                    var rocount = 0; var charge_name = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
                    var cgstrate = ""; var sgstrate = ""; var igstrate = "";
                    var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
                    for (int i = 0; i < chargeslist.Count; i++)
                    {
                        if (i > 0)
                        {
                            charge_name = charge_name + ",";
                            hsnno = hsnno + ",";
                            cgst = cgst + ",";
                            sgst = sgst + ",";
                            igst = igst + ",";
                            Amount = Amount + ",";
                            cgstrate = cgstrate + ",";
                            sgstrate = sgstrate + ",";
                            igstrate = igstrate + ",";
                        }
                        rocount += 1;
                        var charge_no = chargeslist[i].AirExportCharge_Code;
                        var hsncode = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no).charge_sac;
                        hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsncode).sac_code;
                        hsnno = hsnno.Replace("SAC ", "");
                        charge_name = charge_name + chargeslist[i].AirExportCharge_Name + " " + chargeslist[i].billing_charges_remark;
                        Amount = Amount + string.Format("{0:0.00}", (chargeslist[i].AirExportCharge_Amount));
                        if (statename.ToUpper() == userstate.ToUpper())
                        {
                            var charge_master = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no);

                            var state_master = _context.tbl_state_master.SingleOrDefault(x => x.state_name.ToLower() == statename.ToLower());

                            int state_no = state_master.state_no;
                            var sac_no = (int)charge_master.charge_sac;
                            NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
                            CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                            SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                            double cgst1 = Convert.ToDouble(chargeslist[i].AirExportCharge_Amount) * Convert.ToDouble(charge_master.charge_cgst) / 100;
                            double sgst1 = Convert.ToDouble(chargeslist[i].AirExportCharge_Amount) * Convert.ToDouble(charge_master.charge_sgst) / 100;
                            cgst = cgst + string.Format("{0:0.00}", (cgst1)); // Convert.ToDouble(cgst1);
                            sgst = sgst + string.Format("{0:0.00}", (sgst1)); //Convert.ToDouble(sgst1);
                            cgstrate = cgstrate + string.Format("{0:0.00}", (charge_master.charge_cgst));
                            sgstrate = sgstrate + string.Format("{0:0.00}", (charge_master.charge_sgst));
                        }
                        else
                        {

                            var charge_master = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no);
                            var state_master = _context.tbl_state_master.SingleOrDefault(x => x.state_name.ToLower() == statename.ToLower());
                            if (state_master != null)
                                state_id = state_master.state_no;
                            else
                                state_id = 0;
                            if (state_id == 0)
                            {
                                state_id = 3;
                            }
                            int state_no = (int)state_id;
                            var sac_no = (int)charge_master.charge_sac;

                            NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
                            IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                            double igst2 = Convert.ToDouble(chargeslist[i].AirExportCharge_Amount) * Convert.ToDouble(charge_master.charge_igst) / 100;
                            var igsttax = string.Format("{0:0.00}", Convert.ToDouble(igst2));
                            igst = igst + igsttax;
                            igstrate = igstrate + string.Format("{0:0.00}", Convert.ToDouble(charge_master.charge_igst));
                        }
                    }
                    var finalNetamt = string.Format("{0:0.00}", NetAmount);
                    var netamt = "";
                    if (statename.ToUpper() == userstate.ToUpper())
                    {
                        var totval = NetAmount + CGSTAmt + SGSTAmt;
                        netamt = string.Format("{0:0.00}", totval);
                    }
                    else
                    {
                        netamt = string.Format("{0:0.00}", NetAmount + IGSTAmt);
                    }

                    var netamt1 = netamt.ToString().Split('.')[1];
                    var finalroundof = "";
                    if (Convert.ToDecimal(netamt1) >= 50)
                    {
                        InvoiceAmt = Convert.ToDouble(netamt.ToString().Split('.')[0]) + 1;
                        Round_off = 100 - Convert.ToDouble(netamt1);
                        if (Round_off > 9)
                        {
                            finalroundof = "0." + Round_off.ToString();
                        }
                        else
                        {
                            finalroundof = "0.0" + Round_off.ToString();
                        }
                    }
                    else
                    {
                        InvoiceAmt = Convert.ToDouble(netamt.ToString().Split('.')[0]);
                        Round_off = Convert.ToDouble(netamt.ToString().Split('.')[1]);
                        if (Round_off > 9)
                        {
                            finalroundof = "-0." + Round_off.ToString();
                        }
                        else
                        {
                            finalroundof = "-0.0" + Round_off.ToString();
                        }
                    }

                    var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
                    var NumToWord = commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));


                    if (InvoiceInDb.billing_currency == "INR")
                    {
                        NumToWord = "Rupees " + NumToWord + " only";
                    }
                    else if (InvoiceInDb.billing_currency == "USD")
                    {
                        NumToWord = "USD " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                        finalInvoiceAmt = finalNetamt;
                 
                        var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                        NumToWord = NumToWord + " and " + paiseonly + " cents";
                        finalroundof = "0.00";
                    }
                    else if (InvoiceInDb.billing_currency == "EUR")
                    {
                        NumToWord = "EURO " +commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                        finalInvoiceAmt = finalNetamt;
                       
                        var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                        NumToWord = NumToWord + " and " + paiseonly + " cents";
                        finalroundof = "0.00";
                    }
                    else if (InvoiceInDb.billing_currency == "GBP")
                    {
                        NumToWord = "GBP " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                        finalInvoiceAmt = finalNetamt;
             
                        var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                        NumToWord = NumToWord + " and " + paiseonly + " Pence";
                        finalroundof = "0.00";
                    }
                    else
                    {
                        NumToWord = "Rupees " + NumToWord + " only";
                    }

                    var drafedorapprover = "";
                    var approvedornot = "";
                    if (InvoiceInDb.billing_Approval == "yes")
                    {
                        var approvebyid = _context.tbl_user_master.Where(x => x.user_no == InvoiceInDb.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                        var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                        drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd/MM/yyyy hh:mm tt}", InvoiceInDb.billing_Approval_on);
                        approvedornot = "yes";
                    }
                    else
                    {
                        drafedorapprover = "DRAFT BILL";
                        approvedornot = "no";
                    }

                    var Invoice_type = InvoiceInDb.invoice_type;

                    string jsonString = consinee_name + "|" + consinee_addr + "|" + notify + "|" + shipper_name + "|" + InvoiceNo + "|" + InvoiceDt + "|" + mawbno_date + "|" + hawbno_date + "|" + package + "|" + gross_chargeable + "|" + exchangerate + "|" + igmNo_Rono + "|" + flightNo_Date + "|" + origin + "|" + Destination + "|" + jobno + "|" + description + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt + "|" + SGSTAmt + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + subjobInDb.AirImport_Sub_job_no + "|" + statename + "|" + cgstrate + "|" + sgstrate + "|" + drafedorapprover + "|" + approvedornot + "|" + igst + "|" + IGSTAmt + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + irnno + "|" + filename + "|" + pos + "|" + gstno + "|" + Invoice_type + "|" + shippername + "|" + remark;

                    ViewBag.CanPrint = jsonString;
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter _testData = new StreamWriter(Server.MapPath("~/App_Data/errors/AirImpMAWB.txt"), true))
                {
                    _testData.WriteLine("Message: " + ex.Message + "<br/>" + Environment.NewLine + "StackTrace: " + ex.StackTrace + "<br/>" + Environment.NewLine + "DateTime: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt").Replace('-', '/') +
                        "<br/>" + Environment.NewLine + "<br/>" + Environment.NewLine); // Write the file.
                    _testData.Dispose();
                }

                id = encryptDecrypt.EncryptString(mawbid.ToString());
                return RedirectToAction("Index");
            }

            return View();

        }
        public ActionResult SEC_PDF(string mid)
        {
            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var decryptedid = encryptDecrypt.DecryptString(mid);
            int id = Convert.ToInt32(decryptedid);
            var invoiceDB = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id == id).FirstOrDefault();
            var billingpartyname1 = _context.tbl_company_business_unit.Where(x => x.unit_name.ToUpper() == invoiceDB.AirExportBilling_party.ToUpper()).FirstOrDefault();
            var billingparty = invoiceDB.AirExportBilling_party;
            var billingpartyaddress = invoiceDB.AirExportBillingParty_address + "<br>" + billingpartyname1.city_id_text_box + " - " + billingpartyname1.unit_pincode;

            var billremark = invoiceDB.AirExportRemarks;
            if (billremark != "" && billremark != null)
            {
                billremark = billremark.Replace("\n", "<br>");
                billremark = billremark.Replace("  ", "&nbsp;&nbsp;");
            }

            var subjobDB = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaClearance_Subjob_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
            var shippername = "";
            if (subjobDB.SeaClearance_Reference_type != 0)
            {
                var refcmpny = subjobDB.SeaClearance_Shipper_company;
                var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == refcmpny).Select(x => x.unit_name).FirstOrDefault();
                if (unitname.Length > 30)
                    shippername = unitname.Substring(0, 30);
                else
                    shippername = unitname;
            }

            var sbnodate = subjobDB.shipping_bill_no + " / " + string.Format("{0:dd-MM-yyyy}", subjobDB.shipping_bill_date);

            var businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_name.ToLower() == billingparty.ToLower()).FirstOrDefault();
            if (businessuitDB == null)
            {
                businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == subjobDB.SeaClearance_Shipper_company).FirstOrDefault();
            }
            var country = businessuitDB.tbl_country_master.country_name;
            var pos = "";
            if (country.ToLower() == "india")
            {
                var statename = businessuitDB.tbl_state_master.state_name;
                var stateno = businessuitDB.tbl_state_master.state_GST_id;
                pos = stateno + "-" + statename;
            }
            var gstno = businessuitDB.unit_gst_no;
            var panno = businessuitDB.unit_pan_no;


            var segment = subjobDB.SEC_segment_no;
            var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
            switch (segment)
            {
                case 0:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    break;
                case 1:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                    break;
                case 2:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                    break;
            }
            var branchaddr = branchInDb.ryal_branch_address;
            if (!string.IsNullOrEmpty(branchaddr))
            {
                branchaddr = branchaddr.Replace("\n", "<br>");
            }
            var ourgstin = branchInDb.ryal_branch_GST_no;
            var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;

            var accountname = "";
            if (subjobDB.SeaClearance_Reference_type != 0)
            {
                var accountid = subjobDB.SeaClearance_Shipper_company;
                accountname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == accountid).Select(x => x.unit_name).FirstOrDefault();
            }

            var masterjobid = _context.Tbl_SeaClearance_subjob.Where(x => x.SeaClearance_Subjob_id == invoiceDB.AirExportSubJobId).Select(x => x.SeaExport_Masterjob_id).FirstOrDefault();
            var invoiceno = invoiceDB.AirExportInvoice_No;
            var invoicedate = string.Format("{0:dd-MM-yyyy}", invoiceDB.AirExportInvoice_date);
            var subjobno = invoiceDB.AirExportSubJobNo;
            var masterjobno = invoiceDB.AirExportMasterJobNo;
            var package = subjobDB.SeaClearance_Total_packages + " " + subjobDB.SeaClearance_Packaging_type;
            var grosswt = subjobDB.SeaClearance_Total_weight;

            var subjobid = invoiceDB.AirExportSubJobId;
            var originid = _context.Tbl_SeaClearance_subjob.Where(s => s.SeaClearance_Subjob_id == subjobid).Select(x => x.SeaClearance_Destination_seaport_id).FirstOrDefault();
            var shipperref = "";
            var Description = "";
            var originname = _context.tbl_seaport_master.Where(x => x.seaport_no == originid).Select(x => x.seaport_name).FirstOrDefault();


            var irnno = invoiceDB.EInvoice_IrnNo;
            if (irnno == null)
            {
                irnno = "";
            }
            var filename = invoiceDB.Qr_code_file_name;
            if (filename == null)
            {
                filename = "";
            }

            var currency = invoiceDB.billing_currency + " / " + invoiceDB.billing_exchrate;
            var Mainremark = invoiceDB.AirExportRemarks;

            var rocount = 0; var charge_name = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
            var cgstrate = ""; var sgstrate = ""; var igstrate = "";
            var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
            var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == invoiceDB.AirExportBilling_id).ToList();
            for (int i = 0; i < chargeslist.Count(); i++)
            {
                if (i > 0)
                {
                    charge_name = charge_name + ",";
                    hsnno = hsnno + ",";
                    cgst = cgst + ",";
                    sgst = sgst + ",";
                    igst = igst + ",";
                    Amount = Amount + ",";
                    cgstrate = cgstrate + ",";
                    sgstrate = sgstrate + ",";
                    igstrate = igstrate + ",";
                }
                var cgstrate1 = "";
                var sgstrate1 = "";
                var igstrate1 = "";
                rocount += 1;
                var charge_no = chargeslist[i].AirExportCharge_Code;
                charge_name = charge_name + _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_name).FirstOrDefault();
                cgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_cgst.ToString()).FirstOrDefault();
                sgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_sgst.ToString()).FirstOrDefault();
                igstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_igst.ToString()).FirstOrDefault();

                if (string.IsNullOrWhiteSpace(sgstrate1))
                {
                    sgstrate1 = "0 ";
                    igstrate1 = "0 ";
                    cgstrate1 = "0 ";
                }

                sgstrate = sgstrate + sgstrate1;
                igstrate = igstrate + igstrate1;
                cgstrate = cgstrate + cgstrate1;

                var charge_remark = chargeslist[i].billing_charges_remark;
                if (!string.IsNullOrEmpty(charge_remark))
                {
                    charge_name += " ( " + charge_remark + " )";
                }
                var hsnid = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code.ToLower().Trim() == charge_no.ToLower().Trim()).charge_sac;

                hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsnid).sac_code.Split(' ')[1];
                cgst = cgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_CGST);
                CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                sgst = sgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_SGST);
                SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                igst = igst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_IGST);
                IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                Amount = Amount + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_Amount);
                NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
            }

            var finalNetamt = string.Format("{0:0.00}", NetAmount);
            var netamt = NetAmount + CGSTAmt + SGSTAmt + IGSTAmt;
            var netamt2 = string.Format("{0:0.00}", netamt);

            var netamt1 = netamt2.ToString().Split('.')[1];
            var finalroundof = "";
            if (Convert.ToDecimal(netamt1) >= 50)
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]) + 1;
                Round_off = 100 - Convert.ToDouble(netamt1);
                if (Round_off > 9)
                    finalroundof = "0." + Round_off.ToString();
                else
                    finalroundof = "0.0" + Round_off.ToString();
            }
            else
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]);
                Round_off = Convert.ToDouble(netamt2.ToString().Split('.')[1]);
                if (Round_off > 0)
                    if (Round_off > 9)
                        finalroundof = "-0." + Round_off.ToString();
                    else
                        finalroundof = "-0.0" + Round_off.ToString();
                else
                    finalroundof = "0." + Round_off.ToString();
            }
            var CGSTAmt1 = string.Format("{0:0.00}", CGSTAmt);
            var SGSTAmt1 = string.Format("{0:0.00}", SGSTAmt);
            var IGSTAmt1 = string.Format("{0:0.00}", IGSTAmt);


            var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
            var NumToWord =commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));

            if (invoiceDB.billing_currency == "INR")
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }
            else if (invoiceDB.billing_currency == "USD")
            {
                NumToWord = "USD " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "EUR")
            {
                NumToWord = "EURO " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "GBP")
            {
                NumToWord = "GBP " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " Pence";
                finalroundof = "0.00";
            }
            else
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }


            var drafedorapprover = "";
            var approvedornot = "";
            if (invoiceDB.billing_Approval == "yes")
            {
                var approvebyid = _context.tbl_user_master.Where(x => x.user_no == invoiceDB.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd/MM/yyyy hh:mm tt}", invoiceDB.billing_Approval_on);
                approvedornot = "yes";
            }
            else
            {
                drafedorapprover = "DRAFT BILL";
                approvedornot = "no";
            }


            var Invoice_type = invoiceDB.invoice_type;

            string jsonString = billingparty + "|" + billingpartyaddress + "|" + pos + "|" + gstno + "|" + panno + "|" + accountname + "|" + Description + "|" + "" + "|" + invoiceno + "|" + invoicedate + "|" + originname + "|" + "" + "|" + "" + "|" + "" + "|" + currency + "|" + masterjobno + "|" + subjobno + "|" + "" + "|" + package + "|" + grosswt + "|" + shipperref + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + igst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt1 + "|" + SGSTAmt1 + "|" + IGSTAmt1 + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + "" + "|" + billremark + "|" + sbnodate + "|" + Mainremark + "|" + drafedorapprover + "|" + approvedornot + "|" + cgstrate + "|" + sgstrate + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + irnno + "|" + filename + "|" + Invoice_type + "|" + shippername;

            ViewBag.InvoicePrint = jsonString;

        
            return View();

        }
        public ActionResult SEF_PDF(string mid)
        {
            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var decryptedid = encryptDecrypt.DecryptString(mid);
            int id = Convert.ToInt32(decryptedid);

            var invoiceDB = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id == id).FirstOrDefault();
            var billingpartyname1 = _context.tbl_company_business_unit.Where(x => x.unit_name.ToUpper() == invoiceDB.AirExportBilling_party.ToUpper()).FirstOrDefault();
            var billingparty = invoiceDB.AirExportBilling_party;
            var billingpartyaddress = invoiceDB.AirExportBillingParty_address + "<br>" + billingpartyname1.city_id_text_box + " - " + billingpartyname1.unit_pincode;

            var billremark = invoiceDB.AirExportRemarks;
            if (billremark != "" && billremark != null)
            {
                billremark = billremark.Replace("\n", "<br>");
                billremark = billremark.Replace("  ", "&nbsp;&nbsp;");
            }

            var subjobDB = _context.Tbl_Shiping_sub_job.Where(x => x.shipping_Sub_job_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
            var shippername = "";
            if (subjobDB.shipping_Reference_type != 0)
            {
                var refcmpny = subjobDB.shipping_Shipper_company;
                var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == refcmpny).Select(x => x.unit_name).FirstOrDefault();
                if (unitname.Length > 30)
                    shippername = unitname.Substring(0, 30);
                else
                    shippername = unitname;
            }
            var businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_name.ToLower() == billingparty.ToLower()).FirstOrDefault();
            if (businessuitDB == null)
            {
                businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == subjobDB.shipping_Shipper_company).FirstOrDefault();
            }
            var country = businessuitDB.tbl_country_master.country_name;
            var pos = "";
            if (country.ToLower() == "india")
            {
                var statename = businessuitDB.tbl_state_master.state_name;
                var stateno = businessuitDB.tbl_state_master.state_GST_id;
                pos = stateno + "-" + statename;
            }
            var gstno = businessuitDB.unit_gst_no;
            var panno = businessuitDB.unit_pan_no;


            var segment = subjobDB.SEF_segment_no;
            var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
            switch (segment)
            {
                case 0:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    break;
                case 1:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                    break;
                case 2:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                    break;
            }
            var branchaddr = branchInDb.ryal_branch_address;
            if (!string.IsNullOrEmpty(branchaddr))
            {
                branchaddr = branchaddr.Replace("\n", "<br>");
            }
            var ourgstin = branchInDb.ryal_branch_GST_no;
            var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;


            var accountname = "";
            if (subjobDB.shipping_Reference_type != 0)
            {
                var accountid = subjobDB.shipping_Shipper_company;
                accountname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == accountid).Select(x => x.unit_name).FirstOrDefault();
            }

            var volume = "";
            if (subjobDB.shipping_shipment_type == "FCL")
            {
                var shipmentdetails = _context.tbl_shipping_Shipment.Where(x => x.Shipping_shipment_M_id == subjobDB.shipping_Sub_job_id).ToList();
                for (int i = 0; i < shipmentdetails.Count(); i++)
                {
                    if (i > 0)
                    {
                        volume += " / ";
                    }
                    volume += shipmentdetails[i].Shipping_shipment_qty + " X " + shipmentdetails[i].Shipping_shipment_type;
                }
            }
            else
            {
                var masterjobid1 = _context.Tbl_Shiping_sub_job.Where(x => x.shipping_Sub_job_id == invoiceDB.AirExportSubJobId).Select(x => x.shipping_Master_job_id).FirstOrDefault();
                var masterjobtype1 = _context.Tbl_shipping_master_job.Where(x => x.shipping_Master_job_id == masterjobid1).Select(x => x.shipping_Master_job_type).FirstOrDefault();
                if (masterjobtype1.ToLower() == "direct")
                {
                    volume = _context.tbl_mbl_details.Where(x => x.MBL_Master_Job_id == masterjobid1).Select(x => x.MBL_Volume).FirstOrDefault() + " CBM";
                }
                else
                {
                    volume = _context.tbl_hbl_details.Where(x => x.HBL_sub_job_id == invoiceDB.AirExportSubJobId).Select(x => x.HBL_Volume).FirstOrDefault() + " CBM";
                }
            }


            var container = "";
            var sbno_date = "";
            var masterjobid = _context.Tbl_Shiping_sub_job.Where(x => x.shipping_Sub_job_id == invoiceDB.AirExportSubJobId).Select(x => x.shipping_Master_job_id).FirstOrDefault();
            var masterjobtype = _context.Tbl_shipping_master_job.Where(x => x.shipping_Master_job_id == masterjobid).Select(x => x.shipping_Master_job_type).FirstOrDefault();
            if (masterjobtype.ToLower() == "direct")
            {
                var mblid = _context.tbl_mbl_details.Where(x => x.MBL_Master_Job_id == masterjobid).FirstOrDefault();
                var containerlist = _context.tbl_mbl_container.Where(x => x.MBL_M_Id == mblid.MBL_id).ToList();
                for (int i = 0; i < containerlist.Count(); i++)
                {
                    if (i > 0)
                    {
                        container += " / ";
                    }
                    container += containerlist[i].MBL_Container_No;
                }
                sbno_date = mblid.MBL_Shipping_Bill_No + " / " + string.Format("{0:dd-MM-yyyy}", mblid.MBL_Shipping_Bill_Date);
            }
            else
            {
                var hblid = _context.tbl_hbl_details.Where(x => x.HBL_sub_job_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
                var containerlist = _context.tbl_hbl_container.Where(x => x.HBL_M_Id == hblid.HBL_id).ToList();
                for (int i = 0; i < containerlist.Count(); i++)
                {
                    if (i > 0)
                    {
                        container += " / ";
                    }
                    container += containerlist[i].HBL_Container_No;
                }
                sbno_date = hblid.HBL_Shipping_Bill_No + " / " + string.Format("{0:dd-MM-yyyy}", hblid.HBL_Shipping_Bill_Date);
            }


            var irnno = invoiceDB.EInvoice_IrnNo;
            if (irnno == null)
            {
                irnno = "";
            }
            var filename = invoiceDB.Qr_code_file_name;
            if (filename == null)
            {
                filename = "";
            }



            var package = "";// subjobDB.shipping_Total_packages + " " + subjobDB.shipping_Packaging_type;
            var grosswt = "";// subjobDB.shipping_Total_weight;// + " / " + subjobDB.;

            var mjobid = invoiceDB.AirExportMaster_Job_id;
            var MblDB = _context.tbl_mbl_details.Where(x => x.MBL_Master_Job_id == mjobid).FirstOrDefault();

            var subjobid = _context.tbl_hbl_details.Where(x => x.HBL_sub_job_id == invoiceDB.AirExportSubJobId).ToList();
            var hawbno = ""; var hawbdate = ""; var destination = ""; var expno = "";
            if (subjobid.Count == 0)
            {
                destination = MblDB.tbl_seaport_master1.seaport_name;
                expno = MblDB.MBL_exp_invoice_no;

            }
            else
            {
                var hblInDb = _context.tbl_hbl_details.Where(x => x.HBL_sub_job_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
                hawbno = hblInDb.HBL_BLNumber;
                hawbdate = string.Format("{0:dd-MM-yyyy}", hblInDb.HBL_DateOfIssue);
                destination = hblInDb.tbl_seaport_master1.seaport_name;
                expno = hblInDb.HBL_exp_invoice_no;
            }



            if (invoiceDB.billing_Against == "hawb" || invoiceDB.billing_Against == null)
            {
                var jobtype = _context.Tbl_shipping_master_job.FirstOrDefault(x => x.shipping_Master_job_id == masterjobid).shipping_Master_job_type;
                if (jobtype == "Direct")
                {
                    var MawbInDb = _context.tbl_mbl_details.Where(x => x.MBL_Master_Job_id == masterjobid).FirstOrDefault();
                    package = MawbInDb.MBL_Package_Count + " " + MawbInDb.MBL_Package_Type;
                    grosswt = MawbInDb.MBL_GrossWt.ToString();
                }
                else
                {
                    var HawbInDb = _context.tbl_hbl_details.FirstOrDefault(x => x.HBL_sub_job_id == invoiceDB.AirExportSubJobId);
                    package = HawbInDb.HBL_Package_Count + " " + HawbInDb.HBL_Package_Type;
                    grosswt = HawbInDb.HBL_GrossWt.ToString();
                }
            }
            else
            {
                var MawbInDb = _context.tbl_hbl_details.Where(x => x.HBL_sub_job_id == invoiceDB.AirExportSubJobId).ToList();

                package = MawbInDb.Sum(x => x.HBL_Package_Count) + " " + MawbInDb[0].HBL_Package_Type;
                grosswt = MawbInDb.Sum(x => x.HBL_GrossWt).ToString();
            }

            //var airlineid = "";
            var airlinename = "";

            var description = MblDB.MBL_Goods_Description;
            if (!string.IsNullOrEmpty(description))
            {
                description = description.Replace('\n', ' ');
            }
            var invoiceno = invoiceDB.AirExportInvoice_No;
            var invoicedate = string.Format("{0:dd-MM-yyyy}", invoiceDB.AirExportInvoice_date);
            var mawbno = MblDB.MBL_BLNumber;
            var mawbdate = string.Format("{0:dd-MM-yyyy}", MblDB.MBL_DateOfIssue);
            var subjobno = invoiceDB.AirExportSubJobNo;
            var masterjobno = invoiceDB.AirExportMasterJobNo;
            var currency = invoiceDB.billing_currency + " / " + invoiceDB.billing_exchrate;
            var Mainremark = invoiceDB.AirExportRemarks;

            var rocount = 0; var charge_name = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
            var cgstrate = ""; var sgstrate = ""; var igstrate = "";
            var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
            var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == invoiceDB.AirExportBilling_id).ToList();
            for (int i = 0; i < chargeslist.Count(); i++)
            {
                if (i > 0)
                {
                    charge_name = charge_name + ",";
                    hsnno = hsnno + ",";
                    cgst = cgst + ",";
                    sgst = sgst + ",";
                    igst = igst + ",";
                    Amount = Amount + ",";
                    cgstrate = cgstrate + ",";
                    sgstrate = sgstrate + ",";
                    igstrate = igstrate + ",";
                }
                var cgstrate1 = "";
                var sgstrate1 = "";
                var igstrate1 = "";
                rocount += 1;
                var charge_no = chargeslist[i].AirExportCharge_Code;
                charge_name = charge_name + _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_name).FirstOrDefault();
                cgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_cgst.ToString()).FirstOrDefault();
                sgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_sgst.ToString()).FirstOrDefault();
                igstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_igst.ToString()).FirstOrDefault();

                if (string.IsNullOrWhiteSpace(sgstrate1))
                {
                    sgstrate1 = "0 ";
                    igstrate1 = "0 ";
                    cgstrate1 = "0 ";
                }

                sgstrate = sgstrate + sgstrate1;
                igstrate = igstrate + igstrate1;
                cgstrate = cgstrate + cgstrate1;

                var charge_remark = chargeslist[i].billing_charges_remark;
                if (!string.IsNullOrEmpty(charge_remark))
                {
                    charge_name += " ( " + charge_remark + " )";
                }
                var hsnid = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no).charge_sac;

                hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsnid).sac_code.Split(' ')[1];
                cgst = cgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_CGST);
                CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                sgst = sgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_SGST);
                SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                igst = igst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_IGST);
                IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                Amount = Amount + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_Amount);
                NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
            }

            var finalNetamt = string.Format("{0:0.00}", NetAmount);
            var netamt = NetAmount + CGSTAmt + SGSTAmt + IGSTAmt;
            var netamt2 = string.Format("{0:0.00}", netamt);

            var netamt1 = netamt2.ToString().Split('.')[1];
            var finalroundof = "";
            if (Convert.ToDecimal(netamt1) >= 50)
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]) + 1;
                Round_off = 100 - Convert.ToDouble(netamt1);
                if (Round_off > 9)
                    finalroundof = "0." + Round_off.ToString();
                else
                    finalroundof = "0.0" + Round_off.ToString();
            }
            else
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]);
                Round_off = Convert.ToDouble(netamt2.ToString().Split('.')[1]);
                if (Round_off > 0)
                    if (Round_off > 9)
                        finalroundof = "-0." + Round_off.ToString();
                    else
                        finalroundof = "-0.0" + Round_off.ToString();
                else
                    finalroundof = "0." + Round_off.ToString();
            }
            var CGSTAmt1 = string.Format("{0:0.00}", CGSTAmt);
            var SGSTAmt1 = string.Format("{0:0.00}", SGSTAmt);
            var IGSTAmt1 = string.Format("{0:0.00}", IGSTAmt);


            var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
            var NumToWord =commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));

            if (invoiceDB.billing_currency == "INR")
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }
            else if (invoiceDB.billing_currency == "USD")
            {
                NumToWord = "USD " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "EUR")
            {
                NumToWord = "EURO " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "GBP")
            {
                NumToWord = "GBP " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " Pence";
                finalroundof = "0.00";
            }
            else
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }

            var drafedorapprover = "";
            var approvedornot = "";
            if (invoiceDB.billing_Approval == "yes")
            {
                var approvebyid = _context.tbl_user_master.Where(x => x.user_no == invoiceDB.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd/MM/yyyy hh:mm tt}", invoiceDB.billing_Approval_on);
                approvedornot = "yes";
            }
            else
            {
                drafedorapprover = "DRAFT BILL";
                approvedornot = "no";
            }


            var Invoice_type = invoiceDB.invoice_type;

            string jsonString = billingparty + "|" + billingpartyaddress + "|" + pos + "|" + gstno + "|" + panno + "|" + accountname + "|" + airlinename + "|" + container + "|" + invoiceno + "|" + invoicedate + "|" + mawbno + "|" + mawbdate + "|" + hawbno + "|" + hawbdate + "|" + currency + "|" + masterjobno + "|" + subjobno + "|" + destination + "|" + package + "|" + grosswt + "|" + expno + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + igst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt1 + "|" + SGSTAmt1 + "|" + IGSTAmt1 + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + volume + "|" + billremark + "|" + sbno_date + "|" + Mainremark + "|" + drafedorapprover + "|" + approvedornot + "|" + cgstrate + "|" + sgstrate + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + irnno + "|" + filename + "|" + Invoice_type + "|" + shippername;

            ViewBag.InvoicePrint = jsonString;
            return View();
        }
        public ActionResult SIC_PDF(string mid)
        {
            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var decryptedid = encryptDecrypt.DecryptString(mid);
            int id = Convert.ToInt32(decryptedid);
            var invoiceDB = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id == id).FirstOrDefault();
            var billingparty = invoiceDB.AirExportBilling_party;
            var billingpartyaddress = invoiceDB.AirExportBillingParty_address;
            var BillingInDb = _context.tbl_company_business_unit.FirstOrDefault(x => x.unit_name.ToUpper() == billingparty.ToUpper());
            if (BillingInDb != null)
            {
                billingpartyaddress += "<br>" + BillingInDb.city_id_text_box + " - " + BillingInDb.unit_pincode;
            }

            var billremark = invoiceDB.AirExportRemarks;
            if (billremark != "" && billremark != null)
            {
                billremark = billremark.Replace("\n", "<br>");
                billremark = billremark.Replace("  ", "&nbsp;&nbsp;");
            }

            var subjobDB = _context.Tbl_SeaImpClearance_subjob.Where(x => x.SeaImpClearance_Subjob_id == invoiceDB.AirExportSubJobId).FirstOrDefault();
            var Consigneename = "";
            var shippername = "";
            if (subjobDB.SeaImpClearance_Reference_type != 0)
            {
                var refcmpny = subjobDB.SeaImpClearance_Shipper_company;
                var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == refcmpny).Select(x => x.unit_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(unitname))
                {
                    if (unitname.Length > 30)
                        Consigneename = unitname.Substring(0, 30);
                    else
                        Consigneename = unitname;
                }
            }
            else
            {
                shippername = subjobDB.SeaImpClearance_Consignee_company;
            }
            var businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_name.ToLower() == billingparty.ToLower()).FirstOrDefault();
            if (businessuitDB == null)
            {
                businessuitDB = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == subjobDB.SeaImpClearance_Shipper_company).FirstOrDefault();
            }
            var country = businessuitDB.tbl_country_master.country_name;
            var pos = "";
            if (country.ToLower() == "india")
            {
                var statename = businessuitDB.tbl_state_master.state_name;
                var stateno = businessuitDB.tbl_state_master.state_GST_id;
                pos = stateno + "-" + statename;
            }
            var gstno = businessuitDB.unit_gst_no;
            var panno = businessuitDB.unit_pan_no;

            var segment = subjobDB.SIC_segment_no;
            var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
            switch (segment)
            {
                case 0:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    break;
                case 1:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                    break;
                case 2:
                    branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                    break;
            }
            var branchaddr = branchInDb.ryal_branch_address;
            if (!string.IsNullOrEmpty(branchaddr))
            {
                branchaddr = branchaddr.Replace("\n", "<br>");
            }
            var ourgstin = branchInDb.ryal_branch_GST_no;
            var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;



            var accountname = "";
            if (subjobDB.SeaImpClearance_Reference_type != 0)
            {
                var accountid = subjobDB.SeaImpClearance_Shipper_company;
                accountname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == accountid).Select(x => x.unit_name).FirstOrDefault();
            }

            var masterjobid = _context.Tbl_SeaImpClearance_subjob.Where(x => x.SeaImpClearance_Subjob_id == invoiceDB.AirExportSubJobId).Select(x => x.SeaImpImport_Masterjob_id).FirstOrDefault();


            var invoiceno = invoiceDB.AirExportInvoice_No;
            var invoicedate = string.Format("{0:dd-MM-yyyy}", invoiceDB.AirExportInvoice_date);
            var subjobno = invoiceDB.AirExportSubJobNo;
            var masterjobno = invoiceDB.AirExportMasterJobNo;
            var package = subjobDB.SeaImpClearance_Total_packages + " " + subjobDB.SeaImpClearance_Packaging_type;
            var grosswt = subjobDB.SeaImpClearance_Total_weight;// + " / " + subjobDB.;
            var mblno = subjobDB.SeaImpClearance_mbl;
            var hblno = subjobDB.SeaImpClearance_hbl;
            var bedate = string.Format("{0:dd-MM-yyyy}", subjobDB.billof_entry_date);
            var beno = subjobDB.billof_entry_no;
            var boenodate = beno + " / " + bedate;

            var subjobid = invoiceDB.AirExportSubJobId;
            var originid = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Subjob_id == subjobid).Select(x => x.SeaImpClearance_Origin_seaport_id).FirstOrDefault();
            var shipperref = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Subjob_id == subjobid).Select(x => x.SeaImpClearance_InvoiceNo).FirstOrDefault();
            var Description = _context.Tbl_SeaImpClearance_subjob.Where(s => s.SeaImpClearance_Subjob_id == subjobid).Select(x => x.SeaImpClearance_Description).FirstOrDefault();
            var originname = _context.tbl_seaport_master.Where(x => x.seaport_no == originid).Select(x => x.seaport_name).FirstOrDefault();


            var currency = invoiceDB.billing_currency + " / " + invoiceDB.billing_exchrate;
            var Mainremark = invoiceDB.AirExportRemarks;


            var irnno = invoiceDB.EInvoice_IrnNo;
            if (irnno == null)
            {
                irnno = "";
            }
            var filename = invoiceDB.Qr_code_file_name;
            if (filename == null)
            {
                filename = "";
            }

            var rocount = 0; var charge_name = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
            var cgstrate = ""; var sgstrate = ""; var igstrate = "";
            var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
            var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == invoiceDB.AirExportBilling_id).ToList();
            for (int i = 0; i < chargeslist.Count(); i++)
            {
                if (i > 0)
                {
                    charge_name = charge_name + ",";
                    hsnno = hsnno + ",";
                    cgst = cgst + ",";
                    sgst = sgst + ",";
                    igst = igst + ",";
                    Amount = Amount + ",";
                    cgstrate = cgstrate + ",";
                    sgstrate = sgstrate + ",";
                    igstrate = igstrate + ",";
                }
                var cgstrate1 = "";
                var sgstrate1 = "";
                var igstrate1 = "";
                rocount += 1;
                var charge_no = chargeslist[i].AirExportCharge_Code;
                charge_name = charge_name + _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_name).FirstOrDefault();
                cgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_cgst.ToString()).FirstOrDefault();
                sgstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_sgst.ToString()).FirstOrDefault();
                igstrate1 = _context.tbl_charge_master.Where(x => x.charge_code == charge_no).Select(x => x.charge_igst.ToString()).FirstOrDefault();


                if (string.IsNullOrWhiteSpace(sgstrate1))
                {
                    sgstrate1 = "0 ";
                    igstrate1 = "0 ";
                    cgstrate1 = "0 ";
                }


                sgstrate = sgstrate + sgstrate1;
                igstrate = igstrate + igstrate1;
                cgstrate = cgstrate + cgstrate1;

                var charge_remark = chargeslist[i].billing_charges_remark;
                if (!string.IsNullOrEmpty(charge_remark))
                {
                    charge_name += " ( " + charge_remark + " )";
                }
                var hsnid = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no).charge_sac;

                hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsnid).sac_code.Split(' ')[1];
                cgst = cgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_CGST);
                CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                sgst = sgst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_SGST);
                SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                igst = igst + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_IGST);
                IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                Amount = Amount + string.Format("{0:0.00}", chargeslist[i].AirExportCharge_Amount);
                NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
            }

            var finalNetamt = string.Format("{0:0.00}", NetAmount);
            var netamt = NetAmount + CGSTAmt + SGSTAmt + IGSTAmt;
            var netamt2 = string.Format("{0:0.00}", netamt);

            var netamt1 = netamt2.ToString().Split('.')[1];
            var finalroundof = "";
            if (Convert.ToDecimal(netamt1) >= 50)
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]) + 1;
                Round_off = 100 - Convert.ToDouble(netamt1);
                if (Round_off > 9)
                    finalroundof = "0." + Round_off.ToString();
                else
                    finalroundof = "0.0" + Round_off.ToString();
            }
            else
            {
                InvoiceAmt = Convert.ToDouble(netamt2.ToString().Split('.')[0]);
                Round_off = Convert.ToDouble(netamt2.ToString().Split('.')[1]);
                if (Round_off > 0)
                    if (Round_off > 9)
                        finalroundof = "-0." + Round_off.ToString();
                    else
                        finalroundof = "-0.0" + Round_off.ToString();
                else
                    finalroundof = "0." + Round_off.ToString();
            }
            var CGSTAmt1 = string.Format("{0:0.00}", CGSTAmt);
            var SGSTAmt1 = string.Format("{0:0.00}", SGSTAmt);
            var IGSTAmt1 = string.Format("{0:0.00}", IGSTAmt);


            var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
            var NumToWord = commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));

            if (invoiceDB.billing_currency == "INR")
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }
            else if (invoiceDB.billing_currency == "USD")
            {
                NumToWord = "USD " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "EUR")
            {
                NumToWord = "EURO " +commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";
                var paiseonly = commclass. NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " cents";
                finalroundof = "0.00";
            }
            else if (invoiceDB.billing_currency == "GBP")
            {
                NumToWord = "GBP " + commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[0]));
                finalInvoiceAmt = finalNetamt;
                IGSTAmt1 = "0.00";
                SGSTAmt1 = "0.00";
                CGSTAmt1 = "0.00";

                var paiseonly = commclass.NumberToWords(Convert.ToInt32(finalNetamt.Split('.')[1]));
                NumToWord = NumToWord + " and " + paiseonly + " Pence";
                finalroundof = "0.00";
            }
            else
            {
                NumToWord = "Rupees " + NumToWord + " only";
            }


            var drafedorapprover = "";
            var approvedornot = "";
            if (invoiceDB.billing_Approval == "yes")
            {
                var approvebyid = _context.tbl_user_master.Where(x => x.user_no == invoiceDB.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd/MM/yyyy hh:mm tt}", invoiceDB.billing_Approval_on);
                approvedornot = "yes";
            }
            else
            {
                drafedorapprover = "DRAFT BILL";
                approvedornot = "no";
            }

            var advanceamt = invoiceDB.billing_advance_amt;
            if (advanceamt == null)
            {
                advanceamt = 0;
            }

            var Invoice_type = invoiceDB.invoice_type;


            string jsonString = billingparty + "|" + billingpartyaddress + "|" + pos + "|" + gstno + "|" + panno + "|" + accountname + "|" + Description + "|" + "" + "|" + invoiceno + "|" + invoicedate + "|" + originname + "|" + mblno + "|" + "" + "|" + hblno + "|" + currency + "|" + masterjobno + "|" + subjobno + "|" + "" + "|" + package + "|" + grosswt + "|" + shipperref + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + igst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt1 + "|" + SGSTAmt1 + "|" + IGSTAmt1 + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + "" + "|" + billremark + "|" + boenodate + "|" + Mainremark + "|" + drafedorapprover + "|" + approvedornot + "|" + cgstrate + "|" + sgstrate + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + advanceamt + "|" + irnno + "|" + filename + "|" + Invoice_type + "|" + Consigneename + "|" + shippername;

            ViewBag.InvoicePrint = jsonString;

            return View();

        }
        public ActionResult SIF_PDF(string mid)
        {
            AESEncrypt_Decrypt encryptDecrypt = new AESEncrypt_Decrypt();
            var decryptedid = encryptDecrypt.DecryptString(mid);
            string id = decryptedid;
            var InvoiceInDb = _context.tbl_airexport_billing.Where(x => x.AirExportBilling_id.ToString() == id).FirstOrDefault();
            var mawbid = 0;
            var subjobid = InvoiceInDb.AirExportSubJobId;
            var remark = InvoiceInDb.AirExportRemarks;
            try
            {
                if (subjobid != 0)
                {
                    var subjobInDb = _context.Tbl_SeaImport_sub_job.SingleOrDefault(x => x.SeaImport_Sub_job_id == subjobid);
                    var shippername = "";
                    if (subjobInDb.SeaImport_Reference_type != 0)
                    {
                        var refcmpny = subjobInDb.SeaImport_Shipper_company;
                        var unitname = _context.tbl_company_business_unit.Where(x => x.unit_no.ToString() == refcmpny).Select(x => x.unit_name).FirstOrDefault();
                        if (unitname.Length > 30)
                            shippername = unitname.Substring(0, 30);
                        else
                            shippername = unitname;
                    }
                    mawbid = subjobInDb.SeaImport_Master_job_id;
                    var billingpartyname = _context.tbl_company_business_unit.Where(x => x.unit_name.ToUpper() == InvoiceInDb.AirExportBilling_party.ToUpper()).FirstOrDefault();

                    var consinee_name = InvoiceInDb.AirExportBilling_party;
                    var consinee_addr = InvoiceInDb.AirExportBillingParty_address + "<br>" + billingpartyname.city_id_text_box + " - " + billingpartyname.unit_pincode;
                    var consignee = consinee_name + "<br>" + consinee_addr.Replace("\n", "<br>");

                    var statename = billingpartyname.state_id_text_box;//"";
                    var state_id = billingpartyname.state_id;
                    var pos = "";
                    if (state_id > 0)
                    {
                        statename = _context.tbl_state_master.SingleOrDefault(x => x.state_no == state_id).state_name;
                        var stateno = billingpartyname.tbl_state_master.state_GST_id;
                        pos = stateno + "-" + statename;
                    }
                    var gstno = billingpartyname.unit_gst_no;

                    var notify1 = subjobInDb.SeaImport_Notify1_name;
                    var notify1_add = subjobInDb.SeaImport_Notify1_add;
                    var notify = "";
                    if (notify1_add != null && notify1_add != "")
                    {
                        notify = notify1 + "<br>" + notify1_add.Replace("\n", "<br>");
                    }
                    var shipper_name = "";
                    if (subjobInDb.SeaImport_Consignee_company != null && subjobInDb.SeaImport_Consignee_company != "" && subjobInDb.SeaImport_Consignee_company != "NA")
                    {
                        shipper_name = subjobInDb.SeaImport_Consignee_company;
                    }

                    var InvoiceNo = InvoiceInDb.AirExportInvoice_No;
                    var InvoiceDt = string.Format("{0:dd-MM-yyyy}", InvoiceInDb.AirExportInvoice_date);

                    var mbl_no = _context.Tbl_SeaImport_master_job.SingleOrDefault(x => x.SeaImport_Master_job_id == subjobInDb.SeaImport_Master_job_id).SeaImport_Mbl_No;
                    var mbl_date = string.Format("{0:dd-MM-yyyy}", _context.Tbl_SeaImport_master_job.SingleOrDefault(x => x.SeaImport_Master_job_id == subjobInDb.SeaImport_Master_job_id).SeaImport_Mbl_Date);
                    var mblno_date = mbl_no + " / " + mbl_date;

                    var hbl_no = subjobInDb.SeaImport_Hbl_No;
                    var hbl_date = string.Format("{0:dd-MM-yyyy}", subjobInDb.SeaImport_Hbl_Date);
                    var hblno_date = hbl_no + " / " + hbl_date;

                    var NoOfPackage = subjobInDb.SeaImport_Package_Count;
                    var packageType = subjobInDb.SeaImport_Package_Type;
                    var Gross_weight = string.Format("{0:0.00}", subjobInDb.SeaImport_Gross_Weight);
                    var package = NoOfPackage + " " + packageType + " / " + Gross_weight + " KGS";

                    var masterjobid = InvoiceInDb.AirExportMaster_Job_id;
                    if (InvoiceInDb.billing_Against == "hawb" || InvoiceInDb.billing_Against == null)
                    {
                        var HawbInDb = _context.Tbl_SeaImport_sub_job.Where(x => x.SeaImport_Sub_job_id == InvoiceInDb.AirExportSubJobId).ToList();
                        package = HawbInDb.Sum(x => x.SeaImport_Package_Count) + " " + HawbInDb[0].SeaImport_Package_Type + " / " + HawbInDb.Sum(x => x.SeaImport_Gross_Weight);
                    }
                    else
                    {
                        var MawbInDb = _context.Tbl_SeaImport_sub_job.Where(x => x.SeaImport_Master_job_id == InvoiceInDb.AirExportMaster_Job_id).ToList();
                        package = MawbInDb.Sum(x => x.SeaImport_Package_Count) + " " + MawbInDb[0].SeaImport_Package_Type + " / " + MawbInDb.Sum(x => x.SeaImport_Gross_Weight);
                    }

                    var Vessel = subjobInDb.SeaImport_Vessel;
                    var Voyage = subjobInDb.SeaImport_Voyage;
                    var Vessel_Voyage = Vessel + " / " + Voyage;

                    var igm_date = string.Format("{0:dd-MM-yyyy}", subjobInDb.SeaImport_IGMDate);
                    var IgmNo = subjobInDb.SeaImport_DO_IGMNo;
                    var igmNo_date = IgmNo + " / " + igm_date;

                    var Item = subjobInDb.SeaImport_ITEMNO;
                    var SubItem = subjobInDb.SeaImport_SubItem;
                    var itemNo_SubItem = Item + " / " + SubItem;

                    var origin = _context.tbl_seaport_master.SingleOrDefault(x => x.seaport_no == subjobInDb.SeaImport_origin).seaport_name.Split('?')[0];
                    var Destination = _context.tbl_seaport_master.SingleOrDefault(x => x.seaport_no == subjobInDb.SeaImport_Destination).seaport_name.Split('?')[0];

                    var irnno = InvoiceInDb.EInvoice_IrnNo;
                    if (irnno == null)
                    {
                        irnno = "";
                    }
                    var filename = InvoiceInDb.Qr_code_file_name;
                    if (filename == null)
                    {
                        filename = "";
                    }


                    var exchangerate = InvoiceInDb.billing_currency + " / " + string.Format("{0:0.00}", InvoiceInDb.billing_exchrate);

                    var masterjobNo = _context.Tbl_SeaImport_master_job.SingleOrDefault(x => x.SeaImport_Master_job_id == subjobInDb.SeaImport_Master_job_id).SeaImport_Master_job_no;
                    var subjobNo = subjobInDb.SeaImport_Sub_job_no;
                    var jobno = subjobNo;

                    var segment = subjobInDb.SIF_segment_no;
                    var branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                    switch (segment)
                    {
                        case 0:
                            branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 4).FirstOrDefault();
                            break;
                        case 1:
                            branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 5).FirstOrDefault();
                            break;
                        case 2:
                            branchInDb = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == 6).FirstOrDefault();
                            break;
                    }
                    var branchaddr = branchInDb.ryal_branch_address;
                    if (!string.IsNullOrEmpty(branchaddr))
                    {
                        branchaddr = branchaddr.Replace("\n", "<br>");
                    }
                    var ourgstin = branchInDb.ryal_branch_GST_no;
                    var branchstatecode = branchInDb.tbl_state_master.state_GST_id + " - " + branchInDb.tbl_state_master.state_name;
                    var userbranch = segment + 4; //_context.tbl_user_master.Where(x => x.user_no == userid1).Select(x => x.tbl_user_branch_association.Select(y => y.ryal_branch_no).FirstOrDefault()).FirstOrDefault();
                    var userstate = _context.tbl_ryal_business_branch.Where(x => x.ryal_branch_no == userbranch).Select(x => x.tbl_state_master.state_name).FirstOrDefault();

                    var description = subjobInDb.SeaImport_Self_Details;
                    var billingid = InvoiceInDb.AirExportBilling_id;
                    var chargeslist = _context.tbl_airexport_billing_charges.Where(x => x.Billing_id == billingid).ToList();
                    var rocount = 0; var charge_name = ""; var hsnno = ""; var cgst = ""; var sgst = ""; var igst = ""; var Amount = "";
                    var cgstrate = ""; var sgstrate = ""; var igstrate = "";
                    var NetAmount = 0.00; var CGSTAmt = 0.00; var SGSTAmt = 0.00; var IGSTAmt = 0.00; var Round_off = 0.00; var InvoiceAmt = 0.00;
                    for (int i = 0; i < chargeslist.Count; i++)
                    {
                        if (i > 0)
                        {
                            charge_name = charge_name + ",";
                            hsnno = hsnno + ",";
                            cgst = cgst + ",";
                            sgst = sgst + ",";
                            igst = igst + ",";
                            Amount = Amount + ",";
                            cgstrate = cgstrate + ",";
                            sgstrate = sgstrate + ",";
                            igstrate = igstrate + ",";
                        }
                        rocount += 1;
                        var charge_no = chargeslist[i].AirExportCharge_Code;
                        var ChargeIndb = _context.tbl_charge_master.SingleOrDefault(x => x.charge_code == charge_no);
                        var hsncode = ChargeIndb.charge_sac;
                        hsnno = hsnno + _context.tbl_sac_code_master.SingleOrDefault(x => x.sac_no == hsncode).sac_code;
                        hsnno = hsnno.Replace("SAC ", "");
                        charge_name = charge_name + ChargeIndb.charge_name + " " + chargeslist[i].billing_charges_remark;
                        Amount = Amount + string.Format("{0:0.00}", (chargeslist[i].AirExportCharge_Amount));
                        if (statename.ToUpper() == userstate.ToUpper())
                        {
               
                            int state_no = (int)state_id;
                            var sac_no = (int)ChargeIndb.charge_sac;
                            NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
                            CGSTAmt = Convert.ToDouble(CGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_CGST);
                            SGSTAmt = Convert.ToDouble(SGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_SGST);
                            double cgst1 = Convert.ToDouble(chargeslist[i].AirExportCharge_Amount) * Convert.ToDouble(ChargeIndb.charge_cgst) / 100;
                            double sgst1 = Convert.ToDouble(chargeslist[i].AirExportCharge_Amount) * Convert.ToDouble(ChargeIndb.charge_sgst) / 100;
                            cgst = cgst + string.Format("{0:0.00}", (cgst1)); Convert.ToDouble(cgst1);
                            sgst = sgst + string.Format("{0:0.00}", (sgst1)); Convert.ToDouble(sgst1);
                            cgstrate = cgstrate + string.Format("{0:0.00}", (ChargeIndb.charge_cgst));
                            sgstrate = sgstrate + string.Format("{0:0.00}", (ChargeIndb.charge_sgst));
                        }
                        else
                        {

                            if (state_id == null)
                            {
                                state_id = 3;
                            }
                            int state_no = (int)state_id;
                            var sac_no = (int)ChargeIndb.charge_sac;
                            NetAmount = Convert.ToDouble(NetAmount) + Convert.ToDouble(chargeslist[i].AirExportCharge_Amount);
                            IGSTAmt = Convert.ToDouble(IGSTAmt) + Convert.ToDouble(chargeslist[i].AirExportCharge_IGST);
                            double igst2 = Convert.ToDouble(chargeslist[i].AirExportCharge_Amount) * Convert.ToDouble(ChargeIndb.charge_igst) / 100;
                            var igsttax = string.Format("{0:0.00}", Convert.ToDouble(igst2));
                            igst = igst + igsttax;
                            igstrate = igstrate + string.Format("{0:0.00}", Convert.ToDouble(ChargeIndb.charge_igst));
                        }
                    }
                    var finalNetamt = string.Format("{0:0.00}", NetAmount);
                    var netamt = "";
                    if (statename.ToUpper() == userstate.ToUpper())
                    {
                        var totval = NetAmount + CGSTAmt + SGSTAmt;
                        netamt = string.Format("{0:0.00}", totval);
                    }
                    else
                    {
                        netamt = string.Format("{0:0.00}", NetAmount + IGSTAmt);
                    }

                    var netamt1 = netamt.ToString().Split('.')[1];
                    var finalroundof = "";
                    if (Convert.ToDecimal(netamt1) >= 50)
                    {
                        InvoiceAmt = Convert.ToDouble(netamt.ToString().Split('.')[0]) + 1;
                        Round_off = 100 - Convert.ToDouble(netamt1);
                        if (Round_off > 9)
                            finalroundof = "0." + Round_off.ToString();
                        else
                            finalroundof = "0.0" + Round_off.ToString();
                    }
                    else
                    {
                        InvoiceAmt = Convert.ToDouble(netamt.ToString().Split('.')[0]);
                        Round_off = Convert.ToDouble(netamt.ToString().Split('.')[1]);
                        if (Round_off > 9)
                            finalroundof = "-0." + Round_off.ToString();
                        else
                            finalroundof = "-0.0" + Round_off.ToString();
                    }

                    var finalInvoiceAmt = string.Format("{0:0.00}", InvoiceAmt);
                    var NumToWord =commclass.NumberToWords(Convert.ToInt32(InvoiceAmt));

                    var drafedorapprover = "";
                    var approvedornot = "";
                    if (InvoiceInDb.billing_Approval == "yes")
                    {
                        var approvebyid = _context.tbl_user_master.Where(x => x.user_no == InvoiceInDb.billing_Approval_by).Select(x => x.add_contact_no).FirstOrDefault();
                        var approvedbyname = _context.tbl_address_book.Where(x => x.add_contact_no == approvebyid).FirstOrDefault();
                        drafedorapprover = approvedbyname.add_contact_fname + " " + approvedbyname.add_contact_mname + " " + approvedbyname.add_contact_lname + "<br/>" + string.Format("{0:dd/MM/yyyy hh:mm tt}", InvoiceInDb.billing_Approval_on);
                        approvedornot = "yes";
                    }
                    else
                    {
                        drafedorapprover = "DRAFT BILL";
                        approvedornot = "no";
                    }


                    var Invoice_type = InvoiceInDb.invoice_type;

                    string jsonString = consinee_name + "|" + consinee_addr + "|" + notify + "|" + shipper_name + "|" + InvoiceNo + "|" + InvoiceDt + "|" + mblno_date + "|" + hblno_date + "|" + package + "|" + Vessel_Voyage + "|" + exchangerate + "|" + igmNo_date + "|" + itemNo_SubItem + "|" + origin + "|" + Destination + "|" + jobno + "|" + description + "|" + rocount + "|" + charge_name + "|" + hsnno + "|" + cgst + "|" + sgst + "|" + Amount + "|" + finalNetamt + "|" + CGSTAmt + "|" + SGSTAmt + "|" + finalroundof + "|" + finalInvoiceAmt + "|" + NumToWord + "|" + subjobInDb.SeaImport_Sub_job_no + "|" + statename + "|" + cgstrate + "|" + sgstrate + "|" + drafedorapprover + "|" + approvedornot + "|" + igst + "|" + IGSTAmt + "|" + igstrate + "|" + branchaddr + "|" + ourgstin + "|" + branchstatecode + "|" + irnno + "|" + filename + "|" + pos + "|" + gstno + "|" + Invoice_type + "|" + shippername + "|" + remark;

                    ViewBag.CanPrint = jsonString;
                }
            }
            catch (Exception)
            {
                var encryptedid = encryptDecrypt.DecryptString(mawbid.ToString());
                return RedirectToAction("Index", new { Search_By = Session["airimportManageInvoiceBy"], Search_Value = Session["airimportManageInvoiceValue"], Received_From_Dt = Session["airimportManageInvoiceFrom"], Received_To_Dt = Session["airimportManageInvoiceTo"] });
            }

            return View();

        }

        // Generate PDF File Changes by Rituraj
        [HttpPost, ValidateInput(false)]
        public ActionResult GeneratePdfFile(string htmlString, string formatted_job_no)
        {
            string finalHtmlString = @"<html><body>" + htmlString + "" + "</body></html>";

            string response = "";

            try
            {
                string baseUrl = "";
                PdfPageSize pageSize = PdfPageSize.A4;
                PdfPageOrientation pdfOrientation = PdfPageOrientation.Portrait;
                int webPageWidth = 197;

                int webPageHeight = 0;

                PdfDocument doc = new PdfDocument();

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                // set converter options
                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = webPageWidth;
                converter.Options.WebPageHeight = webPageHeight;

                // create a new pdf document converting an url
                doc = converter.ConvertHtmlString(finalHtmlString, baseUrl);
                var mjob = formatted_job_no.Split('-');
                doc.DocumentInformation.Title = mjob[1] + "-" + mjob[2];

                // save pdf document
                //doc.Save(Server.MapPath("~/Sample.pdf"));      

                #region 
                string path = AppDomain.CurrentDomain.BaseDirectory + "uploads\\PDF";

                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }
                string filename = Path.GetFileName(Server.MapPath("~/Sample.pdf"));
                string pth = Path.Combine(path, filename);
                doc.Save(pth);
                #endregion

                // Delete pdf document
                #region
                IAmazonS3 clientDel = new AmazonS3Client(RegionEndpoint.APSouth1);

                TransferUtility utilityDel = new TransferUtility(clientDel);
                TransferUtilityUploadRequest requestDel = new TransferUtilityUploadRequest();

                requestDel.BucketName = "ryalfiles";
                string unique_file_name_del = formatted_job_no + ".pdf";
                requestDel.Key = unique_file_name_del;

                clientDel.DeleteObject(requestDel.BucketName, requestDel.Key);
                clientDel.Dispose();
                #endregion


                // Save pdf document
                #region 
                AmazonUploader myUploader = new AmazonUploader();

                string unique_file_name = "";

                IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);

                TransferUtility utility = new TransferUtility(client);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

                request.BucketName = "ryalfiles";
                unique_file_name = formatted_job_no + ".pdf";

                request.Key = unique_file_name;

                byte[] files = System.IO.File.ReadAllBytes(pth);

                Stream stream = new MemoryStream(files);

                request.InputStream = stream;
                utility.Upload(request);

                response = "true|" + unique_file_name;

                // close pdf document
                doc.Close();
                #endregion
            }
            catch (Exception ex)
            {
                using (StreamWriter _testData = new StreamWriter(Server.MapPath("~/App_Data/errors/AirExportInvoice.txt"), true))
                {
                    _testData.WriteLine("Message: " + ex.Message + "<br/>" + Environment.NewLine + "StackTrace: " + ex.StackTrace + "<br/>" + Environment.NewLine + "DateTime: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt").Replace('-', '/') +
                        "<br/>" + Environment.NewLine + "<br/>" + Environment.NewLine); // Write the file.
                    _testData.Dispose();
                }
                response = "false|" + ex.Message;
                return Content("Invalid json");
            }

            string doc_name = "NA";

            if (response.Split('|')[0] == "true")
            {
                doc_name = response.Split('|')[1];
            }

            return Content("https://s3.ap-south-1.amazonaws.com/ryalfiles/" + doc_name);


        }
    }
}