﻿$(function () {

    /*Delete button warning message*/
    $('input[value="DELETE"]').click(function () {

        if ($(".table").find("tr:gt(0)").find("td:eq(" + Number($(".table").find("tr:eq(1) td").length - 1) + ") :checked").length === 0) {
            alert("Kindly select at least one record!");
            return false;
        }
    });

    $('.modal').css("margin-top", $(window).height() / 4);

    $('#login').css("margin-top", $(window).height() / 10);

    $("#select_all_chk").hide();

    $("#select_all_chk").click(function () {

        if ($(this).prop("checked")) {
            $("#div_table").find("li:gt(0)").find("input[type='checkbox']").prop("checked", true);
            $("#div_table").find("li:eq(0)").find("#select_all_lbl").text("Unselect All");
        } else {
            $("#div_table").find("li:gt(0)").find("input[type='checkbox']").prop("checked", false);
            $("#div_table").find("li:eq(0)").find("#select_all_lbl").text("Select All");
        }
    });

    $("#btn_reset").on('click', function () {

        $("#Search_Data").val("");
        $('input[value="Search"]').trigger("click");
    });

    $('#div_table tr td').each(function () {
        if ($(this).find('p').hasClass('btn-link') === false) {
            $(this).find('p').removeClass('available');
        }
    });



    $(".onlynoaccept").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. Numbers.
        var regex = /^[0-9]+$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }

        return isValid;
    });

    $(".nocomaspace").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. Numbers.
        var regex = /^[0-9\s,]+$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }

        return isValid;
    });


    $(".onlydecimalno").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        var abc = $(this).val();

        //Regex for Valid Characters i.e. Numbers.
        var regex = /[0-9.]/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }
        else {
            if (abc.indexOf('.') >= 0 && keyCode === 46) {
                return false;
            }
        }
    });
       
    $("[isselect=\"no\"]").blur(function (event) {
        if ($(this).attr("isselect") === "no" && $(this).next().val() === "") {
            $(this).val("");
        }
        if ($(this).val() === "") {
            $(this).attr('isselect', 'no');
        } else if ($(this).next().val() === "") {
            $(this).val("");
            $(this).attr('isselect', 'no');
        }
    });
    $("input[isselect]").keydown(function (event) {
        var keycode = event.keyCode;
        if (keycode !== 9) {
            $(this).next().val("");
            $(this).attr('isselect', 'no');
        }

    });






    $(".onlynoandspace").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. Numbers and space.
        var regex = /^[0-9\s]*$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }

        return isValid;
    });
    $(".onlycharecter").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. charecter and space.
        var regex = /^[a-zA-Z\s.]*$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }

        return isValid;
    });
    $(".onlycharecter1").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. charecter.
        var regex = /^[a-zA-Z\s]*$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }

        return isValid;
    });
    $(".onlynoandcharecter").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. number and charecter.
        var regex = /^[a-zA-Z0-9]*$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }

        return isValid;
    });
    $(".onlynospaceandcharecter").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. number and charecter.
        var regex = /^[a-zA-Z\s0-9\s]*$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            return false;
        }

        return isValid;
    });

    $(".notsomespecial").on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. number and charecter.
        var regex = /^[\\|]*$/;

        //Validate TextBox value against the Regex.
        var isValid = !regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            //return isValid;
            return false;
        } else {
            //return false;
        }

        return isValid;
    });

    $(".notsomespecial").on('blur', function (e) {
        var val = $(this).val();
        val = val.replace('\\', '-');
        val = val.replace('|', '.');
        return val;
    });


});